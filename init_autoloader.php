<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

/**
 * This autoloading setup is really more complicated than it needs to be for most
 * applications. The added complexity is simply to reduce the time it takes for
 * new developers to be productive with a fresh skeleton. It allows autoloading
 * to be correctly configured, regardless of the installation method and keeps
 * the use of composer completely optional. This setup should work fine for
 * most users, however, feel free to configure autoloading however you'd like.
 */

/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(__DIR__);

// Composer autoloading
if (file_exists('vendor/autoload.php')) {
    $loader = include 'vendor/autoload.php';
}

if (class_exists('Zend\Loader\AutoloaderFactory')) {

	// Disable dompdf's internal autoloader as we're using composer
	define('DOMPDF_ENABLE_AUTOLOAD', false);
    define('DOMPDF_ENABLE_REMOTE', true);
    define('DOMPDF_ENABLE_CSS_FLOAT', true);
	require_once realpath('vendor/dompdf/dompdf/dompdf_config.inc.php');

    // Setup Newspress core autoloading
    require_once realpath('vendor/zendframework/zendframework/library/Zend/Loader/StandardAutoloader.php');
    $loader = new Zend\Loader\StandardAutoloader();
    $loader->registerNamespace('Newspress', realpath('core/Newspress/library/core'));
    $loader->register();

    // Setup the final core Newspress class
    require 'core/Newspress.php';

    return;
}

throw new RuntimeException('Unable to load ZF2. Run `php composer.phar install` or define a ZF2_PATH environment variable.');
