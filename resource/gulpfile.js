// 'use strict';

/**
 * Include our npm plugins (including gulp)
 */
var gulp        = require('gulp'),
    sass        = require('gulp-ruby-sass'),
    sourcemaps  = require('gulp-sourcemaps'),
    fs          = require('fs'),
    prefix      = require('gulp-autoprefixer'),
    notify      = require('gulp-notify'),
    livereload  = require('gulp-livereload'),
    minify      = require('gulp-minify-css'),
    concat      = require('gulp-concat'),
    uglify      = require('gulp-uglify'),
    jshint      = require('gulp-jshint'),
    header      = require('gulp-header'),
    imagemin    = require('gulp-imagemin'),
    pngcrush    = require('imagemin-pngcrush'),
    plumber     = require('gulp-plumber'),
    svgSprite   = require('gulp-svg-sprite'),
    filter      = require('gulp-filter'),
    svg2png     = require('gulp-svg2png'),
    shell       = require('gulp-shell'),
    htmlhint    = require('gulp-htmlhint'),
    del         = require('del');


/**
 * Get our package.json file
 * @type object
 */
var pkg = require('./package.json'),

/**
 * Browsers List
 * @type Array
 */
browserList = ["last 10 Chrome versions",
    "last 10 Firefox versions",
    "Firefox ESR",
    "last 10 iOS versions",
    "last 10 Android versions",
    "last 10 BlackBerry versions",
    "last 10 Opera versions",
    "last 10 OperaMobile versions",
    "last 10 OperaMini versions",
    "last 10 ChromeAndroid versions",
    "last 10 FirefoxAndroid versions",
    "last 10 ExplorerMobile versions"];




/**
 * Create our copyright comment variable
 *
 * This uses variables from our package.json file which include the author,
 * the company website, licence and the version of the package. Versioning
 * should be kept up to date when major and minor changes are made to the
 * core system (once version 1.0.0 has been completed).
 * 
 * @type {Array}
 */
var banner = [
        '/**',
        ' * <%= pkg.author %> (<%= pkg.website %>)',
        ' *',
        ' * @link      <%= pkg.website %>',
        ' * @copyright Copyright (c) <%= new Date().getFullYear() %> <%= pkg.author %> (<%= pkg.website %>)',
        ' * @license  <%= pkg.license %> Licence',
        ' *',
        ' * Version: <%= pkg.version %>',
        ' */',
        ''
    ].join('\n');


/**
 * @todo: Add our sprite configuration as a variable so we don't repeat code
 *
 * Currently we cannot do this as the template file needs to be different.
 */
// var spriteConfig = {
//     preview: false,
//     svg: {
//         sprite: "sprites.svg"
//     },
//     cssFile: "./sass/includes/_sprites.scss",
//     padding: 3,
//     // layout: "horizontal",diagonal
//     templates: {
//         css: fs.readFileSync("./../core/Newspress/resource/sass/includes/_template.scss", "utf-8")
//     }
// };


/**
 * Compile our core Sass
 * 
 * @return stream
 */
gulp.task('core-sass', function() {
    return gulp.src('./../core/Newspress/resource/sass/**/*.scss')
        .pipe(sass({
            // sourcemap: true,
            style: 'compact'
        }))
        .on('error', notify.onError())
        .pipe(prefix(browserList, { cascade: true }))
        .on('error', notify.onError())
        .pipe(minify())
        .on('error', notify.onError())
        .pipe(header(banner, { pkg : pkg } ))
        .pipe(gulp.dest('./../core/Newspress/public/css'))
        .pipe(notify("SASS compilation complete: <%=file.relative%>"))
        .pipe(livereload());
});


/**
 * Compile our local Sass
 * 
 * @return stream
 */
gulp.task('local-sass', ['core-sass'], function() {
    var sassFilter = filter(['*.css', '!*.map']);
    return gulp.src('./sass/**/*.scss')
        .pipe(sass({
            style: 'compact'
        }))
        .on('error', notify.onError())
        .pipe(sourcemaps.init())
        .pipe(prefix(browserList, { cascade: true }))
        .on('error', notify.onError())
        .pipe(minify())
        .on('error', notify.onError())
        .pipe(header(banner, { pkg : pkg } ))
        .pipe(sassFilter)
        .pipe(sourcemaps.write('.', { includeContent: false }))
        // .pipe(sassFilter.restore()) // Issue with streaming? {end: true}
        .pipe(gulp.dest('./../public/css'))
        // .pipe(notify("SASS compilation complete: <%=file.relative%>"))
        .pipe(livereload());
});


gulp.task('local-sass-browser-ie8-hacks', function() {
    var sassFilter = filter(['*.css', '!*.map']);
    return gulp.src('./sass/hacks/ie8/*.scss')
        .pipe(sass({
            style: 'compact'
        }))
        .on('error', notify.onError())
        .pipe(sourcemaps.init())
        .pipe(prefix(browserList, { cascade: true }))
        .on('error', notify.onError())
        .pipe(minify())
        .on('error', notify.onError())
        .pipe(header(banner, { pkg : pkg } ))
        .pipe(sassFilter)
        .pipe(sourcemaps.write('.', { includeContent: false }))
        // .pipe(sassFilter.restore()) // Issue with streaming? {end: true}
        .pipe(gulp.dest('./../public/css/hacks/ie8'))
        // .pipe(notify("SASS compilation complete: <%=file.relative%>"))
        .pipe(livereload());
});

/**
 * Compile our JavaScript
 * 
 * @return void
 * @todo   Add streaming
 */
gulp.task('scripts', function() {
    gulp.src('./../core/Newspress/resource/javascript/*.js')
        //.pipe(concat('global.js'))
        .pipe(jshint())
        .pipe(jshint.reporter())
        //.pipe(uglify())
        .pipe(header(banner, { pkg : pkg } ))
        .pipe(gulp.dest('./../core/Newspress/public/js'))
        .pipe(livereload());

    gulp.src('./javascript/*.js')
        //.pipe(concat('global.js'))
        .pipe(jshint())
        .pipe(jshint())
        //.pipe(uglify())
        .pipe(header(banner, { pkg : pkg } ))
        .pipe(gulp.dest('./../public/js'))
        .pipe(livereload());
});


/**
 * Compress our images
 *
 * Notice we are moving our SVG files rather than compressing them as the
 * compression plugin being used has a bug in that can cause small details
 * within the SVG to be removed (as the plugin sees this as compression).
 * 
 * @return void
 * @todo   Add streaming
 */
gulp.task('images', function() {

    // Compress images for core
    gulp.src([
            './../core/Newspress/resource/images/**/*.*',
            '!./../core/Newspress/resource/images/*.svg',
            '!./../core/Newspress/resource/images/sprites/*.*',
            '!./../core/Newspress/resource/images/sprites.*'
        ])
        .pipe(imagemin({
            progressive: true,
            use: [pngcrush()]
        }))
        .pipe(gulp.dest('./../core/Newspress/public/img'));

    gulp.src([
            './../core/Newspress/resource/images/*.svg',
            '!./../core/Newspress/resource/images/sprites.*'
        ])
        .pipe(gulp.dest('./../core/Newspress/public/img'))
        .pipe(livereload());

    // Compress images locally
    gulp.src([
            './images/**/*.*',
            '!./images/*.svg',
            '!./images/sprites/*.*',
            '!./images/sprites/sprites.*'
        ])
        .pipe(imagemin({
            progressive: true,
            use: [pngcrush()]
        }))
        .pipe(gulp.dest('./../public/img'));

    gulp.src([
            './images/*.svg',
            '!./images/sprites.*',
            '!./images/sprites/*.*'
        ])
        .pipe(gulp.dest('./../public/img'))
        .pipe(livereload());
});


/**
 * Run our sprites function
 *
 * This generates a spritesheet using SVGs and creates PNG fallbacks. It also
 * creates the CSS required for these sprites which will be relative to the
 * sprite filename. This function works with streaming so will follow a
 * backwards path for its dependancy functions.
 *
 * @return void
 */
gulp.task('sprites', ['move-local-sprites'], function() {

    // Optimise the core sprites PNG file and move to the core public directory
    gulp.src('./../core/Newspress/resource/images/sprites.png')
        .pipe(imagemin({
            progressive: true,
            use: [pngcrush()]
        }))
        .pipe(gulp.dest('./../core/Newspress/public/img'))
        .pipe(livereload());

    // Optimise the sprites PNG file and move to the public directory
    gulp.src('./images/sprites.png')
        .pipe(imagemin({
            progressive: true,
            use: [pngcrush()]
        }))
        .pipe(gulp.dest('./../public/img'))
        .pipe(livereload());

    // Now that we've changed the sprites scss file, update the sass files that
    // include the _sprites.scss file

});

/**
 * Generate sprites for the core (for use in core modules and views)
 * 
 * @return stream
 */
gulp.task('core-sprites', function(callback) {
    return gulp.src('./../core/Newspress/resource/images/sprites/*.svg')
        .pipe(plumber())
        .pipe(svgSprite({
            shape: {
                spacing: {
                    padding: 3
                }
            },
            mode: {
                css: {
                    prefix: "%s",
                    dimensions: '',
                    sprite: './../core/Newspress/resource/images/sprites.svg',
                    bust: false,
                    render: {
                        scss: {
                            dest: './../core/Newspress/resource/sass/includes/_sprites.scss',
                            template: './../core/Newspress/resource/sass/includes/_template.scss'
                        }
                    }
                }
            }
        }))
        .on('error', function(error) {
            callback(error);
        })
        .pipe(gulp.dest("./../"));
});

/**
 * Generate sprites for the local website
 * 
 * @return stream
 */
gulp.task('local-sprites', ['move-core-sprites'], function(callback) {
    return gulp.src('./images/sprites/*.svg')
        .pipe(plumber())
        .pipe(svgSprite({
            shape: {
                spacing: {
                    padding: 3
                }
            },
            mode: {
                css: {
                    prefix: "%s",
                    dimensions: '',
                    sprite: './../resource/images/sprites.svg',
                    bust: false,
                    render: {
                        scss: {
                            dest: './../resource/sass/includes/_sprites.scss',
                            template: './../core/Newspress/resource/sass/includes/_template.scss'
                        }
                    }
                }
            }
        }))
        .on('error', function(error) {
            callback(error);
        })
        .pipe(gulp.dest("./../"));
});

/**
 * Create a PNG fallback image of the core sprite SVG file
 * 
 * @return stream
 * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
 */
gulp.task('generate-core-sprite-png', ['core-sprites'], function() {
    return gulp.src('./../core/Newspress/resource/images/sprites.svg')
        .pipe(svg2png())
        .pipe(gulp.dest('./../core/Newspress/resource/images'));
});

/**
 * Create a PNG fallback image of the local sprite SVG file
 * 
 * @return stream
 * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
 */
gulp.task('generate-local-sprite-png', ['local-sprites'], function() {
    return gulp.src('./images/sprites.svg')
        .pipe(svg2png())
        .pipe(gulp.dest('./images'));
});

/**
 * Move the SVG in the core resources folder to the core public directory
 * 
 * @return stream
 * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
 */
gulp.task('move-core-sprites', ['generate-core-sprite-png'], function() {
    return gulp.src('./../core/Newspress/resource/images/sprites.svg')
        .pipe(gulp.dest('./../core/Newspress/public/img'));
});

/**
 * Move the SVG in the resources folder to the public directory
 * 
 * @return stream
 * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
 */
gulp.task('move-local-sprites', ['generate-local-sprite-png'], function() {
    return gulp.src('./images/sprites.svg')
        .pipe(gulp.dest('./../public/img'));
});




/**
 * @todo: Validate HTML
 * @todo: Validate XHTML for email templates
 */


/**
 * Run the php script that clears the server cache
 * 
 * @return stream
 */
gulp.task('cache', function() {
    return shell.task(['php core/scripts/clearCache.php development']);
});


/**
 * Clear the .sass-cache directory
 *
 * This function may only need to be used a handful of times, but
 * can sort any issues when compiling stylesheets with includes.
 * 
 * @return stream
 */
gulp.task('clear-sass-cache', function() {
    return del('./.sass-cache/*', function (err, deletedFiles) {
        console.log('Files deleted:', deletedFiles.join(', '));
    });
});


/**
 * Reload core .phtml files
 *
 * Makes working on front end changes quicker with livereload.
 *
 * @todo   Make update for html validation to ignore PHP
 * @return stream
 */
gulp.task('core-phtml', function() {
    return gulp.src('./../core/Newspress/module/*/view/*/*/**.phtml')
        // .pipe(htmlhint())
        // .pipe(htmlhint.reporter())
        .pipe(livereload());
});


/**
 * Reload local .phtml files
 *
 * Makes working on front end changes quicker with livereload.
 *
 * @todo   Make update for html validation to ignore PHP
 * @return stream
 */
gulp.task('local-phtml', function() {
    return gulp.src('./../module/*/view/*/*/**.phtml')
        // .pipe(htmlhint())
        // .pipe(htmlhint.reporter())
        .pipe(livereload());
});


/**
 * Our watch script. Watches for changes in files to run the relevant
 * function that compiles the required code.
 * 
 * @return void
 */
gulp.task('watch', function() {

    // Watch for changes in core sass files to recompile
    gulp.watch(['./../core/Newspress/resource/sass/**/*.scss'], ['core-sass']);

    // Watch for changes in local sass files to recompile
    gulp.watch(['./sass/**/*.scss'], ['local-sass']);

    gulp.watch(['./../core/Newspress/resource/javascript/*', './javascript/*'], ['scripts']);

    // Watch for changes in images (both core and local) for image compression
    gulp.watch(['./../core/Newspress/resource/images/*', './images/*'], ['images']);

    // Watch sprite files and template for sprite generation
    gulp.watch([
            './../core/Newspress/resource/images/sprites/*.svg',
            './../core/Newspress/resource/sass/includes/_template.scss',
            './images/sprites/*.svg',
            './sass/includes/_template.scss' // Compile the sass files
        ], ['sprites']);

    // Reload core view files when editing
    gulp.watch('./../core/Newspress/module/*/view/*/*/**.phtml', ['core-phtml']);

    // Reload local view files when editing
    gulp.watch('./../module/*/view/*/*/**.phtml', ['local-phtml']);

    // Clear the cache when changing .ini files
    // gulp.watch('./../core/*/config/*.ini', ['cache']);
});


/**
 * Default task (runs when running `gulp` from the command line)
 *
 * Each of these functions can also be run individually from the command line
 * by using `gulp {function}`. Taking the clear-sass-cache function as an
 * example, you would run `gulp clear-sass-cache` from the command line.
 */
gulp.task('default', ['sprites', 'local-sass', 'scripts', 'images', 'watch']);
