var mediaQuery = 'desktop',
    emailButtonClicked,
    lastScrollOffset = 0,
    windowWidth = $(window).width(),
    mobileView = windowWidth < 480,
    iPad = (/iPad/i.test(navigator.userAgent)),
    isIe8 = $('html').is('.ie8'),
    downloadProcessing = false,
    lastForm = '';

//Slick slider - used for press release scrolling on mobile
!function(a){"use strict";"function"==typeof define&&define.amd?define(["jquery"],a):"undefined"!=typeof exports?module.exports=a(require("jquery")):a(jQuery)}(function(a){"use strict";var b=window.Slick||{};b=function(){function c(c,d){var f,g,h,e=this;if(e.defaults={accessibility:!0,adaptiveHeight:!1,appendArrows:a(c),appendDots:a(c),arrows:!0,asNavFor:null,prevArrow:'<button type="button" data-role="none" class="slick-prev" aria-label="previous">Previous</button>',nextArrow:'<button type="button" data-role="none" class="slick-next" aria-label="next">Next</button>',autoplay:!1,autoplaySpeed:3e3,centerMode:!1,centerPadding:"50px",cssEase:"ease",customPaging:function(a,b){return'<button type="button" data-role="none">'+(b+1)+"</button>"},dots:!1,dotsClass:"slick-dots",draggable:!0,easing:"linear",edgeFriction:.35,fade:!1,focusOnSelect:!1,infinite:!0,initialSlide:0,lazyLoad:"ondemand",mobileFirst:!1,pauseOnHover:!0,pauseOnDotsHover:!1,respondTo:"window",responsive:null,rows:1,rtl:!1,slide:"",slidesPerRow:1,slidesToShow:1,slidesToScroll:1,speed:500,swipe:!0,swipeToSlide:!1,touchMove:!0,touchThreshold:5,useCSS:!0,variableWidth:!1,vertical:!1,verticalSwiping:!1,waitForAnimate:!0},e.initials={animating:!1,dragging:!1,autoPlayTimer:null,currentDirection:0,currentLeft:null,currentSlide:0,direction:1,$dots:null,listWidth:null,listHeight:null,loadIndex:0,$nextArrow:null,$prevArrow:null,slideCount:null,slideWidth:null,$slideTrack:null,$slides:null,sliding:!1,slideOffset:0,swipeLeft:null,$list:null,touchObject:{},transformsEnabled:!1},a.extend(e,e.initials),e.activeBreakpoint=null,e.animType=null,e.animProp=null,e.breakpoints=[],e.breakpointSettings=[],e.cssTransitions=!1,e.hidden="hidden",e.paused=!1,e.positionProp=null,e.respondTo=null,e.rowCount=1,e.shouldClick=!0,e.$slider=a(c),e.$slidesCache=null,e.transformType=null,e.transitionType=null,e.visibilityChange="visibilitychange",e.windowWidth=0,e.windowTimer=null,f=a(c).data("slick")||{},e.options=a.extend({},e.defaults,f,d),e.currentSlide=e.options.initialSlide,e.originalSettings=e.options,g=e.options.responsive||null,g&&g.length>-1){e.respondTo=e.options.respondTo||"window";for(h in g)g.hasOwnProperty(h)&&(e.breakpoints.push(g[h].breakpoint),e.breakpointSettings[g[h].breakpoint]=g[h].settings);e.breakpoints.sort(function(a,b){return e.options.mobileFirst===!0?a-b:b-a})}"undefined"!=typeof document.mozHidden?(e.hidden="mozHidden",e.visibilityChange="mozvisibilitychange"):"undefined"!=typeof document.msHidden?(e.hidden="msHidden",e.visibilityChange="msvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(e.hidden="webkitHidden",e.visibilityChange="webkitvisibilitychange"),e.autoPlay=a.proxy(e.autoPlay,e),e.autoPlayClear=a.proxy(e.autoPlayClear,e),e.changeSlide=a.proxy(e.changeSlide,e),e.clickHandler=a.proxy(e.clickHandler,e),e.selectHandler=a.proxy(e.selectHandler,e),e.setPosition=a.proxy(e.setPosition,e),e.swipeHandler=a.proxy(e.swipeHandler,e),e.dragHandler=a.proxy(e.dragHandler,e),e.keyHandler=a.proxy(e.keyHandler,e),e.autoPlayIterator=a.proxy(e.autoPlayIterator,e),e.instanceUid=b++,e.htmlExpr=/^(?:\s*(<[\w\W]+>)[^>]*)$/,e.init(),e.checkResponsive(!0)}var b=0;return c}(),b.prototype.addSlide=b.prototype.slickAdd=function(b,c,d){var e=this;if("boolean"==typeof c)d=c,c=null;else if(0>c||c>=e.slideCount)return!1;e.unload(),"number"==typeof c?0===c&&0===e.$slides.length?a(b).appendTo(e.$slideTrack):d?a(b).insertBefore(e.$slides.eq(c)):a(b).insertAfter(e.$slides.eq(c)):d===!0?a(b).prependTo(e.$slideTrack):a(b).appendTo(e.$slideTrack),e.$slides=e.$slideTrack.children(this.options.slide),e.$slideTrack.children(this.options.slide).detach(),e.$slideTrack.append(e.$slides),e.$slides.each(function(b,c){a(c).attr("data-slick-index",b)}),e.$slidesCache=e.$slides,e.reinit()},b.prototype.animateHeight=function(){var a=this;if(1===a.options.slidesToShow&&a.options.adaptiveHeight===!0&&a.options.vertical===!1){var b=a.$slides.eq(a.currentSlide).outerHeight(!0);a.$list.animate({height:b},a.options.speed)}},b.prototype.animateSlide=function(b,c){var d={},e=this;e.animateHeight(),e.options.rtl===!0&&e.options.vertical===!1&&(b=-b),e.transformsEnabled===!1?e.options.vertical===!1?e.$slideTrack.animate({left:b},e.options.speed,e.options.easing,c):e.$slideTrack.animate({top:b},e.options.speed,e.options.easing,c):e.cssTransitions===!1?(e.options.rtl===!0&&(e.currentLeft=-e.currentLeft),a({animStart:e.currentLeft}).animate({animStart:b},{duration:e.options.speed,easing:e.options.easing,step:function(a){a=Math.ceil(a),e.options.vertical===!1?(d[e.animType]="translate("+a+"px, 0px)",e.$slideTrack.css(d)):(d[e.animType]="translate(0px,"+a+"px)",e.$slideTrack.css(d))},complete:function(){c&&c.call()}})):(e.applyTransition(),b=Math.ceil(b),d[e.animType]=e.options.vertical===!1?"translate3d("+b+"px, 0px, 0px)":"translate3d(0px,"+b+"px, 0px)",e.$slideTrack.css(d),c&&setTimeout(function(){e.disableTransition(),c.call()},e.options.speed))},b.prototype.asNavFor=function(b){var c=this,d=null!==c.options.asNavFor?a(c.options.asNavFor).slick("getSlick"):null;null!==d&&d.slideHandler(b,!0)},b.prototype.applyTransition=function(a){var b=this,c={};c[b.transitionType]=b.options.fade===!1?b.transformType+" "+b.options.speed+"ms "+b.options.cssEase:"opacity "+b.options.speed+"ms "+b.options.cssEase,b.options.fade===!1?b.$slideTrack.css(c):b.$slides.eq(a).css(c)},b.prototype.autoPlay=function(){var a=this;a.autoPlayTimer&&clearInterval(a.autoPlayTimer),a.slideCount>a.options.slidesToShow&&a.paused!==!0&&(a.autoPlayTimer=setInterval(a.autoPlayIterator,a.options.autoplaySpeed))},b.prototype.autoPlayClear=function(){var a=this;a.autoPlayTimer&&clearInterval(a.autoPlayTimer)},b.prototype.autoPlayIterator=function(){var a=this;a.options.infinite===!1?1===a.direction?(a.currentSlide+1===a.slideCount-1&&(a.direction=0),a.slideHandler(a.currentSlide+a.options.slidesToScroll)):(0===a.currentSlide-1&&(a.direction=1),a.slideHandler(a.currentSlide-a.options.slidesToScroll)):a.slideHandler(a.currentSlide+a.options.slidesToScroll)},b.prototype.buildArrows=function(){var b=this;b.options.arrows===!0&&b.slideCount>b.options.slidesToShow&&(b.$prevArrow=a(b.options.prevArrow),b.$nextArrow=a(b.options.nextArrow),b.htmlExpr.test(b.options.prevArrow)&&b.$prevArrow.appendTo(b.options.appendArrows),b.htmlExpr.test(b.options.nextArrow)&&b.$nextArrow.appendTo(b.options.appendArrows),b.options.infinite!==!0&&b.$prevArrow.addClass("slick-disabled"))},b.prototype.buildDots=function(){var c,d,b=this;if(b.options.dots===!0&&b.slideCount>b.options.slidesToShow){for(d='<ul class="'+b.options.dotsClass+'">',c=0;c<=b.getDotCount();c+=1)d+="<li>"+b.options.customPaging.call(this,b,c)+"</li>";d+="</ul>",b.$dots=a(d).appendTo(b.options.appendDots),b.$dots.find("li").first().addClass("slick-active").attr("aria-hidden","false")}},b.prototype.buildOut=function(){var b=this;b.$slides=b.$slider.children(":not(.slick-cloned)").addClass("slick-slide"),b.slideCount=b.$slides.length,b.$slides.each(function(b,c){a(c).attr("data-slick-index",b)}),b.$slidesCache=b.$slides,b.$slider.addClass("slick-slider"),b.$slideTrack=0===b.slideCount?a('<div class="slick-track"/>').appendTo(b.$slider):b.$slides.wrapAll('<div class="slick-track"/>').parent(),b.$list=b.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(),b.$slideTrack.css("opacity",0),(b.options.centerMode===!0||b.options.swipeToSlide===!0)&&(b.options.slidesToScroll=1),a("img[data-lazy]",b.$slider).not("[src]").addClass("slick-loading"),b.setupInfinite(),b.buildArrows(),b.buildDots(),b.updateDots(),b.options.accessibility===!0&&b.$list.prop("tabIndex",0),b.setSlideClasses("number"==typeof this.currentSlide?this.currentSlide:0),b.options.draggable===!0&&b.$list.addClass("draggable")},b.prototype.buildRows=function(){var b,c,d,e,f,g,h,a=this;if(e=document.createDocumentFragment(),g=a.$slider.children(),a.options.rows>1){for(h=a.options.slidesPerRow*a.options.rows,f=Math.ceil(g.length/h),b=0;f>b;b++){var i=document.createElement("div");for(c=0;c<a.options.rows;c++){var j=document.createElement("div");for(d=0;d<a.options.slidesPerRow;d++){var k=b*h+(c*a.options.slidesPerRow+d);g.get(k)&&j.appendChild(g.get(k))}i.appendChild(j)}e.appendChild(i)}a.$slider.html(e),a.$slider.children().children().children().width(100/a.options.slidesPerRow+"%").css({display:"inline-block"})}},b.prototype.checkResponsive=function(b){var d,e,f,c=this,g=c.$slider.width(),h=window.innerWidth||a(window).width();if("window"===c.respondTo?f=h:"slider"===c.respondTo?f=g:"min"===c.respondTo&&(f=Math.min(h,g)),c.originalSettings.responsive&&c.originalSettings.responsive.length>-1&&null!==c.originalSettings.responsive){e=null;for(d in c.breakpoints)c.breakpoints.hasOwnProperty(d)&&(c.originalSettings.mobileFirst===!1?f<c.breakpoints[d]&&(e=c.breakpoints[d]):f>c.breakpoints[d]&&(e=c.breakpoints[d]));null!==e?null!==c.activeBreakpoint?e!==c.activeBreakpoint&&(c.activeBreakpoint=e,"unslick"===c.breakpointSettings[e]?c.unslick():(c.options=a.extend({},c.originalSettings,c.breakpointSettings[e]),b===!0&&(c.currentSlide=c.options.initialSlide),c.refresh())):(c.activeBreakpoint=e,"unslick"===c.breakpointSettings[e]?c.unslick():(c.options=a.extend({},c.originalSettings,c.breakpointSettings[e]),b===!0&&(c.currentSlide=c.options.initialSlide),c.refresh())):null!==c.activeBreakpoint&&(c.activeBreakpoint=null,c.options=c.originalSettings,b===!0&&(c.currentSlide=c.options.initialSlide),c.refresh())}},b.prototype.changeSlide=function(b,c){var f,g,h,d=this,e=a(b.target);switch(e.is("a")&&b.preventDefault(),h=0!==d.slideCount%d.options.slidesToScroll,f=h?0:(d.slideCount-d.currentSlide)%d.options.slidesToScroll,b.data.message){case"previous":g=0===f?d.options.slidesToScroll:d.options.slidesToShow-f,d.slideCount>d.options.slidesToShow&&d.slideHandler(d.currentSlide-g,!1,c);break;case"next":g=0===f?d.options.slidesToScroll:f,d.slideCount>d.options.slidesToShow&&d.slideHandler(d.currentSlide+g,!1,c);break;case"index":var i=0===b.data.index?0:b.data.index||a(b.target).parent().index()*d.options.slidesToScroll;d.slideHandler(d.checkNavigable(i),!1,c);break;default:return}},b.prototype.checkNavigable=function(a){var c,d,b=this;if(c=b.getNavigableIndexes(),d=0,a>c[c.length-1])a=c[c.length-1];else for(var e in c){if(a<c[e]){a=d;break}d=c[e]}return a},b.prototype.cleanUpEvents=function(){var b=this;b.options.dots===!0&&b.slideCount>b.options.slidesToShow&&a("li",b.$dots).off("click.slick",b.changeSlide),b.options.dots===!0&&b.options.pauseOnDotsHover===!0&&b.options.autoplay===!0&&a("li",b.$dots).off("mouseenter.slick",b.setPaused.bind(b,!0)).off("mouseleave.slick",b.setPaused.bind(b,!1)),b.options.arrows===!0&&b.slideCount>b.options.slidesToShow&&(b.$prevArrow&&b.$prevArrow.off("click.slick",b.changeSlide),b.$nextArrow&&b.$nextArrow.off("click.slick",b.changeSlide)),b.$list.off("touchstart.slick mousedown.slick",b.swipeHandler),b.$list.off("touchmove.slick mousemove.slick",b.swipeHandler),b.$list.off("touchend.slick mouseup.slick",b.swipeHandler),b.$list.off("touchcancel.slick mouseleave.slick",b.swipeHandler),b.$list.off("click.slick",b.clickHandler),b.options.autoplay===!0&&a(document).off(b.visibilityChange,b.visibility),b.$list.off("mouseenter.slick",b.setPaused.bind(b,!0)),b.$list.off("mouseleave.slick",b.setPaused.bind(b,!1)),b.options.accessibility===!0&&b.$list.off("keydown.slick",b.keyHandler),b.options.focusOnSelect===!0&&a(b.$slideTrack).children().off("click.slick",b.selectHandler),a(window).off("orientationchange.slick.slick-"+b.instanceUid,b.orientationChange),a(window).off("resize.slick.slick-"+b.instanceUid,b.resize),a("[draggable!=true]",b.$slideTrack).off("dragstart",b.preventDefault),a(window).off("load.slick.slick-"+b.instanceUid,b.setPosition),a(document).off("ready.slick.slick-"+b.instanceUid,b.setPosition)},b.prototype.cleanUpRows=function(){var b,a=this;a.options.rows>1&&(b=a.$slides.children().children(),b.removeAttr("style"),a.$slider.html(b))},b.prototype.clickHandler=function(a){var b=this;b.shouldClick===!1&&(a.stopImmediatePropagation(),a.stopPropagation(),a.preventDefault())},b.prototype.destroy=function(){var b=this;b.autoPlayClear(),b.touchObject={},b.cleanUpEvents(),a(".slick-cloned",b.$slider).remove(),b.$dots&&b.$dots.remove(),b.$prevArrow&&"object"!=typeof b.options.prevArrow&&b.$prevArrow.remove(),b.$nextArrow&&"object"!=typeof b.options.nextArrow&&b.$nextArrow.remove(),b.$slides&&(b.$slides.removeClass("slick-slide slick-active slick-center slick-visible").attr("aria-hidden","true").removeAttr("data-slick-index").css({position:"",left:"",top:"",zIndex:"",opacity:"",width:""}),b.$slider.html(b.$slides)),b.cleanUpRows(),b.$slider.removeClass("slick-slider"),b.$slider.removeClass("slick-initialized")},b.prototype.disableTransition=function(a){var b=this,c={};c[b.transitionType]="",b.options.fade===!1?b.$slideTrack.css(c):b.$slides.eq(a).css(c)},b.prototype.fadeSlide=function(a,b){var c=this;c.cssTransitions===!1?(c.$slides.eq(a).css({zIndex:1e3}),c.$slides.eq(a).animate({opacity:1},c.options.speed,c.options.easing,b)):(c.applyTransition(a),c.$slides.eq(a).css({opacity:1,zIndex:1e3}),b&&setTimeout(function(){c.disableTransition(a),b.call()},c.options.speed))},b.prototype.filterSlides=b.prototype.slickFilter=function(a){var b=this;null!==a&&(b.unload(),b.$slideTrack.children(this.options.slide).detach(),b.$slidesCache.filter(a).appendTo(b.$slideTrack),b.reinit())},b.prototype.getCurrent=b.prototype.slickCurrentSlide=function(){var a=this;return a.currentSlide},b.prototype.getDotCount=function(){var a=this,b=0,c=0,d=0;if(a.options.infinite===!0)d=Math.ceil(a.slideCount/a.options.slidesToScroll);else if(a.options.centerMode===!0)d=a.slideCount;else for(;b<a.slideCount;)++d,b=c+a.options.slidesToShow,c+=a.options.slidesToScroll<=a.options.slidesToShow?a.options.slidesToScroll:a.options.slidesToShow;return d-1},b.prototype.getLeft=function(a){var c,d,f,b=this,e=0;return b.slideOffset=0,d=b.$slides.first().outerHeight(),b.options.infinite===!0?(b.slideCount>b.options.slidesToShow&&(b.slideOffset=-1*b.slideWidth*b.options.slidesToShow,e=-1*d*b.options.slidesToShow),0!==b.slideCount%b.options.slidesToScroll&&a+b.options.slidesToScroll>b.slideCount&&b.slideCount>b.options.slidesToShow&&(a>b.slideCount?(b.slideOffset=-1*(b.options.slidesToShow-(a-b.slideCount))*b.slideWidth,e=-1*(b.options.slidesToShow-(a-b.slideCount))*d):(b.slideOffset=-1*b.slideCount%b.options.slidesToScroll*b.slideWidth,e=-1*b.slideCount%b.options.slidesToScroll*d))):a+b.options.slidesToShow>b.slideCount&&(b.slideOffset=(a+b.options.slidesToShow-b.slideCount)*b.slideWidth,e=(a+b.options.slidesToShow-b.slideCount)*d),b.slideCount<=b.options.slidesToShow&&(b.slideOffset=0,e=0),b.options.centerMode===!0&&b.options.infinite===!0?b.slideOffset+=b.slideWidth*Math.floor(b.options.slidesToShow/2)-b.slideWidth:b.options.centerMode===!0&&(b.slideOffset=0,b.slideOffset+=b.slideWidth*Math.floor(b.options.slidesToShow/2)),c=b.options.vertical===!1?-1*a*b.slideWidth+b.slideOffset:-1*a*d+e,b.options.variableWidth===!0&&(f=b.slideCount<=b.options.slidesToShow||b.options.infinite===!1?b.$slideTrack.children(".slick-slide").eq(a):b.$slideTrack.children(".slick-slide").eq(a+b.options.slidesToShow),c=f[0]?-1*f[0].offsetLeft:0,b.options.centerMode===!0&&(f=b.options.infinite===!1?b.$slideTrack.children(".slick-slide").eq(a):b.$slideTrack.children(".slick-slide").eq(a+b.options.slidesToShow+1),c=f[0]?-1*f[0].offsetLeft:0,c+=(b.$list.width()-f.outerWidth())/2)),c},b.prototype.getOption=b.prototype.slickGetOption=function(a){var b=this;return b.options[a]},b.prototype.getNavigableIndexes=function(){var e,a=this,b=0,c=0,d=[];for(a.options.infinite===!1?(e=a.slideCount-a.options.slidesToShow+1,a.options.centerMode===!0&&(e=a.slideCount)):(b=-1*a.options.slidesToScroll,c=-1*a.options.slidesToScroll,e=2*a.slideCount);e>b;)d.push(b),b=c+a.options.slidesToScroll,c+=a.options.slidesToScroll<=a.options.slidesToShow?a.options.slidesToScroll:a.options.slidesToShow;return d},b.prototype.getSlick=function(){return this},b.prototype.getSlideCount=function(){var c,d,e,b=this;return e=b.options.centerMode===!0?b.slideWidth*Math.floor(b.options.slidesToShow/2):0,b.options.swipeToSlide===!0?(b.$slideTrack.find(".slick-slide").each(function(c,f){return f.offsetLeft-e+a(f).outerWidth()/2>-1*b.swipeLeft?(d=f,!1):void 0}),c=Math.abs(a(d).attr("data-slick-index")-b.currentSlide)||1):b.options.slidesToScroll},b.prototype.goTo=b.prototype.slickGoTo=function(a,b){var c=this;c.changeSlide({data:{message:"index",index:parseInt(a)}},b)},b.prototype.init=function(){var b=this;a(b.$slider).hasClass("slick-initialized")||(a(b.$slider).addClass("slick-initialized"),b.buildRows(),b.buildOut(),b.setProps(),b.startLoad(),b.loadSlider(),b.initializeEvents(),b.updateArrows(),b.updateDots()),b.$slider.trigger("init",[b])},b.prototype.initArrowEvents=function(){var a=this;a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.on("click.slick",{message:"previous"},a.changeSlide),a.$nextArrow.on("click.slick",{message:"next"},a.changeSlide))},b.prototype.initDotEvents=function(){var b=this;b.options.dots===!0&&b.slideCount>b.options.slidesToShow&&a("li",b.$dots).on("click.slick",{message:"index"},b.changeSlide),b.options.dots===!0&&b.options.pauseOnDotsHover===!0&&b.options.autoplay===!0&&a("li",b.$dots).on("mouseenter.slick",b.setPaused.bind(b,!0)).on("mouseleave.slick",b.setPaused.bind(b,!1))},b.prototype.initializeEvents=function(){var b=this;b.initArrowEvents(),b.initDotEvents(),b.$list.on("touchstart.slick mousedown.slick",{action:"start"},b.swipeHandler),b.$list.on("touchmove.slick mousemove.slick",{action:"move"},b.swipeHandler),b.$list.on("touchend.slick mouseup.slick",{action:"end"},b.swipeHandler),b.$list.on("touchcancel.slick mouseleave.slick",{action:"end"},b.swipeHandler),b.$list.on("click.slick",b.clickHandler),b.options.autoplay===!0&&a(document).on(b.visibilityChange,b.visibility.bind(b)),b.$list.on("mouseenter.slick",b.setPaused.bind(b,!0)),b.$list.on("mouseleave.slick",b.setPaused.bind(b,!1)),b.options.accessibility===!0&&b.$list.on("keydown.slick",b.keyHandler),b.options.focusOnSelect===!0&&a(b.$slideTrack).children().on("click.slick",b.selectHandler),a(window).on("orientationchange.slick.slick-"+b.instanceUid,b.orientationChange.bind(b)),a(window).on("resize.slick.slick-"+b.instanceUid,b.resize.bind(b)),a("[draggable!=true]",b.$slideTrack).on("dragstart",b.preventDefault),a(window).on("load.slick.slick-"+b.instanceUid,b.setPosition),a(document).on("ready.slick.slick-"+b.instanceUid,b.setPosition)},b.prototype.initUI=function(){var a=this;a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.show(),a.$nextArrow.show()),a.options.dots===!0&&a.slideCount>a.options.slidesToShow&&a.$dots.show(),a.options.autoplay===!0&&a.autoPlay()},b.prototype.keyHandler=function(a){var b=this;37===a.keyCode&&b.options.accessibility===!0?b.changeSlide({data:{message:"previous"}}):39===a.keyCode&&b.options.accessibility===!0&&b.changeSlide({data:{message:"next"}})},b.prototype.lazyLoad=function(){function g(b){a("img[data-lazy]",b).each(function(){var b=a(this),c=a(this).attr("data-lazy"),d=document.createElement("img");d.onload=function(){b.animate({opacity:1},200)},d.src=c,b.css({opacity:0}).attr("src",c).removeAttr("data-lazy").removeClass("slick-loading")})}var c,d,e,f,b=this;b.options.centerMode===!0?b.options.infinite===!0?(e=b.currentSlide+(b.options.slidesToShow/2+1),f=e+b.options.slidesToShow+2):(e=Math.max(0,b.currentSlide-(b.options.slidesToShow/2+1)),f=2+(b.options.slidesToShow/2+1)+b.currentSlide):(e=b.options.infinite?b.options.slidesToShow+b.currentSlide:b.currentSlide,f=e+b.options.slidesToShow,b.options.fade===!0&&(e>0&&e--,f<=b.slideCount&&f++)),c=b.$slider.find(".slick-slide").slice(e,f),g(c),b.slideCount<=b.options.slidesToShow?(d=b.$slider.find(".slick-slide"),g(d)):b.currentSlide>=b.slideCount-b.options.slidesToShow?(d=b.$slider.find(".slick-cloned").slice(0,b.options.slidesToShow),g(d)):0===b.currentSlide&&(d=b.$slider.find(".slick-cloned").slice(-1*b.options.slidesToShow),g(d))},b.prototype.loadSlider=function(){var a=this;a.setPosition(),a.$slideTrack.css({opacity:1}),a.$slider.removeClass("slick-loading"),a.initUI(),"progressive"===a.options.lazyLoad&&a.progressiveLazyLoad()},b.prototype.next=b.prototype.slickNext=function(){var a=this;a.changeSlide({data:{message:"next"}})},b.prototype.orientationChange=function(){var a=this;a.checkResponsive(),a.setPosition()},b.prototype.pause=b.prototype.slickPause=function(){var a=this;a.autoPlayClear(),a.paused=!0},b.prototype.play=b.prototype.slickPlay=function(){var a=this;a.paused=!1,a.autoPlay()},b.prototype.postSlide=function(a){var b=this;b.$slider.trigger("afterChange",[b,a]),b.animating=!1,b.setPosition(),b.swipeLeft=null,b.options.autoplay===!0&&b.paused===!1&&b.autoPlay()},b.prototype.prev=b.prototype.slickPrev=function(){var a=this;a.changeSlide({data:{message:"previous"}})},b.prototype.preventDefault=function(a){a.preventDefault()},b.prototype.progressiveLazyLoad=function(){var c,d,b=this;c=a("img[data-lazy]",b.$slider).length,c>0&&(d=a("img[data-lazy]",b.$slider).first(),d.attr("src",d.attr("data-lazy")).removeClass("slick-loading").load(function(){d.removeAttr("data-lazy"),b.progressiveLazyLoad(),b.options.adaptiveHeight===!0&&b.setPosition()}).error(function(){d.removeAttr("data-lazy"),b.progressiveLazyLoad()}))},b.prototype.refresh=function(){var b=this,c=b.currentSlide;b.destroy(),a.extend(b,b.initials),b.init(),b.changeSlide({data:{message:"index",index:c}},!1)},b.prototype.reinit=function(){var b=this;b.$slides=b.$slideTrack.children(b.options.slide).addClass("slick-slide"),b.slideCount=b.$slides.length,b.currentSlide>=b.slideCount&&0!==b.currentSlide&&(b.currentSlide=b.currentSlide-b.options.slidesToScroll),b.slideCount<=b.options.slidesToShow&&(b.currentSlide=0),b.setProps(),b.setupInfinite(),b.buildArrows(),b.updateArrows(),b.initArrowEvents(),b.buildDots(),b.updateDots(),b.initDotEvents(),b.options.focusOnSelect===!0&&a(b.$slideTrack).children().on("click.slick",b.selectHandler),b.setSlideClasses(0),b.setPosition(),b.$slider.trigger("reInit",[b])},b.prototype.resize=function(){var b=this;a(window).width()!==b.windowWidth&&(clearTimeout(b.windowDelay),b.windowDelay=window.setTimeout(function(){b.windowWidth=a(window).width(),b.checkResponsive(),b.setPosition()},50))},b.prototype.removeSlide=b.prototype.slickRemove=function(a,b,c){var d=this;return"boolean"==typeof a?(b=a,a=b===!0?0:d.slideCount-1):a=b===!0?--a:a,d.slideCount<1||0>a||a>d.slideCount-1?!1:(d.unload(),c===!0?d.$slideTrack.children().remove():d.$slideTrack.children(this.options.slide).eq(a).remove(),d.$slides=d.$slideTrack.children(this.options.slide),d.$slideTrack.children(this.options.slide).detach(),d.$slideTrack.append(d.$slides),d.$slidesCache=d.$slides,d.reinit(),void 0)},b.prototype.setCSS=function(a){var d,e,b=this,c={};b.options.rtl===!0&&(a=-a),d="left"==b.positionProp?Math.ceil(a)+"px":"0px",e="top"==b.positionProp?Math.ceil(a)+"px":"0px",c[b.positionProp]=a,b.transformsEnabled===!1?b.$slideTrack.css(c):(c={},b.cssTransitions===!1?(c[b.animType]="translate("+d+", "+e+")",b.$slideTrack.css(c)):(c[b.animType]="translate3d("+d+", "+e+", 0px)",b.$slideTrack.css(c)))},b.prototype.setDimensions=function(){var a=this;a.options.vertical===!1?a.options.centerMode===!0&&a.$list.css({padding:"0px "+a.options.centerPadding}):(a.$list.height(a.$slides.first().outerHeight(!0)*a.options.slidesToShow),a.options.centerMode===!0&&a.$list.css({padding:a.options.centerPadding+" 0px"})),a.listWidth=a.$list.width(),a.listHeight=a.$list.height(),a.options.vertical===!1&&a.options.variableWidth===!1?(a.slideWidth=Math.ceil(a.listWidth/a.options.slidesToShow),a.$slideTrack.width(Math.ceil(a.slideWidth*a.$slideTrack.children(".slick-slide").length))):a.options.variableWidth===!0?a.$slideTrack.width(5e3*a.slideCount):(a.slideWidth=Math.ceil(a.listWidth),a.$slideTrack.height(Math.ceil(a.$slides.first().outerHeight(!0)*a.$slideTrack.children(".slick-slide").length)));var b=a.$slides.first().outerWidth(!0)-a.$slides.first().width();a.options.variableWidth===!1&&a.$slideTrack.children(".slick-slide").width(a.slideWidth-b)},b.prototype.setFade=function(){var c,b=this;b.$slides.each(function(d,e){c=-1*b.slideWidth*d,b.options.rtl===!0?a(e).css({position:"relative",right:c,top:0,zIndex:800,opacity:0}):a(e).css({position:"relative",left:c,top:0,zIndex:800,opacity:0})}),b.$slides.eq(b.currentSlide).css({zIndex:900,opacity:1})},b.prototype.setHeight=function(){var a=this;if(1===a.options.slidesToShow&&a.options.adaptiveHeight===!0&&a.options.vertical===!1){var b=a.$slides.eq(a.currentSlide).outerHeight(!0);a.$list.css("height",b)}},b.prototype.setOption=b.prototype.slickSetOption=function(a,b,c){var d=this;d.options[a]=b,c===!0&&(d.unload(),d.reinit())},b.prototype.setPosition=function(){var a=this;a.setDimensions(),a.setHeight(),a.options.fade===!1?a.setCSS(a.getLeft(a.currentSlide)):a.setFade(),a.$slider.trigger("setPosition",[a])},b.prototype.setProps=function(){var a=this,b=document.body.style;a.positionProp=a.options.vertical===!0?"top":"left","top"===a.positionProp?a.$slider.addClass("slick-vertical"):a.$slider.removeClass("slick-vertical"),(void 0!==b.WebkitTransition||void 0!==b.MozTransition||void 0!==b.msTransition)&&a.options.useCSS===!0&&(a.cssTransitions=!0),void 0!==b.OTransform&&(a.animType="OTransform",a.transformType="-o-transform",a.transitionType="OTransition",void 0===b.perspectiveProperty&&void 0===b.webkitPerspective&&(a.animType=!1)),void 0!==b.MozTransform&&(a.animType="MozTransform",a.transformType="-moz-transform",a.transitionType="MozTransition",void 0===b.perspectiveProperty&&void 0===b.MozPerspective&&(a.animType=!1)),void 0!==b.webkitTransform&&(a.animType="webkitTransform",a.transformType="-webkit-transform",a.transitionType="webkitTransition",void 0===b.perspectiveProperty&&void 0===b.webkitPerspective&&(a.animType=!1)),void 0!==b.msTransform&&(a.animType="msTransform",a.transformType="-ms-transform",a.transitionType="msTransition",void 0===b.msTransform&&(a.animType=!1)),void 0!==b.transform&&a.animType!==!1&&(a.animType="transform",a.transformType="transform",a.transitionType="transition"),a.transformsEnabled=null!==a.animType&&a.animType!==!1},b.prototype.setSlideClasses=function(a){var c,d,e,f,b=this;b.$slider.find(".slick-slide").removeClass("slick-active").attr("aria-hidden","true").removeClass("slick-center"),d=b.$slider.find(".slick-slide"),b.options.centerMode===!0?(c=Math.floor(b.options.slidesToShow/2),b.options.infinite===!0&&(a>=c&&a<=b.slideCount-1-c?b.$slides.slice(a-c,a+c+1).addClass("slick-active").attr("aria-hidden","false"):(e=b.options.slidesToShow+a,d.slice(e-c+1,e+c+2).addClass("slick-active").attr("aria-hidden","false")),0===a?d.eq(d.length-1-b.options.slidesToShow).addClass("slick-center"):a===b.slideCount-1&&d.eq(b.options.slidesToShow).addClass("slick-center")),b.$slides.eq(a).addClass("slick-center")):a>=0&&a<=b.slideCount-b.options.slidesToShow?b.$slides.slice(a,a+b.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false"):d.length<=b.options.slidesToShow?d.addClass("slick-active").attr("aria-hidden","false"):(f=b.slideCount%b.options.slidesToShow,e=b.options.infinite===!0?b.options.slidesToShow+a:a,b.options.slidesToShow==b.options.slidesToScroll&&b.slideCount-a<b.options.slidesToShow?d.slice(e-(b.options.slidesToShow-f),e+f).addClass("slick-active").attr("aria-hidden","false"):d.slice(e,e+b.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false")),"ondemand"===b.options.lazyLoad&&b.lazyLoad()},b.prototype.setupInfinite=function(){var c,d,e,b=this;if(b.options.fade===!0&&(b.options.centerMode=!1),b.options.infinite===!0&&b.options.fade===!1&&(d=null,b.slideCount>b.options.slidesToShow)){for(e=b.options.centerMode===!0?b.options.slidesToShow+1:b.options.slidesToShow,c=b.slideCount;c>b.slideCount-e;c-=1)d=c-1,a(b.$slides[d]).clone(!0).attr("id","").attr("data-slick-index",d-b.slideCount).prependTo(b.$slideTrack).addClass("slick-cloned");for(c=0;e>c;c+=1)d=c,a(b.$slides[d]).clone(!0).attr("id","").attr("data-slick-index",d+b.slideCount).appendTo(b.$slideTrack).addClass("slick-cloned");b.$slideTrack.find(".slick-cloned").find("[id]").each(function(){a(this).attr("id","")})}},b.prototype.setPaused=function(a){var b=this;b.options.autoplay===!0&&b.options.pauseOnHover===!0&&(b.paused=a,b.autoPlayClear())},b.prototype.selectHandler=function(b){var c=this,d=a(b.target).is(".slick-slide")?a(b.target):a(b.target).parents(".slick-slide"),e=parseInt(d.attr("data-slick-index"));return e||(e=0),c.slideCount<=c.options.slidesToShow?(c.$slider.find(".slick-slide").removeClass("slick-active").attr("aria-hidden","true"),c.$slides.eq(e).addClass("slick-active").attr("aria-hidden","false"),c.options.centerMode===!0&&(c.$slider.find(".slick-slide").removeClass("slick-center"),c.$slides.eq(e).addClass("slick-center")),c.asNavFor(e),void 0):(c.slideHandler(e),void 0)},b.prototype.slideHandler=function(a,b,c){var d,e,f,g,h=null,i=this;return b=b||!1,i.animating===!0&&i.options.waitForAnimate===!0||i.options.fade===!0&&i.currentSlide===a||i.slideCount<=i.options.slidesToShow?void 0:(b===!1&&i.asNavFor(a),d=a,h=i.getLeft(d),g=i.getLeft(i.currentSlide),i.currentLeft=null===i.swipeLeft?g:i.swipeLeft,i.options.infinite===!1&&i.options.centerMode===!1&&(0>a||a>i.getDotCount()*i.options.slidesToScroll)?(i.options.fade===!1&&(d=i.currentSlide,c!==!0?i.animateSlide(g,function(){i.postSlide(d)}):i.postSlide(d)),void 0):i.options.infinite===!1&&i.options.centerMode===!0&&(0>a||a>i.slideCount-i.options.slidesToScroll)?(i.options.fade===!1&&(d=i.currentSlide,c!==!0?i.animateSlide(g,function(){i.postSlide(d)}):i.postSlide(d)),void 0):(i.options.autoplay===!0&&clearInterval(i.autoPlayTimer),e=0>d?0!==i.slideCount%i.options.slidesToScroll?i.slideCount-i.slideCount%i.options.slidesToScroll:i.slideCount+d:d>=i.slideCount?0!==i.slideCount%i.options.slidesToScroll?0:d-i.slideCount:d,i.animating=!0,i.$slider.trigger("beforeChange",[i,i.currentSlide,e]),f=i.currentSlide,i.currentSlide=e,i.setSlideClasses(i.currentSlide),i.updateDots(),i.updateArrows(),i.options.fade===!0?(c!==!0?i.fadeSlide(e,function(){i.postSlide(e)}):i.postSlide(e),i.animateHeight(),void 0):(c!==!0?i.animateSlide(h,function(){i.postSlide(e)}):i.postSlide(e),void 0)))},b.prototype.startLoad=function(){var a=this;a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.hide(),a.$nextArrow.hide()),a.options.dots===!0&&a.slideCount>a.options.slidesToShow&&a.$dots.hide(),a.$slider.addClass("slick-loading")},b.prototype.swipeDirection=function(){var a,b,c,d,e=this;return a=e.touchObject.startX-e.touchObject.curX,b=e.touchObject.startY-e.touchObject.curY,c=Math.atan2(b,a),d=Math.round(180*c/Math.PI),0>d&&(d=360-Math.abs(d)),45>=d&&d>=0?e.options.rtl===!1?"left":"right":360>=d&&d>=315?e.options.rtl===!1?"left":"right":d>=135&&225>=d?e.options.rtl===!1?"right":"left":e.options.verticalSwiping===!0?d>=35&&135>=d?"left":"right":"vertical"},b.prototype.swipeEnd=function(){var c,b=this;if(b.dragging=!1,b.shouldClick=b.touchObject.swipeLength>10?!1:!0,void 0===b.touchObject.curX)return!1;if(b.touchObject.edgeHit===!0&&b.$slider.trigger("edge",[b,b.swipeDirection()]),b.touchObject.swipeLength>=b.touchObject.minSwipe)switch(b.swipeDirection()){case"left":c=b.options.swipeToSlide?b.checkNavigable(b.currentSlide+b.getSlideCount()):b.currentSlide+b.getSlideCount(),b.slideHandler(c),b.currentDirection=0,b.touchObject={},b.$slider.trigger("swipe",[b,"left"]);break;case"right":c=b.options.swipeToSlide?b.checkNavigable(b.currentSlide-b.getSlideCount()):b.currentSlide-b.getSlideCount(),b.slideHandler(c),b.currentDirection=1,b.touchObject={},b.$slider.trigger("swipe",[b,"right"])}else b.touchObject.startX!==b.touchObject.curX&&(b.slideHandler(b.currentSlide),b.touchObject={})},b.prototype.swipeHandler=function(a){var b=this;
    if(!(b.options.swipe===!1||"ontouchend"in document&&b.options.swipe===!1||b.options.draggable===!1&&-1!==a.type.indexOf("mouse")))switch(b.touchObject.fingerCount=a.originalEvent&&void 0!==a.originalEvent.touches?a.originalEvent.touches.length:1,b.touchObject.minSwipe=b.listWidth/b.options.touchThreshold,b.options.verticalSwiping===!0&&(b.touchObject.minSwipe=b.listHeight/b.options.touchThreshold),a.data.action){case"start":b.swipeStart(a);break;case"move":b.swipeMove(a);break;case"end":b.swipeEnd(a)}},b.prototype.swipeMove=function(a){var d,e,f,g,h,b=this;return h=void 0!==a.originalEvent?a.originalEvent.touches:null,!b.dragging||h&&1!==h.length?!1:(d=b.getLeft(b.currentSlide),b.touchObject.curX=void 0!==h?h[0].pageX:a.clientX,b.touchObject.curY=void 0!==h?h[0].pageY:a.clientY,b.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(b.touchObject.curX-b.touchObject.startX,2))),b.options.verticalSwiping===!0&&(b.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(b.touchObject.curY-b.touchObject.startY,2)))),e=b.swipeDirection(),"vertical"!==e?(void 0!==a.originalEvent&&b.touchObject.swipeLength>4&&a.preventDefault(),g=(b.options.rtl===!1?1:-1)*(b.touchObject.curX>b.touchObject.startX?1:-1),b.options.verticalSwiping===!0&&(g=b.touchObject.curY>b.touchObject.startY?1:-1),f=b.touchObject.swipeLength,b.touchObject.edgeHit=!1,b.options.infinite===!1&&(0===b.currentSlide&&"right"===e||b.currentSlide>=b.getDotCount()&&"left"===e)&&(f=b.touchObject.swipeLength*b.options.edgeFriction,b.touchObject.edgeHit=!0),b.swipeLeft=b.options.vertical===!1?d+f*g:d+f*(b.$list.height()/b.listWidth)*g,b.options.verticalSwiping===!0&&(b.swipeLeft=d+f*g),b.options.fade===!0||b.options.touchMove===!1?!1:b.animating===!0?(b.swipeLeft=null,!1):(b.setCSS(b.swipeLeft),void 0)):void 0)},b.prototype.swipeStart=function(a){var c,b=this;return 1!==b.touchObject.fingerCount||b.slideCount<=b.options.slidesToShow?(b.touchObject={},!1):(void 0!==a.originalEvent&&void 0!==a.originalEvent.touches&&(c=a.originalEvent.touches[0]),b.touchObject.startX=b.touchObject.curX=void 0!==c?c.pageX:a.clientX,b.touchObject.startY=b.touchObject.curY=void 0!==c?c.pageY:a.clientY,b.dragging=!0,void 0)},b.prototype.unfilterSlides=b.prototype.slickUnfilter=function(){var a=this;null!==a.$slidesCache&&(a.unload(),a.$slideTrack.children(this.options.slide).detach(),a.$slidesCache.appendTo(a.$slideTrack),a.reinit())},b.prototype.unload=function(){var b=this;a(".slick-cloned",b.$slider).remove(),b.$dots&&b.$dots.remove(),b.$prevArrow&&"object"!=typeof b.options.prevArrow&&b.$prevArrow.remove(),b.$nextArrow&&"object"!=typeof b.options.nextArrow&&b.$nextArrow.remove(),b.$slides.removeClass("slick-slide slick-active slick-visible").attr("aria-hidden","true").css("width","")},b.prototype.unslick=function(){var a=this;a.destroy()},b.prototype.updateArrows=function(){var b,a=this;b=Math.floor(a.options.slidesToShow/2),a.options.arrows===!0&&a.options.infinite!==!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.removeClass("slick-disabled"),a.$nextArrow.removeClass("slick-disabled"),0===a.currentSlide?(a.$prevArrow.addClass("slick-disabled"),a.$nextArrow.removeClass("slick-disabled")):a.currentSlide>=a.slideCount-a.options.slidesToShow&&a.options.centerMode===!1?(a.$nextArrow.addClass("slick-disabled"),a.$prevArrow.removeClass("slick-disabled")):a.currentSlide>=a.slideCount-1&&a.options.centerMode===!0&&(a.$nextArrow.addClass("slick-disabled"),a.$prevArrow.removeClass("slick-disabled")))},b.prototype.updateDots=function(){var a=this;null!==a.$dots&&(a.$dots.find("li").removeClass("slick-active").attr("aria-hidden","true"),a.$dots.find("li").eq(Math.floor(a.currentSlide/a.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden","false"))},b.prototype.visibility=function(){var a=this;document[a.hidden]?(a.paused=!0,a.autoPlayClear()):(a.paused=!1,a.autoPlay())},a.fn.slick=function(){var g,a=this,c=arguments[0],d=Array.prototype.slice.call(arguments,1),e=a.length,f=0;for(f;e>f;f++)if("object"==typeof c||"undefined"==typeof c?a[f].slick=new b(a[f],c):g=a[f].slick[c].apply(a[f].slick,d),"undefined"!=typeof g)return g;return a}});

//ScrollTo
;(function(f){"use strict";"function"===typeof define&&define.amd?define(["jquery"],f):"undefined"!==typeof module&&module.exports?module.exports=f(require("jquery")):f(jQuery)})(function($){"use strict";function n(a){return!a.nodeName||-1!==$.inArray(a.nodeName.toLowerCase(),["iframe","#document","html","body"])}function h(a){return $.isFunction(a)||$.isPlainObject(a)?a:{top:a,left:a}}var p=$.scrollTo=function(a,d,b){return $(window).scrollTo(a,d,b)};p.defaults={axis:"xy",duration:0,limit:!0};$.fn.scrollTo=function(a,d,b){"object"=== typeof d&&(b=d,d=0);"function"===typeof b&&(b={onAfter:b});"max"===a&&(a=9E9);b=$.extend({},p.defaults,b);d=d||b.duration;var u=b.queue&&1<b.axis.length;u&&(d/=2);b.offset=h(b.offset);b.over=h(b.over);return this.each(function(){function k(a){var k=$.extend({},b,{queue:!0,duration:d,complete:a&&function(){a.call(q,e,b)}});r.animate(f,k)}if(null!==a){var l=n(this),q=l?this.contentWindow||window:this,r=$(q),e=a,f={},t;switch(typeof e){case "number":case "string":if(/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(e)){e= h(e);break}e=l?$(e):$(e,q);if(!e.length)return;case "object":if(e.is||e.style)t=(e=$(e)).offset()}var v=$.isFunction(b.offset)&&b.offset(q,e)||b.offset;$.each(b.axis.split(""),function(a,c){var d="x"===c?"Left":"Top",m=d.toLowerCase(),g="scroll"+d,h=r[g](),n=p.max(q,c);t?(f[g]=t[m]+(l?0:h-r.offset()[m]),b.margin&&(f[g]-=parseInt(e.css("margin"+d),10)||0,f[g]-=parseInt(e.css("border"+d+"Width"),10)||0),f[g]+=v[m]||0,b.over[m]&&(f[g]+=e["x"===c?"width":"height"]()*b.over[m])):(d=e[m],f[g]=d.slice&& "%"===d.slice(-1)?parseFloat(d)/100*n:d);b.limit&&/^\d+$/.test(f[g])&&(f[g]=0>=f[g]?0:Math.min(f[g],n));!a&&1<b.axis.length&&(h===f[g]?f={}:u&&(k(b.onAfterFirst),f={}))});k(b.onAfter)}})};p.max=function(a,d){var b="x"===d?"Width":"Height",h="scroll"+b;if(!n(a))return a[h]-$(a)[b.toLowerCase()]();var b="client"+b,k=a.ownerDocument||a.document,l=k.documentElement,k=k.body;return Math.max(l[h],k[h])-Math.min(l[b],k[b])};$.Tween.propHooks.scrollLeft=$.Tween.propHooks.scrollTop={get:function(a){return $(a.elem)[a.prop]()}, set:function(a){var d=this.get(a);if(a.options.interrupt&&a._last&&a._last!==d)return $(a.elem).stop();var b=Math.round(a.now);d!==b&&($(a.elem)[a.prop](b),a._last=this.get(a))}};return p});

$(document).ready(function() {

    /**
     * Setting for Slick slider - used on mobile view
     *
     * @author Thomas Forster <thomas.forster@newspress.co.uk>
     */

    if(mobileView){
        $.when( $('#slick-slider').removeAttr('class') ).done(function( x ) {
            $('#slick-slider').slick({
                infinite: false,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1,
                prevArrow: '<button type="button" class="slick-prev mobile-scroll__control mobile-scroll__control--left icon icon--arrow-slider-left"></button>',
                nextArrow: '<button type="button" class="slick-next mobile-scroll__control mobile-scroll__control--right icon icon--arrow-slider-right"></button>'

            });
        });
    }

    /**
     * Event page - Documents layout (mobile view only)
     *
     * @author Thomas Forster <thomas.forster@newspress.co.uk>
     */
    $( "#accordion" ).accordion({
        heightStyle: "content",
        activate: function( event, ui ) {

            var page = $("html, body"),
                offSet = $('.header').outerHeight(true);
            page.animate({ scrollTop: (ui.newHeader.offset().top - offSet)  -64 }, 300, function(){
                page.off("scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove");
            });
        }
    });


    $('.login-wrapper .next-step, .login-wrapper .previous-step').click(function(event) {
        var direction = '+';
        if ($(this).hasClass('previous-step')) {
            direction = '-';
        }
        $('.login-wrapper .form__section-wrapper')
            .not(':animated')
            .animate({
                scrollLeft: direction + '=380px'
            }, 300, function() {
                if (changeSignUpStep(direction)) {
                    changeSignUpButton('submit');
                } else {
                    changeSignUpButton('button');
                }
            });
    });

    /**
     * Changes the step number for the sign up process
     *
     * @param  string direction The direction of the page change
     * @return boolean          Returns true if we are on the last page
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    var changeSignUpStep = function(direction) {

        var page = parseInt($('.form__note--page').text(), 10);
        var total = parseInt($('.form__note--total').text(), 10);

        if (direction == '+') {
            if (page < total) {
                page++;
            }
        } else {
            if (page > 1) {
                page--;
            }
        }

        $('.form__note--page').text(page);
        return (page === total);
    };

    /**
     * Change the button type (if the form is ready to submit)
     *
     * @param  string type The type of the button we want to change it to
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    var changeSignUpButton = function(type) {
        var $button = $('.login-wrapper .next-step');
        if (type == 'submit') {
            if ($button.attr('type') == 'button') {
                $('.login-wrapper .next-step').attr('type', 'submit').text('Sign Up');
            }
        } else {
            if ($button.attr('type') == 'submit') {
                $('.login-wrapper .next-step').attr('type', 'button').text('Next Step');
            }
        }
    };

    /**
     * Focuses the search input form when clicking on the .search class
     *
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    $('.search').click(function() {
        $(this).find('.search-input').focus();
    });

    /**
     * Highlights all text within a textarea or input field
     *
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    $(".action--select-all").on("click", function() {
        $(this).select();
    });

    /**
     * Homepage slider (not for the carousel)
     *
     * @todo Think of a name for me
     * @author Richard Skene <richard.skene@newspress.co.uk>
     */
    function changeMultipleSlider() {
        var $third = $('.slideshow--multiple .slideshow__item:eq(2)').clone(),
            $first = $('.slideshow--multiple .slideshow__item:eq(0)').clone(),
            $second = $('.slideshow--multiple .slideshow__item:eq(1)').clone();
        var $next;
        $('.slideshow--multiple .slideshow__item').each(function(i) {
            if (i === 0) {
                $next = $third;
            } else if (i === 1) {
                $next = $first;
            } else if (i === 2) {
                $next = $second;
            }
            $next.find('a').addClass('slideshow__item--clone');

            $(this).prepend($next.find('.slideshow__item--clone')).find('.slideshow__item--clone').animate({
                opacity: 1
            }, 1500);

            $(this).find('a:last').animate({
                opacity: 0
            }, 1500, function() {
                $(this).parent().find('.slideshow__item--clone').removeClass('slideshow__item--clone');
                $(this).remove();
            });
        });
    }

    if ($('.slideshow--multiple').length) {
        setInterval(function() {
            changeMultipleSlider();
        }, 5000);
    }

    /**
     * Changes the mediaQuery variable to the correct type based on the current
     * page width. The mediaQuery variable is defined globally so can be used
     * in other functions.
     *
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    function changeMediaQuery() {
        var windowWidth = $(window).width();
        if (windowWidth <= 480 && mediaQuery != 'mobile') {
            mediaQuery = 'mobile';
        } else if ((windowWidth > 480 && windowWidth <= 769) && mediaQuery != 'tablet') {
            mediaQuery = 'tablet';
        } else if ((windowWidth > 769 && windowWidth <= 1039) && mediaQuery != 'portable') {
            mediaQuery = 'portable';
        } else if (windowWidth >= 1040 && mediaQuery != 'desktop') {
            mediaQuery = 'desktop';
        }
    }

    changeMediaQuery();

    $(window).resize(function() {
        changeMediaQuery();
    });

    /**
     * Allow navigation list items to click through to the location of the a tag
     *
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    $('.header__nav--top, .header__nav').find('ul li').click(function() {
        var href = $(this).find('a').attr('href');
        if (href && href != '#' && href != '/#') {
            // Add target="_blank" support with window.open()
            // window.location = href;
        }
    });

    /**
     * Remove a tag from the search filters
     *
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    $('.tags__tag__remove').click(function() {

        var name = $(this).parent().data('name');

        $(this).parent().remove();
        $('.filters select[name=\'' + name + '\']').find('option[value=\'\']').prop('selected', 'selected');
        $('.filters input[name=\'' + name + '\']').val('');
        $('.filters--form').submit();
    });


    /**
     * Changes the class names of the associated grid for the gallery
     *
     * It will change the view for the gallery grid from one-third to one-quarter
     *
     * @todo   Add styling for download partial when at one-quarter
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    $('.sort__size').click(function() {

        // Show all items
        if ($.trim($(this).html()).toLowerCase() == 'all') {
            if ($('input[name=show-all]').val() == '') {
                $('input[name=show-all]').val('true');
                $('input[name=show-all]').addClass('sort__size--show-all-active');
                $('input[name=show-all]').removeClass('sort__size--show-all-inactive');
            } else {
                $('input[name=show-all]').val('');
                $('input[name=show-all]').addClass('sort__size--show-all-inactive');
                $('input[name=show-all]').removeClass('sort__size--show-all-active');
            }
            $(this).closest('form').submit();
            return;
        }

        $('.media').removeClass('media--active');
        $('.media-download').hide(0);
        $('.media-download').removeClass('media-download--second media-download--third media-download--fourth media-download__threes media-download__fours');

        if ($(this).hasClass('icon--grid-large-inactive')) {
            $(this)
                .toggleClass('icon--grid-large-inactive')
                .toggleClass('icon--grid-large-active');

            $('.sort__size.icon--grid-small-active')
                .toggleClass('icon--grid-small-active')
                .toggleClass('icon--grid-small-inactive');

            $(this)
                .closest('form')
                .next()
                .children('.grid')
                .children('.grid .grid__item').each(function() {
                    $(this).toggleClass('one-third l-one-third one-quarter l-one-quarter');
                });


            //IE8 HACK
            $('.media__thumbnail-title').css({width: '300px'});
        }

        if ($(this).hasClass('icon--grid-small-inactive')) {
            $(this)
                .toggleClass('icon--grid-small-inactive')
                .toggleClass('icon--grid-small-active');

            $('.sort__size.icon--grid-large-active')
                .toggleClass('icon--grid-large-active')
                .toggleClass('icon--grid-large-inactive');

            $(this)
                .closest('form')
                .next()
                .children('.grid')
                .children('.grid .grid__item').each(function() {
                    $(this).toggleClass('one-third l-one-third one-quarter l-one-quarter');
                });

            //IE8 HACK
            $('.media__thumbnail-title').css({width: '215px'});
        }

    });

    /**
     * Change value of hidden checkbox for type filter
     *
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    $('.sort__label--checkbox').change(function() {
        var types = [];
        $('.sort__label--checkbox').each(function(i) {
            var input = $(this).find('input');
            if (input.prop('checked')) {
                types.push(input.val());
            }
        });
        $('.filters input[name=\'type\']').val(types.join(','));
        $('.filters form').submit();
    });

    /**
     * Enable fixed header after scrolling has gone past the top navigation
     *
     * Calculate the offset of the .header__nav--top element (the calculated
     * height above the fixed navigation). Using this value, check if the
     * navigation is fixed after the DOM elements have loaded. On scroll,
     * this value will be checked to apply the header__nav--fixed class.
     * This way we are not calling the addClass/removeClass more times than
     * we need. If this function is changed to use a different function, we
     * do not have to worry about it causing problems or using more CPU
     * within the user's browser.
     *
     * @todo   Add last navigationIsFixed to check before running the function
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    var navigationIsFixed = false;
    var mobileNavigationIsAbsolute = false;

    var mobileOffset = 0;
    var offset = $('.header__nav--top').height();

    if ($(window).scrollTop() > offset) {
        navigationIsFixed = true;
    }

    function fixHeaderNavigation(navigationIsFixed) {
        if (navigationIsFixed) {
            $('.header').addClass('header--fixed');
        } else {
            $('.header').removeClass('header--fixed');
        }
    }

    function updateMobileNavigationPosition(action) {
        if (action == 'click') {
            mobileOffset = $(window).scrollTop();
        }
        if ($('.header__nav').hasClass('header__nav--show')) {
            if ($(window).scrollTop() > mobileOffset) {
                mobileNavigationIsAbsolute = true;
            } else {
                mobileNavigationIsAbsolute = false;
            }
        } else {
            mobileNavigationIsAbsolute = false;
        }

        if (mobileNavigationIsAbsolute) {
            $('.header').addClass('header--fixed--mobile');
            $('.header__nav--show').css('top', mobileOffset + 'px');
        } else {
            $('.header__nav').removeClass('no-animate');
            setTimeout(function() {
                if (action == 'scroll') {
                    $('.header__nav').addClass('no-animate');
                }
                $('.header').removeClass('header--fixed--mobile');
                $('.header__nav').css('top', '0px');
            }, (action == 'click') ? 300 : 0);
        }
    }

    fixHeaderNavigation(navigationIsFixed);

    // @todo: If the window is resized after the page is loaded (from mobile) the
    // height of the offset variable will be incorrect which will cause the nav
    // to jump when toggling the header--fixed class

    $(window).scroll(function() {
        if ($(window).scrollTop() > offset) {
            navigationIsFixed = true;
        } else {
            navigationIsFixed = false;
        }
        fixHeaderNavigation(navigationIsFixed);
        updateMobileNavigationPosition('scroll');
    });

    /**
     * Setting for mobile slider
     *
     * @author Thomas Forster <thomas.forster@newspress.co.uk>
     */
    $('#mobileSlider').slick({

        autoplay: true,
        autoplaySpeed: 2500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        pauseOnHover: false
    });

    /**
     * Scrolls to given point on page
     *
     * @author Thomas Forster <thomas.forster@newspress.co.uk>
     */
    function scrollToElement(to, ease, delay){

        var ease = (ease===undefined ? 1000 : ease),
            delay = (delay===undefined ? 0 : delay);

        var page = $("html, body"),
            offSet = $('.header').outerHeight(true);

        page.on("scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove", function(){
            page.stop();
        });

        page.delay(delay).animate({ scrollTop: to }, ease, function(){
            page.off("scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove");
        });
    }



    /**
     * Pin release sidebar on scroll
     * NOTE: functionality will not be implemented if the browser is IE8
     *
     * @return void
     * @author Richard Skene <richard.skene@newspress.co.uk>
     */

    if(!isIe8){
        var sidebarPinned = false,
            sidebarAbsolute = false,
            releaseSidebarTopOffset = 20,
            sidebarOriginal;

        function adjustReleaseSidebarClones() {
            var sidebarOriginalWidth = sidebarOriginal.outerWidth(true),
                sidebarOriginalLeftOffset = sidebarOriginal.offset().left;
            $('.release-sidebar--fixed').css({
                'width': sidebarOriginalWidth,
                'left': sidebarOriginalLeftOffset
            });
            $('.release-sidebar--absolute').css('width', sidebarOriginalWidth);
        }

        function cloneReleaseSidebar() {
            sidebarOriginal = $('.release-sidebar');
            sidebarOriginal.clone().insertAfter('.release-sidebar').addClass('release-sidebar--absolute').addClass('release-sidebar--invisible');
            sidebarOriginal.clone().insertAfter('.release-sidebar--absolute').addClass('release-sidebar--fixed').addClass('release-sidebar--invisible');
            sidebarOriginal.addClass('release-sidebar--original');
            adjustReleaseSidebarClones();
        }

        if ($('.release-sidebar').length > 0) {
            cloneReleaseSidebar();
            adjustReleaseSidebarClones();
        }

        function changeSidebar(type) {
            $('.release-sidebar').addClass('release-sidebar--invisible');
            $('.release-sidebar' + type).removeClass('release-sidebar--invisible');
        }

        function adjustSidebar() {
            var releasePositionOffsetTop = ($('.header').outerHeight(true) === 0 ? $('.header__nav--mobile').outerHeight(true) : $('.header').outerHeight(true)),
                windowScroll = $(window).scrollTop() + releaseSidebarTopOffset,
                releaseSidebarPosition = sidebarOriginal.position().top + releasePositionOffsetTop,
                releaseSidebarAbsolutePosition = $('.release-sidebar--absolute').position().top + releasePositionOffsetTop;

            if (windowScroll > releaseSidebarPosition && windowScroll < releaseSidebarAbsolutePosition && !sidebarPinned) {
                changeSidebar('--fixed');
                sidebarPinned = true;
                sidebarAbsolute = false;

            } else if (windowScroll < releaseSidebarPosition && sidebarPinned) {
                changeSidebar('--original');
                sidebarPinned = false;
                sidebarAbsolute = false;

            } else if (releaseSidebarAbsolutePosition <= windowScroll && !sidebarAbsolute) {
                changeSidebar('--absolute');
                sidebarPinned = false;
                sidebarAbsolute = true;

            }
        }

        $(window).scroll(function() {
            if ($('.release-sidebar').length > 0) {
                adjustSidebar();
            }
        });

        $(window).resize(function() {
            if ($('.release-sidebar').length > 0) {
                adjustReleaseSidebarClones();
                adjustSidebar();
            }
        });
    }


    /**
     * Fix footer to bottom of screen if content smaller then viewport
     *
     * @return void
     * @author Richard Skene <richard.skene@newspress.co.uk>
     */
    function positionFooter() {
        var contentsHeight = $('.content').outerHeight(true) + $('.header').outerHeight(true) + $('.footer').outerHeight(true);
        if (contentsHeight < $(window).height()) {
            $('.footer').addClass('footer--fixed');
        } else {
            $('.footer').removeClass('footer--fixed');
        }
    }

    $(window).load(function() {
        positionFooter();
    });

    $(window).resize(function() {
        positionFooter();
    });

    /**
     * Show the mobile navigation sub dropdown when title is clicked
     *
     * @return void
     * @author Richard Skene <richard.skene@newspress.co.uk>
     */
    $('.dropdown__title').click(function(event) {
        if (mediaQuery == 'tablet' || mediaQuery == 'mobile') {
            event.preventDefault();
            $(this).toggleClass('dropdown--active');
            $(this).next('ul').toggleClass('active');
        }
    });

    /**
     * Show the mobile navigation main dropdown when select-dropdown is clicked
     *
     * @return void
     * @author Richard Skene <richard.skene@newspress.co.uk>
     */
    $('.select-dropdown').click(function(event) {
        if (mediaQuery == 'tablet' || mediaQuery == 'mobile') {
            $(this).toggleClass('dropdown--active');
            event.preventDefault();
            return false;
        }
    });

    /**
     * Show the mobile navigation when tapping the hamburger icon
     *
     * @todo   Remove the event.preventDefault() function (add core JS)
     * @todo   Fix issue with height being 100% when scrollTop is past 0
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */

     var updateNavigation = function(){
        $('.header__nav').removeClass('no-animate');

        event.preventDefault();

        $('.nav-overlay').toggle();

        $('.header__nav').toggleClass('header__nav--show');
        $('body').toggleClass('body--mobile__show-nav');
        updateMobileNavigationPosition('click');
    }


    $('.mobile__nav, .nav-overlay').click(updateNavigation);


    /**
     * Stop propagation from reaching the li element which will open the main link
     *
     * @todo   Consider removig this from global JS file
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    $('.dropdown').click(function(event) {
        event.stopPropagation();
    });

    /**
     * Display the hovered dropdown link as preview
     *
     * @todo   Change image function to take from data attribute
     * @todo   Change the selector from an a tag to an li
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    $('.dropdown a').hover(function() {
        var title = $(this).text(),
            dropdown = $(this).closest(".dropdown"),
            image = $(this).data('image');

        if (image === undefined) {
            image = '/img/placeholders/models/dropdown/' + title.toLowerCase()
                .split(' ').join('-')
                .split('/').join('-')
                .replace('™', '') + '.jpg';
        }

        dropdown.find('.dropdown__subtitle').text(title);
        dropdown.find('.dropdown__image-preview').attr('src', image);
    });

    /**
     * Slideshow initilisation and settings
     * @type array
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    $('.slideshow').slider({
        autoplay: true,
        index: false,
        count: false,
        autodelay: 15000
    });

    if ($('input[name=show-all]').val() === 'true') {
        var loading = false;
        var sum_actual_count = parseInt($.trim($('.showing-to').html()));
        var page_num = 1;

        $(window).scroll(function () {

            if( !loading && ($(window).scrollTop() + $(window).height() >= $(document).height()) ) {

                $('.gallery-media').parent().append('<h1 class="loading-content loading-content--uppercase">Loading media...</h1>');

                loading = true;
                page_num++;

                var url = '/gallery/scroll-load-media';
                var category = window.location.pathname.indexOf('category') != -1;

                if (category) {
                    var path = window.location.pathname.split('/');
                    path = path.reverse();

                    if (window.location.search != '') {
                        url = url + '/' + path[0] + window.location.search + '&source=category';
                    } else {
                        url = url + '/' + path[0] + '?source=category';
                    }
                } else {
                    url = url + window.location.search;
                }

                if (url.indexOf('?') != -1) {
                    url = url + '&page=' + page_num;
                } else {
                    url = url + '?page=' + page_num;
                }

                var actual_count = parseInt($.trim($('.showing-to').html()));
                var total_count  = parseInt($.trim($('.total-count').html()));

                if(actual_count >= total_count){
                    $('.showing-to').html(total_count);
                    $('.loading-content').html('There is no more media to be loaded.');
                } else {
                    $.ajax({
                        type: "GET",
                        url: url,
                        success: function(res) {
                            $('.loading-content').remove();
                            $(".gallery-media").append(res);
                            if(actual_count + sum_actual_count > total_count){
                                actual_count = total_count;
                            } else {
                                actual_count = actual_count + sum_actual_count;
                            }
                            $('.showing-to').html(actual_count);

                            loading = false;
                        }
                    });
                }
            }
        });
    }

    $('.grid').on('click', 'div.media', function(e) {
        $('.media').not(this).removeClass('media--active');

        $('.media').removeClass('ie8-margin-bottom');

        $('.media-download').not($(this).next()).hide(0);
        $('.media-download').removeClass('media-download--second media-download--third media-download--fourth media-download__threes media-download__fours');

        var $this = $(this);

        var scrollOffset = $this.toggleClass('media--active').next().toggle().offset();

        var index;

        if(!mobileView){
            if ($this.parent().hasClass('l-one-third')) {

                index = $this.parent().index() % 3;

                $this.next().toggleClass('media-download__threes');

                if (index == 1) {
                    $this.next().toggleClass('media-download--second');

                    if(isIe8 && $this.hasClass('media--active')){
                        $(this).parent().next().children().first().toggleClass('ie8-margin-bottom');
                    }

                } else if (index == 2) {
                    $this.next().toggleClass('media-download--third');
                } else {

                    if(isIe8 && $this.hasClass('media--active')){
                        $(this).parent().next().next().children().first().toggleClass('ie8-margin-bottom');
                    }
                }

            } else {

                index = $this.parent().index() % 4;

                $this.next().toggleClass('media-download__fours');

                if (index == 1) {
                    $this.next().toggleClass('media-download--second');
                    if(isIe8 && $this.hasClass('media--active')){
                        $(this).parent().next().next().children().first().toggleClass('ie8-margin-bottom');
                    }
                } else if (index == 2) {
                    $this.next().toggleClass('media-download--third');
                    if(isIe8 && $this.hasClass('media--active')){
                        $(this).parent().next().children().first().toggleClass('ie8-margin-bottom');
                    }
                } else if (index == 3) {
                    $this.next().toggleClass('media-download--fourth');
                } else {
                    if(isIe8 && $this.hasClass('media--active')){
                        $(this).parent().next().next().next().children().first().toggleClass('ie8-margin-bottom');
                    }
                }
            }
        }






        // Offset with clicked media at the top of the page
        // scrollOffset = scrollOffset.top - $this.height() - $('.header').outerHeight(true);

        // Center to screen
        scrollOffset = Math.round(($(window).height() / 2) - ($(this).next().height() / 2));
        scrollOffset = $(this).next().offset().top - scrollOffset;

        if(mobileView){
            scrollOffset -= ($('.header__nav--mobile').outerHeight() + 30);
        }

        if ($this.hasClass('media--active') ) {
            $(window).scrollTo(scrollOffset,300,{interrupt:!iPad});
        }



    });




    $('.grid').on('click', '.media-download__close', function(e) {
        e.preventDefault();
        $(this).closest('.media-download').prev().trigger('click');
    });

    /**
     * Show and hide the filters
     *
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    $('.filters__toggle, .filters__title').click(function(event) {
        event.preventDefault();
        var toggle = $(this).parent().find('.filters__toggle');
        $(this).parent().next().stop(true, false).slideToggle(200);
        toggle.toggleClass('filters__toggle--open');

        if (toggle.hasClass('filters__toggle--open')) {
            toggle.text(toggle.text().replace('Show', 'Hide'));
        } else {
            toggle.text(toggle.text().replace('Hide', 'Show'));
        }
    });


    /**
     * Allow the filters on the page to clear and resubmit the form
     *
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    $('.filters__clear').click(function(event) {
        event.preventDefault();

        $('.filters__footer').remove();
        $('.filters select').find('option[value=\'\']').prop('selected', 'selected');
        $('.filters input').val('');
        $('input.select__input').val('');
        $('.filters--form').submit();
    });

    /**
     * Change the hidden input values for ordering and submit the form
     *
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    $('.sort__action--order').change(function() {
        var value = $(this).val();
        if (value) {
            value = value.split('-');
            $('.filters input[name=\'order\']').val(value[0]);
            $('.filters input[name=\'order-by\']').val(value[1]);
        } else {
            $('.filters input[name=\'order\']').val('');
            $('.filters input[name=\'order-by\']').val('');
        }
        $('.filters form').submit();
    });

    /**
     * Show and hide content for accoridan when clicking on the titles
     *
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    $('.accordion__title').click(function(event) {
        event.preventDefault();
        if ($(this).find('a').length) {
            history.pushState(null, null, '#' + $(this).find('a').attr("name"));
        }
        var accordionArticleActive = $(this).parent().hasClass('accordion__article--active');
        $('.accordion__article--active').removeClass('accordion__article--active');
        if (!accordionArticleActive) {
            $(this).parent().addClass('accordion__article--active');
        }
    });

    /**
     * Enable sliding for mobile grids
     *
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */

    //$('.mobile-scroll__control').click(function() {
    //    var direction = '+';
    //    if ($(this).hasClass('mobile-scroll__control--left')) {
    //        direction = '-';
    //    }
    //    moveGrid($(this), direction);
    //});
    //
    //$('.mobile-scroll__controls').on('swipeleft', function(e) {
    //    moveGrid($(this), '+');
    //}).on('swiperight', function(e) {
    //    moveGrid($(this), '-');
    //});
    //
    //$('.release-sidebar__title a').click(function() {
    //    var offSet = $('.header').outerHeight(true);
    //    $('html, body').animate({
    //        scrollTop: ($($.attr(this, 'href')).offset().top - offSet)
    //    }, 1000);
    //    return false;
    //});
    //
    $( ".page__anchor" ).click(function(e) {
        var page = $("html, body"),
            offSet = $('.header').outerHeight(true);

            if(mobileView){
                offSet = $('.header__nav--mobile').outerHeight(true);
            }

        $(window).scrollTo(($($.attr(this, 'href')).offset().top - offSet),1000,{interrupt:!iPad});
    });


    /**
     * Updates the dropdown menu count by adding the integer passed
     *
     * @param  int i The integer to add to the downloads count
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    var updateDownloadsDropdownCount = function(i) {

        if(i != 0){
            var downloadsCount = parseInt($('.downloads:eq(0)').text());
            downloadsCount += i;
            $('.downloads').addClass('downloads--active').text(downloadsCount);
        }

    };

    var alreadyAddedToBasket = function() {
        // Do stuff here if we need to (like a nice user friendly message
        // telling the user their download has already been added).
        return true;
    };

    /**
     * Add an item to basket
     *
     * @return void
     * @author  Richard Skene <richard.skene@newspress.co.uk>
     */
    $('.grid').on('click', '.basket-action--add', function(e) {
        e.preventDefault();
        
        var crops = [],
            mediaId = sanitizeString($(this).prevAll('input[name="id"]').val()),
            mediaType = sanitizeString($(this).prevAll('input[name="type"]').val()),
            mediaTitle = sanitizeString($(this).prevAll('input[name="title"]').val()),
            url = '/downloads/add/' + mediaId,
            downloadsCount = 0;

        if (mediaType == 'image') {

            var selected = $(this).parent().prev('.media-download__table').find('input[type="checkbox"]:checked');

            if(selected.length){
                selected.each(function(i) {

                    if($(this).attr('data-added')  == 'false'){
                        var value = sanitizeString($(this).val());
                        crops.push(value);
                        downloadsCount++;
                        $(this).attr('data-added', true);
                    }

                });
            }else{

                console.log('hello');
                $(this).parent().prev().find('tr')
                    .css({ borderBottom: '1px solid #333'})
                    .animate({backgroundColor: '#be520c'}, 300)
                    .animate({backgroundColor: '#333'}, 200)
                    .animate({backgroundColor: '#be520c'}, 300)
                    .animate({backgroundColor: '#333'}, 200);
            }


        } else {
            crops.push(false);
            downloadsCount++;
        }

        if(downloadsCount != 0){
            updateDownloadsDropdownCount(downloadsCount);
            $.post(url, {
                crops: crops,
                type: mediaType,
                title: mediaTitle
            }, function(data) {
                $('.downloads-overview').addClass('downloads-overview--show');
                updateDownloadsDropdown();
                setTimeout(function() {
                    $('.downloads-overview').removeClass('downloads-overview--show');
                }, 3000);
            });
        }



    });

    /**
     * Add documents to basket
     *
     * @return void
     * @author  Richard Skene <richard.skene@newspress.co.uk>
     */
    $('.basket-action--add-documents').click(function(event) {
        event.preventDefault();
        var items = {},
            downloadsCount = 0;

        $(this).parent().prev('.release-sidebar__documents').find('input[type="checkbox"]:checked').each(function(i) {
            if ($('.downloads-overview input[type=\'hidden\'][name=\'id\'][value=\'' + $(this).val() + '\']').length) {
                return alreadyAddedToBasket();
            }
            var item = {};
            item.id = sanitizeString($(this).val());
            item.title = sanitizeString($(this).parent().next('input[name="title"]').val());
            items[i] = item;
            downloadsCount++;
        });

        var url = '/downloads/add-documents';
        updateDownloadsDropdownCount(downloadsCount);

        $.post(url, {
            items: items
        }, function(data) {
            $('.downloads-overview').addClass('downloads-overview--show');
            updateDownloadsDropdown();
            setTimeout(function() {
                $('.downloads-overview').removeClass('downloads-overview--show');
            }, 3000);
        });
    });

    /**
     * Check all media checkboxes
     *
     * @return void
     * @author  Richard Skene <richard.skene@newspress.co.uk>
     */
    $('.basket-action--select-all').change(function() {
        var $this = $(this);
        $this.closest('table').find('input[type="checkbox"]').each(function(i) {
            if ($this.is(':checked')) {
                $(this).prop('checked', true);
            } else {
                $(this).prop('checked', false);
            }
        });
    });

    $('.downloads__table input[type=\'checkbox\']').not('.basket-action--select-all').click(function() {
        var total = $(this).closest('tbody').find('input[type=\'checkbox\']').length,
            totalChecked = $(this).closest('tbody').find('input[type=\'checkbox\']:checked').length;
        $(this).closest('table').find('.basket-action--select-all').prop('checked', (total === totalChecked));
    });

    /**
     * Download selected items
     *
     * @return void
     * @author Richard Skene <richard.skene@newspress.co.uk>
     */
    $('.basket-action--download-selected').click(function() {

        if(!downloadProcessing){

            downloadProcessing = true;

            var btn = $(this);

            //Disable this button for 5 seconds
            btn.css({opacity:.3});

            setTimeout(function(){
                btn.css({opacity:1});
                downloadProcessing = false;
            }, 5000);


            if (checkChecked(false, $(this)) === false) {
                return false;
            }
            var form = document.createElement("form");
            form.method = "POST";
            form.addClass = "hidden";

            if ($(this).hasClass('downloads-sidebar__button--recent')) {
                form.action = "/downloads/recentdownload";
            } else {
                form.action = "/downloads/download";
            }

            $(this).closest('.download-section').find('input[type="checkbox"]:checked').each(function(i) {

                var id = document.createElement("input"),
                    type = document.createElement("input"),
                    crop = document.createElement("input");

                id.value = $(this).parent().parent().find('input[name=\'id\']').val();
                id.type = "hidden";
                id.name = "id[]";

                type.value = $(this).parent().parent().find('input[name=\'type\']').val();
                type.type = "hidden";
                type.name = "type[]";

                crop.value = $(this).parent().parent().find('input[name=\'crop\']').val();
                crop.type = "hidden";
                crop.name = "crop[]";

                form.appendChild(id);
                form.appendChild(type);
                form.appendChild(crop);
            });

            $('body').append(form);

            if($('form').serialize() == lastForm){
                var downloadAgain = window.confirm("Are you sure you want to download the same zip?");

                if(downloadAgain){
                    lastForm = $('form').serialize();
                    form.submit();
                }

            }else{
                lastForm = $('form').serialize();
                form.submit();
            }

            form.remove();
        }

    });


    /**
     * Check if selected items
     *
     * @return void
     * @author Vilson Zekaj <vilson.zekaj@newspress.co.uk>
     */
    function checkChecked(formname, button) {
        var anyBoxesChecked = false;
        var inputs = '';
        if (formname === false) {
            inputs = 'input[type="checkbox"]:checked';
        } else {
            inputs = '#' + formname + ' input[type="checkbox"]:checked';
        }
        button.closest('.download-section').find(inputs).each(function() {
            anyBoxesChecked = true;
        });

        if (anyBoxesChecked === false) {
            $('body').addClass('modal--overlay');
            $('body').prepend('<img class="modal__loader" src="/core/img/loader.gif" alt="Loading">');
            $.get('/downloads/error-message', function(data) {
                $('body').prepend(data);
                $('.modal__loader').remove();
            });
            return false;
        } else {
            return true;
        }
    }


    /**
     * Email selected items
     *
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */

    $('.basket-action--email-selected').click(function(event) {
        event.preventDefault();

        if (checkChecked(false, $(this)) === false) {
            return false;
        }

        emailButtonClicked = $(this);

        $('body').addClass('modal--overlay');
        $('body').prepend('<img class="modal__loader" src="/core/img/loader.gif" alt="Loading">');

        $.post('/downloads/email-form', function(data) {
            $('body').prepend(data);
            $('.modal__loader').remove();
        });
    });

    /**
     * Security filters
     *
     * @author Thomas Forster <thomas.forster@newspress.co.uk>
     * @author Mauro Marinho  <mauro.marinho@newspress.co.uk>
     */
    $('html').on('keypress blur focus', 'input', function (event) {

        var keyCode = event.keyCode || event.which,
            regex  = new RegExp("^[a-zA-Z0-9 -]+$"),
            regex2 = new RegExp("^[$%&(\'\"!?]+$"),
            key = String.fromCharCode(!event.charCode ? event.which : event.charCode);


        if (String($(this).attr('name')).indexOf('email') == -1 && String($(this).attr('name')).indexOf('password') == -1) {

            // Don't validate the input if below arrow, delete and backspace keys were pressed
            if (keyCode == 8 || keyCode == 13 || (keyCode >= 32 && keyCode <= 40) || keyCode == 127) { // Left / Up / Right / Down Arrow, Backspace,Delete keys
                if (regex2.test(key) || !key) {
                    event.preventDefault();
                    return false;
                }
                return;
            }

        } else if (String($(this).attr('name')).indexOf('email') != -1) {

            // Don't validate the input if below @ / . / Left / Up / Right / Down Arrow, Backspace, Delete keys were pressed
            if (keyCode == 8 || keyCode == 13 || (keyCode >= 32 && keyCode <= 40) || keyCode == 127 || keyCode == 46 || keyCode == 64) {
                if (regex2.test(key) || !key) {
                    event.preventDefault();
                    return false;
                }
                return;
            }

        } else if(String($(this).attr('name')).indexOf('password') != -1){
            return;
        }

        if (!regex.test(key) || !key) {
            event.preventDefault();
            return false;
        }

    });

    /**
     * Security filters
     *
     * @author Thomas Forster <thomas.forster@newspress.co.uk>
     * @author Mauro Marinho  <mauro.marinho@newspress.co.uk>
     */
    var sanitizeString = function (str) {
        return String(str).replace(/[&\/\\\#,\+=\^()\£$~%.;\'\":*!?<>\{\}\[\]_¡€#¢∞§¶•ªº≠‘“πø^¨¥†®´∑œåß∂ƒ©˙∆˚¬…æ«≥≤µ~∫√ç≈Ω`]/g,'');
    };
    var sanitizeEmail = function (str) {
        return String(str).replace(/[&\/\\\#,\+=\^()\£$~%;\'\":*!?<>\{\}\[\]_¡€#¢∞§¶•ªº≠‘“πø^¨¥†®´∑œåß∂ƒ©˙∆˚¬…æ«≥≤µ~∫√ç≈Ω` ]/g,'');
    };
    var sanitizeIds = function (str) {
        return String(str).replace(/[&\/\\\#\+=\^()\£$~%;\'\":*!?<>\{\}\[\]_¡€#¢∞§¶•ªº≠‘“πø^¨¥†®´∑œåß∂ƒ©˙∆˚¬…æ«≥≤µ~∫√ç≈Ω` ]/g,'');
    };

    $('html').on('submit', 'form', function(event) {

        var $form = $(this);
        var form_fields = $form.find('input');

        $.each(form_fields, function(value, data) {

            var name = $(this).attr('name');

            if(name != 'email' && name != 'password' && name != 'category_ids'){
                $(this).val(sanitizeString($(this).val()));
            } else if (name == 'email') {
                $(this).val(sanitizeEmail($(this).val()));
            } else if (name == 'category_ids') {
                $(this).val(sanitizeIds($(this).val()));
            }
        });
    });


    $(document).on('click', '.basket-action--send-archive', function(event) {
        event.preventDefault();

        var items = {},
            url = '/downloads/email',
            result = emailButtonClicked.hasClass('basket-action--send-recent-archive') ? 1 : 0;


        var name  = sanitizeString($(this).parent().find('input[name=\'name\']').val());
        var email = sanitizeEmail($(this).parent().find('input[name=\'email\']').val());
        $(this).parent().find('input[name=\'name\']').val(name);
        $(this).parent().find('input[name=\'email\']').val(email);

        $('.basket-action--response').removeClass('form__messages--error');
        $('.basket-action--response').text('Creating archive, please wait...');

        emailButtonClicked.closest('.download-section').find('input[data-select]:checked').each(function(i) {
            var item = {};
            item.id = sanitizeString($(this).parent().nextAll('input[name="id"]').val());
            item.type = sanitizeString($(this).parent().nextAll('input[name="type"]').val());
            item.crop = sanitizeString($(this).parent().nextAll('input[name="crop"]').val());
            if (typeof item.id != 'undefined') {
                items[i] = item;
            }
        });

        if (!validateEmail(email)) {
            $('.basket-action--response').addClass('form__messages--error');
            $('.basket-action--response').text('Please provide a valid e-mail address.');
            return false;
        }

        if (!Object.keys(items).length) {
            $('.basket-action--response').addClass('form__messages--error');
            $('.basket-action--response').text('Please select at least one image before send the email.');
            return false;
        }

        $.post(url, {
            items: items,
            name: name,
            email: email,
            recent: sanitizeString(result)
        }, function(data) {
            $('.basket-action--response').closest('form').slideUp(300)
                .prev().prev().text("Items sent - Please check your email account's " +
                "Spam or Junk folder to ensure the message was not filtered.");
        });
    });

    /**
     * Remove selected items
     *
     * @return void
     * @author  Richard Skene <richard.skene@newspress.co.uk>
     */
    $('.basket-action--remove-selected').click(function() {

        if (checkChecked(false, $(this)) === false) {
            return false;
        }

        if ($(this).hasClass('downloads-sidebar__button--bin-recent')) {
            var url = '/downloads/remove-recent';
        } else {
            var url = '/downloads/remove';
        }

        var items = {},
            checkedInputs = $(this).closest('.download-section').find('input[data-select]:checked');

        checkedInputs.each(function(i) {

            var item = {};
            item.id = sanitizeString($(this).parent().nextAll('input[name="id"]').val());
            item.type = sanitizeString($(this).parent().nextAll('input[name="type"]').val());
            item.crop = sanitizeString($(this).parent().nextAll('input[name="crop"]').val());
            items[i] = item;

        });

        $.post(url, {
            items: items
        }, function(data) {
            if (data.status == 'success') {
                checkedInputs.each(function(i) {
                    $(this).closest('tr').remove();
                });
            }
            if ($('#download-media').find('tr').length < 1 || $('#download-documents').find('tr').length < 1) {
                window.location.href = "/downloads";
            }
        });
    });

    /**
     * Remove selected item from download page
     *
     * @return void
     * @todo   Update user feedback
     * @author  Richard Skene <richard.skene@newspress.co.uk>
     */
    $('.basket-action--remove-this').click(function() {
        var items = {},
            url = '/downloads/remove',
            thisParentTbody = $(this).closest('tbody'),
            thisParentTr = $(this).closest('tr'),
            id = thisParentTr.find('td').find('input[name="id"]').val(),
            type = thisParentTr.find('td').find('input[name="type"]').val(),
            crop = thisParentTr.find('td').find('input[name="crop"]').val(),
            item = {};

        item.id = sanitizeString(id);
        item.type = sanitizeString(type);
        item.crop = sanitizeString(crop);
        items[0] = item;

        if ($(this).hasClass('basket-action--remove-this-recent')) {
            url = '/downloads/remove-recent';
        }

        $.post(url, {
            items: items
        }, function(data) {
            if (data.status == 'success') {
                thisParentTr.remove();
            }
            if (thisParentTbody.find('tr').length < 1) {
                window.location.href = "/downloads";
            }
        });
    });

    /**
     * Remove selected item from download overview
     * @return void
     * @author  Richard Skene <richard.skene@newspress.co.uk>
     */
    $('.downloads-overview__content').on('click', '.downloads-overview--remove-this', function(e) {
        e.preventDefault();
        var items = {},
            url = '/downloads/remove-item',
            selection = $(this),
            downloadsCount = parseInt($('.downloads:eq(0)').text());

        if (downloadsCount > 0) {
            downloadsCount--;
            if (downloadsCount === 0) {
                $('.downloads').removeClass('downloads--active');
            } else {
                $('.downloads').addClass('downloads--active');
            }
            $('.downloads').text(downloadsCount);
        }

        var item = {};
        item.id = sanitizeString(selection.nextAll('input[name="id"]').val());
        item.type = sanitizeString(selection.nextAll('input[name="type"]').val());
        item.crop = sanitizeString(selection.nextAll('input[name="crop"]').val());

        $.post(url, {
            item: item
        }, function(data) {
            updateDownloadsDropdown();
        });
    });

    var updateDownloadsDropdown = function() {
        $.get('/downloads/dropdown', function(data) {
            $('.downloads-overview__content').html(data);
            var dropdownTotal = dropdownPageTotal();
            if (dropdownTotal > 0) {
                $('.downloads__pagination').removeClass('display--none');
                $('.downloads__pagination > span').text('Page ' + dropdownCurrentPage + ' of ' + dropdownTotal);
                $('.downloads__button').removeClass('downloads__button--passive');
            } else {
                $('.downloads__pagination').addClass('display--none');
                $('.downloads__button').addClass('downloads__button--passive');
            }
        });
    };

    updateDownloadsDropdown();

    /**
     * Delete selected items
     *
     * @return void
     * @todo   Update user feedback
     * @author  Richard Skene <richard.skene@newspress.co.uk>
     */
    var dropdownCurrentPage = 1;
    var dropdownPageTotal = function() {
        return $('.dropdown__page').length;
    };

    $('.downloads__pagination-action').click(function(event) {
        event.preventDefault();
        var direction;

        if ($(this).hasClass('downloads__pagination-action--prev')) {
            direction = '-';
            if (dropdownCurrentPage > 1) {
                dropdownCurrentPage--;
            }
        } else {
            direction = '+';
            if (dropdownCurrentPage < $('.dropdown__page').length) {
                dropdownCurrentPage++;
            }
        }

        $('.downloads__pagination > span').text('Page ' + dropdownCurrentPage + ' of ' + dropdownPageTotal());

        $('.downloads-overview__content')
            .not(':animated')
            .animate({
                scrollLeft: direction + '=207px'
            });
    });

    /**
     * Direct download selected from the media download dropdown
     *
     * @return void
     * @author Richard Skene <richard.skene@newspress.co.uk>
     */
    $('.grid').on('click', '.media-download__action--download-now', function(event) {
        event.preventDefault();
        var crops = [],
            mediaId = $(this).prevAll('input[name="id"]').val(),
            mediaType = $(this).prevAll('input[name="type"]').val(),
            mediaTitle = $(this).prevAll('input[name="title"]').val(),
            url = '/downloads/download/';

        if (mediaType == 'image') {
            $(this).parent().prev('.media-download__table').find('input[type="checkbox"]:checked').each(function(i) {
                crops.push($(this).val());
            });
        }

        if (mediaType != 'image' || crops.length < 1) {
            window.location.href = $(this).prevAll('input[name="direct_url"]').val();
            return false;
        } else {
            $(this).parent().parent('form').submit();
        }

    });

    /**
     * Direct download selected from the release sidebar
     *
     * @return void
     * @author Richard Skene <richard.skene@newspress.co.uk>
     */
    $('.document__action-download--now').click(function(event) {
        event.preventDefault();

        if ($(this).parent().prev('.release-sidebar__documents').find('input[type="checkbox"]:checked').length) {
            $(this).parent().parent('form').submit();
        }
    });


    /**
     * Show a modal with information about a partner
     * @return void
     * @author  Richard Skene <richard.skene@newspress.co.uk>
     */
    $('.partner-logo').click(function(event) {
        event.preventDefault();

        var partnerTitle = $(this).data('title'),
            url = "/partners/partner-modal/" + partnerTitle;

        $('body').addClass('modal--overlay');
        $('body').prepend('<img class="modal__loader" src="/core/img/loader.gif" alt="Loading">');

        $.get(url, function(data) {
            $('body').prepend(data);
            $('.modal__loader').remove();
        });
    });

    /**
     * Show modal to play selected video
     * @return void
     * @author  Richard Skene <richard.skene@newspress.co.uk>
     */
    $('.media--play-button').click(function(event) {
        event.stopPropagation();

        var videoId = $(this).data('video-id'),
            url = '/gallery/video-modal/' + videoId;

        $('body').addClass('modal--overlay');
        $('body').prepend('<img class="modal__loader" src="/core/img/loader.gif" alt="Loading">');

        $.get(url, function(data) {
            $('body').prepend(data);
            $('.modal__loader').remove();
        });
    });

    /**
     * Toggle packs
     * @return void
     * @author  Richard Skene <richard.skene@newspress.co.uk>
     */
    $('.show-pack').click(function(event) {

        if ($(this).parent().hasClass('orange-link--td')) {
            $('.orange-link--td').removeClass('orange-link--active');
            $(this).parent().addClass('orange-link--active');
        } else {
            $('.orange-link').removeClass('orange-link--active');
            $(this).addClass('orange-link--active');
        }

        $('.pack__item').removeClass('pack--active');
        $('.pack').removeClass('pack--active');

        var thisPack = $(this).data('pack');
        $('#' + thisPack).addClass('pack--active');
        $('#' + thisPack).find('.document-links__item').removeClass('document-links__item--active');
        $('#' + thisPack).find('.document-links__item:first').addClass('document-links__item--active');
        $('#' + thisPack).find('.pack__item:first').addClass('pack--active');
    });

    /**
     * Toggle packs mobile
     * @return void
     * @author  Thomas Forster <thomas.forster@newspress.co.uk>
     */
    $('.show-pack-mobile').click(function(event) {

        if($(this).parent().hasClass('orange-link--active')){
            return;
        }

        $('.orange-link').removeClass('orange-link--active');

        $(this).addClass('orange-link--active');
        $(this).parent().next().find('.document-links').children().first().click();

    });


    /**
     * Toggle pack items
     * @return void
     * @author Richard Skene <richard.skene@newspress.co.uk>
     */
    $('.show-pack__item').click(function(event) {
        event.preventDefault();
        $('.document-links__item').removeClass('document-links__item--active');
        $(this).addClass('document-links__item--active');
        $('.pack__item').removeClass('pack--active');
        var thisPack = $(this).data('pack-item');
        $('.' + thisPack).addClass('pack--active');
    });

    var moveGrid = function(object, direction) {
        // Is there enough room to actually move?
        object
            .closest('.mobile-scroll')
            .find('.mobile-scroll__content')
            .not(':animated')
            .animate({
                scrollLeft: direction + '=300%'
            });
    };


    /**
     * Close modals if anywhere but modal is clicked
     * @return void
     * @author Richard Skene <richard.skene@newspress.co.uk
     */
    $(window).on('click', function(event) {
        if (event.target.className.indexOf('modal--overlay') !== -1) {
            $('.modal__action--close').trigger('click');
        }
    });

    if ($('.release__flags--scroll-wrapper').length) {
        $('.release__flags--scroll-wrapper').each(function(i, element) {
            var flags = $(this).find('.flag');
            $(this).css('width', (flags.length * flags.eq(1).outerWidth(true)) + 'px');
        });
    }

    $('.release__flags--scroll-left, .release__flags--scroll-right').click(function() {
        var direction = '+';
        if ($(this).hasClass('release__flags--scroll-left')) {
            direction = '-';
        }
        var flagWidth = $(this).parent().find('.release__flags--scroll-wrapper .flag').eq(1).outerWidth(true);
        $(this).parent().find('.release__flags--scroll').not(':animated').animate({
            'scrollLeft': direction + '=' + flagWidth + 'px'
        }, 200);
    });

    $('.ie9 .search-input, .ie8 .search-input, .ie7 .search-input').val('Search');
    $('.ie9 .search-input, .ie8 .search-input, .ie7 .search-input').focus(function() {
        $(this).val('');
    });
    $('.ie9 .search-input, .ie8 .search-input, .ie7 .search-input').blur(function() {
        $(this).val('Search');
    });

    $('.bio__name').click(function(event) {
        event.preventDefault();
        $(this).parent().toggleClass('bio__content--show').find('.bio__content').stop(true).slideToggle();
    });

    /**
     * Enables the functionality for the custom
     * selection boxes
     *
     * @author Richard Skene <richard.skene@newspress.co.uk>
     */
    $('.select').click(function(event) {
        event.stopPropagation();
        if ($('.select--show').not($(this))) {
            $('.select--show').not($(this)).removeClass('select--show').find('.select__list').stop().slideUp(300);
        }
        $(this).toggleClass('select--show').find('.select__list').stop().slideToggle(300);
    });
    $('.select__options').click(function(event) {
        event.stopPropagation();

        if ($(this).closest('.select').find('.select__input').attr('name') == 'models') {
            $('input[name=model_id]').val($(this).data('id'));
        }

        $(this).closest('.select').find('.select__text').html($(this).html());
        $(this).closest('.select').find('.select__input').val($(this).data('value'));
        $(this).closest('.select').trigger('click');

        if(!mobileView){
            $('.filters__input').focus();
        }

    });

    /**
     * Enables the functionality for the custom
     * multi selection box
     *
     * @author Mauro Marinho <mauro.marinho@newspress.co.uk>
     */
    $('.select__multi').on('click',function(event) {
        event.stopPropagation();

        var $this      = $(this);
        var classes    = $.trim($this.attr('class'));
        var value      = String($this.data('value')).replace(/[^a-z0-9\s]/gi, '').replace(/  \(\)/g, ' ');
        var id         = String($this.data('id'));
        var item_class = value.replace(/ /g, '').toLowerCase();

        var text       = $('.select__multi-text-'+item_class).html();

        classes = '.' + $.trim(classes.replace(/ /g,'.').replace('.icon--plus', '').replace('.icon--minus', ''));

        if ($(classes + ' span.icon').hasClass('icon--plus')) {
            selectMultiAdd($this, text, value, id);
        } else {
            selectMultiRemove($this, text, value, id);
        }

        $(classes + ' span.icon').toggleClass('icon--plus');
        $(classes + ' span.icon').toggleClass('icon--minus');
        $(classes).toggleClass('select__multi--selected');


        if(!mobileView){
            $('.filters__input').focus();
        }

    });
    var selectMultiAdd = function ($element, text, value, id) {
        var current_text  = $element.closest('.select').find('.select__text').html();
        var current_value = $element.closest('.select').find('.select__input').val();

        current_text  = $.trim(current_text);
        current_value = $.trim(current_value);

        if (current_text == 'Select') {
            current_text = text;
            current_value = value;
        } else {
            current_text = current_text + ' | ' + text;
            current_value = current_value + '|' + value;
        }

        if ($element.closest('.select').find('.select__input').attr('name') != 'year') {
            var cat_ids = $('input[name=category_ids]').val();
            $('input[name=category_ids]').val(
                (cat_ids != '' ? cat_ids + ',' + id : id)
            );
        }

        $element.closest('.select').find('.select__text').html(current_text);
        $element.closest('.select').find('.select__input').val(current_value);
    };
    var selectMultiRemove = function ($element, option_text, option_value, option_id) {
        var current_text  = $.trim($element.closest('.select').find('.select__text').html());
        var current_value = $.trim($element.closest('.select').find('.select__input').val());
        var current_ids   = $.trim($('input[name=category_ids]').val());

        current_text  = current_text.split(' | ');
        current_value = current_value.split('|');
        current_ids   = current_ids.split(',');

        current_text = $.grep(current_text, function(value) {
            return $.trim(value) != option_text;
        });
        current_text = current_text.join(' | ');

        current_value = $.grep(current_value, function(value) {
            return $.trim(value) != option_value;
        });
        current_value = current_value.join('|');

        current_ids = $.grep(current_ids, function(value) {
            return $.trim(value) != option_id;
        });
        current_ids = current_ids.join(',');

        if ($.trim(current_text) == '') {
            current_text  = 'Select';
            current_value = '';
        }

        $element.closest('.select').find('.select__text').html(current_text);
        $element.closest('.select').find('.select__input').val(current_value);
        $('input[name=category_ids]').val(current_ids);
    };
    var initializeCategoryType = function() {
        var selected_model         = String($('input[name=models]').val()).split('|');
        var selected_categories    = String($('input[name=category]').val()).split('|');
        var selected_subcategories = String($('input[name=sub-category]').val()).split('|');
        var selected_regions       = String($('input[name=region]').val()).split('|');
        var selected_companies     = String($('input[name=company]').val()).split('|');
        var selected_events        = String($('input[name=events]').val()).split('|');
        var selected_years         = String($('input[name=year]').val()).split('|');
        var filter_opts            = $('.select__multi');

        $.each(filter_opts, function(key, value) {
            var text_content = $.trim(String(value.textContent));
            text_content = text_content.replace(/[^a-z0-9\s]/gi, '').replace(/  /g, ' ');

            if (typeof text_content != 'undefined' &&
                (
                $.inArray(text_content, selected_categories) != -1
                || $.inArray(text_content, selected_subcategories) != -1
                || $.inArray(text_content, selected_regions) != -1
                || $.inArray(text_content, selected_companies) != -1
                || $.inArray(text_content, selected_events) != -1
                || $.inArray(text_content, selected_years) != -1
                )
            ) {
                $(this).addClass('select__multi--selected');
                $(this).find('span.icon').toggleClass('icon--plus');
                $(this).find('span.icon').toggleClass('icon--minus');

                if ($(this).closest('.select').find('.select__input').attr('name') != 'year') {
                    var cat_ids = $('input[name=category_ids]').val();
                    $('input[name=category_ids]').val(
                        (cat_ids != '' ? cat_ids + ',' + $(this).data('id') : $(this).data('id'))
                    );
                }
            }
        });

        if (selected_model != '') {

            var model_id = $('li[data-value="'+selected_model+'"]').data('id');

            $('input[name=model_id]').val(model_id);
        }

    };
    initializeCategoryType();

    /**
     * Submit form when sort option is selected
     *
     * @author  Richard Skene <richard.skene@newspress.co.uk>
     */
    $('.js-sort-option').on('click', function() {
        $(this).closest('form').submit();
    });

    /**
     * Submits mobile search
     *
     * @author  Thomas Forster <thomas.forster@newspress.co.uk>
     */
    $('#mobile-search-submit').click(function() {

        $(this).closest('form').submit();
    });


    /**
     * Changes for mobile navigation.
     *  As the user is using a touch device the use of hover effects is unavailable
     * @author  Thomas Forster <thomas.forster@newspress.co.uk>
     */
    if( iPad ) {

        $('.slick-slide').css({padding:0});

        $('.mobile-scroll__control').css({
            top: 'calc(39.5% - 80px)',
            'z-index': 1,
            padding: '40px 0'
        });

        $('.header__nav ul li a.select-dropdown').click(function(){

            if($(this).hasClass('dropdown--show')){
                $('.header__nav ul li a.select-dropdown').removeClass('dropdown--show').css({color:'#d6d6d6'});
                $('.dropdown').hide();
                $('.header__nav ul li a.select-dropdown:after').css({'background-position': '81.87441860465117% 69.23076923076923%'});
            }else{
                $('.header__nav ul li a.select-dropdown').removeClass('dropdown--show').css({color:'#d6d6d6'});
                $('.dropdown').hide();
                $(this).addClass('dropdown--show').css({color:'#e0610e'}).next('.dropdown').show();

            }

        });
    }

    /**
     * When a user hovers on a ellipsis title, display the full title in a pop up.
     *
     * @author  Thomas Forster <thomas.forster@newspress.co.uk>
     */
    $('.document-link').hover(function(){
        $(this).prev().stop(true,false).fadeIn(150);
        $(this).prev().css({top:'-'+($(this).prev().height() + 30)+'px'});

    },function(){
        $(this).prev().stop(true,false).fadeOut(150);
    });


    /**
     * Returns the Browser and its version that is being used.
     *
     * @author  Mauro Marinho <mauro.marinho@newspress.co.uk>
     */
    var validateEmail = function (email) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(email)) {
            return true;
        } else {
            return false;
        }
    };

    /**
     * prevents double submission on a form.
     * NOTE: use preventDoubleSubmission() on the form you want this function effects.
     *  e.g. $('.your_class_or_element_here').preventDoubleSubmission();
     * @author  Thomas Forster <thomas.forster@newspress.co.uk>
     */
    jQuery.fn.preventDoubleSubmission = function() {

        var downloaded = false,
            images = 0,
            videos = 0,
            documents = 0;

        $(this).on('submit',function(e){
            var $form = $(this);
            var submitButton = $form.find('button[name="direct-medias"]');

            if ($form.data('submitted') === true) {
                // Previously submitted - don't submit again
                e.preventDefault();
            } else {
                // Mark it so that the next submit can be ignored

                submitButton.oldText = 'Download all media';
                submitButton.text('Please wait...');

                $form.data('submitted', true);

                if(downloaded){
                    if(!confirm('Do you want to download these files again?')){
                        e.preventDefault();
                        submitButton.text(submitButton.oldText);
                        $form.data('submitted', false);
                        return;
                    }
                }

                images = $form.find('input[value="image"]').length;
                videos = $form.find('input[value="video"]').length;

                setTimeout(function(){
                    $form.data('submitted', false);
                    submitButton.text(submitButton.oldText);
                    downloaded = true;
                }, (images+videos+documents)*1000);
            }
        });

        return this;
    };

    $('.release__download-all').preventDoubleSubmission();

});