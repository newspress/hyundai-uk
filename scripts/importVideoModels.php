<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';

// DELETE FROM `object_links` WHERE `from_object` = 7 AND `to_object` = 1

/**
 * Removes double extension names from S3 and the database
 */
$sql = \Newspress::db()->sql();
$now = new \DateTime();

$select = $sql->select();
$select->from('legacy_models_vids')
       ->join('categories', 'categories.legacy_id = legacy_models_vids.mid', array('category_id' => 'id'))
       ->join('media', 'media.legacy_id = legacy_models_vids.vid', array('media_id' => 'id'));
$select->where
       ->equalTo('categories.legacy_table', 'legacy_models')
       ->equalTo('media.type', 'video');

$legacyImageModels = \Newspress::db()->execute($select);

 var_dump($sql->getSqlStringForSqlObject($select));
 exit;

/*
$modelArray = array(
        // Old ID ::: New ID
        1 => 5,
        2 => 6,
        3 => 7,
        4 => 8,
        5 => 9,
        6 => 14,
        7 => 8,
        8 => 11,
        9 => 12,
        10 => 13,
        // 11 => x,
        12 => 15,
        13 => 16,
        14 => 17,
        15 => 18,
        16 => 19,
        17 => 20
    );
*/

$modelArray = array(
    // Old ID :: New ID
    1 => 5, // = 'McLAREN 12C'
    4 => 6, // = 'McLAREN GT'
    5 => 7, // = 'F1'
    6 => 8, // = 'MP4/1'
    7 => 9, // = 'M6GT'
    8 => 10, // = 'McLAREN P1'
    9 => 11, // = '12C GT CAN-AM EDITION'
    10 => 12, // ='12C GT SPRINT'
    11 => 13, // = 'McLAREN 650S COUPE'
    12 => 14, // = 'McLAREN 12C SPIDER'
    13 => 15, // = 'McLAREN 650S SPIDER'
    13 => 79, // = 'McLAREN 650S SPIDER'
    14 => 16, // = 'McLAREN P1 GTR'
    15 => 17, // = '650S GT3'
    16 => 18, // = '650S SPRINT'
    17 => 19, // = 'SPORTS SERIES'y
    19 => 20 // = 'SPORTS SERIES'
    
);

foreach ($legacyImageModels as $legacyImageModel) {

    if (!isset($modelArray[$legacyImageModel['category_id']])) {
        \Newspress\Cli::uiError('Could not match model with ID: ' . $legacyImageModel['category_id']);
        continue;
    }


    $categoryId = $modelArray[$legacyImageModel['category_id']];

    \Newspress\Cli::uiMessage('categoryId ID: ' . $categoryId);

    \Newspress\Cli::uiMessage('media_id ID: ' . $legacyImageModel['media_id']);


    $insert = $sql->insert();
    $insert->into('object_links')
           ->columns(array('id', 'from_object', 'from_object_id', 'to_object', 'to_object_id', 'sort'))
           ->values(array(
                'id'             => null,
                'from_object'    => 7,
                'from_object_id' => $categoryId,
                'to_object'      => 2,
                'to_object_id'   => $legacyImageModel['media_id'],
                'sort'           => 0,
            ));

    $statement = $sql->prepareStatementForSqlObject($insert);
    $statement->execute();

    $objectLinkId = \Newspress::db()->getInsertId();

    \Newspress\Cli::uiMessage('Added object link for image model with ID: ' . $objectLinkId);

}
