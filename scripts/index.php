<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';

/**
 * Indexes all objects in Elasticsearch
 */
$options = array();

if (isset($argv[2])) {
    $options = array_splice($argv, 2);
}


/**
 * Make a new instance of the Select class
 */
\Newspress\Cli::uiMessage('Creating an instance of the database class');
$sql = \Newspress::db()->sql();


/**
 * Make a new instance of the Elasticsearch service
 */
\Newspress\Cli::uiMessage('Creating an instance of the Elasticsearch service');
$esClient = new \Newspress\Service\Elasticsearch();


/**
 * Delete the Elasticsearch to start from fresh
 */
// if (in_array('delete', $options)) {
\Newspress\Cli::uiMessage('Deleting the current Elasticsearch index');
$esClient->deleteIndex();
// \Newspress\Cli::uiMessage('Please re-run the script');
// exit;
// }

if (empty($options) || in_array('media', $options)) {

    /**
     * Import all images into the database (save the old ID)?
     */
    \Newspress\Cli::uiMessage('Counting media');
    $select = $sql->select();
    $select->columns(array(
        'count' => new \Zend\Db\Sql\Expression('COUNT(*)')
    ))
        ->from('media');

    $results = \Newspress::db()->execute($select);

    // Set defaults for batch select queries
    $offset = 0;
    $limit = 100;
    $total = (int) $results->current()['count'];

    /**
     * Start selecting images in batches
     */
    \Newspress\Cli::uiMessage('Starting index for ' . $total . ' media');
    while ($total > $offset) {

        $select = $sql->select();
        $select->from('media')
            ->columns(array('id', 'type', 'name', 'alt', 'title', 'description', 'keywords', 'size', 'published', 'modified', 'created'));
        $select->order('id DESC')
            ->limit($limit)
            ->offset($offset);

        // Echo the query on the command line if needed (for debugging)
        // \Newspress\Cli::uiMessage('Image import batch query: ' . $sql->getSqlStringForSqlObject($select));

        $results = \Newspress::db()->execute($select);

        // Go through results of current batch
        foreach ($results as $result) {

            $mediaId = (int) $result['id'];
            unset($result['id']);

            // Select the categories that belong to this release
            $select = $sql->select();
            $select->from('object_links')
                ->where
                ->equalTo('from_object', 7)
                ->equalTo('to_object', 2)
                ->equalTo('to_object_id', $mediaId);
            $select->order('id DESC');

            $objectLinks = \Newspress::db()->execute($select);

            $objectsArray = array();
            foreach ($objectLinks as $objectLink) {
                $objectsArray[] = $objectLink['from_object_id'];
            }

            $categories = array();
            if (count($objectsArray) > 0) {

                // Go through categories and get the category paths
                $select = $sql->select();
                $select->from('categories')
                    ->where
                    ->in('id', $objectsArray);
                $select->order('id ASC');

                // var_dump($objectsArray);
                // var_dump($sql->getSqlStringForSqlObject($select));

                $categoryResults = \Newspress::db()->execute($select);

                foreach ($categoryResults as $categoryResult) {
                    $categories[] = implode(',', array_merge(array($categoryResult['id']), array_reverse(explode(',', $categoryResult['parent_path']))));
                }

                $categories = explode(',', implode(',', $categories));

            }

            $body = array(
                'content' => \Newspress\Encoding::toUTF8($result),
                'categories' => \Newspress\Encoding::toUTF8($categories)
            );

            try {

                $esData = array();
                $esData['id'] = \Newspress\Encoding::toUTF8($mediaId);
                $esData['type'] = \Newspress\Encoding::toUTF8('media');
                $esData['body'] = $body;

                $esClient->index($esData);

                \Newspress\Cli::uiMessage('Indexed image with ID: ' . $mediaId);

            } catch (\Exception $e) {
                \Newspress\Cli::uiError('Could not index image with ID: ' . $mediaId . ' and message: ' . $e->getMessage());
            }

        }

        // Add the batch limit to the offset
        $offset += $limit;

    }

}

if (empty($options) || in_array('releases', $options)) {

    /**
     * Count the number of releases to import
     */
    \Newspress\Cli::uiMessage('Counting releases');
    $select = $sql->select();
    $select->columns(array(
        'count' => new \Zend\Db\Sql\Expression('COUNT(*)')
    ))
        ->from('releases');
    $results = \Newspress::db()->execute($select);

    // Set defaults for batch select queries
    $offset = 0;
    $limit = 100;
    $total = (int) $results->current()['count'];

    /**
     * Start selecting releases in batches
     */
    \Newspress\Cli::uiMessage('Starting index for ' . $total . ' releases');
    while ($total > $offset) {

        // Select the releases (within the specified batch) joining the English translation
        $select = $sql->select();
        $select->from('releases')
            ->columns(array(
                'id',
                'title',
                'excerpt',
                'content' => new \Zend\Db\Sql\Predicate\Expression('SUBSTR(content,1,897)'),
                'status',
                'published',
                'modified',
                'created'
            ));
        $select->order('id ASC')
            ->limit($limit)
            ->offset($offset);

        // Echo the query on the command line if needed (for debugging)
        // \Newspress\Cli::uiMessage($sql->getSqlStringForSqlObject($select));
        // exit;

        $results = \Newspress::db()->execute($select);

        // Go through results of current batch
        foreach ($results as $key => $result) {

            /**
             * Select all translations for that particular release
             */
            $releaseId = (int) $result['id'];
            unset($result['id']);

            $languages = array();
            $releaseTranslations = array();

            $select = $sql->select();
            $select->from('releases_translations');
            $select->columns(array(
                'id',
                'release',
                'locale',
                'title',
                'excerpt',
                'content' => new \Zend\Db\Sql\Predicate\Expression('SUBSTR(content,1,897)')
            ));
            $select->where
                ->equalTo('release', $releaseId);
            $select->order('id ASC');

            $translationResults = \Newspress::db()->execute($select);

            foreach ($translationResults as $key => $translationResult) {
                unset($translationResult['release']);
                $languages[] = $translationResult['locale'];
                $releaseTranslations[] = array(
                    // @todo: Maybe change the key $localeId to the text version?
                    $translationResult['locale'] => \Newspress\Encoding::toUTF8($translationResult)
                );
            }

            // Update the elasticsearch index with the new translation data
            $result['translations'] = $releaseTranslations;

            // Select the categories that belong to this release
            $select = $sql->select();
            $select->from('object_links')
                ->where
                ->equalTo('from_object', 7)
                ->equalTo('to_object', 1)
                ->equalTo('to_object_id', (int) $releaseId);
            $select->order('id ASC');

            $objectLinks = \Newspress::db()->execute($select);

            $objectsArray = array();
            foreach ($objectLinks as $objectLink) {
                $objectsArray[] = $objectLink['from_object_id'];
            }

            $categories = array();
            if (count($objectsArray) > 0) {

                // Go through categories and get the category paths
                $select = $sql->select();
                $select->from('categories')
                    ->where
                    ->in('id', $objectsArray);
                $select->order('id ASC');

                // var_dump($objectsArray);
                // var_dump($sql->getSqlStringForSqlObject($select));

                $categoryResults = \Newspress::db()->execute($select);

                foreach ($categoryResults as $categoryResult) {
                    $category = implode(',', array_merge(array($categoryResult['id']), array_reverse(explode(',', $categoryResult['parent_path']))));
                    $categories[] = $category;
                }

                $categories = explode(',', implode(',', $categories));

            }

            $body = array(
                'content' => \Newspress\Encoding::toUTF8($result),
                'categories' => \Newspress\Encoding::toUTF8($categories),
                'languages' => \Newspress\Encoding::toUTF8($languages)
            );

            try {

                $esData = array();
                $esData['id'] = \Newspress\Encoding::toUTF8($releaseId);
                $esData['type'] = \Newspress\Encoding::toUTF8('release');
                $esData['body'] = $body;

                $esClient->index($esData);

                \Newspress\Cli::uiMessage('Indexed release with ID: ' . $releaseId);

            } catch (\Exception $e) {
                \Newspress\Cli::uiError('Could not index release with ID: ' . $releaseId . ' and message: ' . $e->getMessage());
            }

        }

        // Add the batch limit to the offset
        $offset += $limit;
    }

}
