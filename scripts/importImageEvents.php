<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';

// DELETE FROM `object_links` WHERE `from_object` = 7 AND `to_object` = 1

/**
 * Removes double extension names from S3 and the database
 */
$sql = \Newspress::db()->sql();
$now = new \DateTime();
/*
SELECT 
`legacy_images_events`.*, `events`.`id` AS `event_id`, `media`.`id` AS `media_id`
FROM 
`legacy_images_events`
INNER JOIN `events` ON `events`.`legacy_id` = `legacy_images_events`.`eid`
INNER JOIN `media` ON `media`.`legacy_id` = `legacy_images_events`.`iid` 
WHERE
`media`.`type` = 'image'
*/

$select = $sql->select();
$select->from('legacy_images_events')
       ->join('events', 'events.legacy_id = legacy_images_events.eid', array('event_id' => 'id'))
       ->join('media', 'media.legacy_id = legacy_images_events.iid', array('media_id' => 'id'));
$select->where
       ->equalTo('media.type', 'image');

$legacyImageEvennts = \Newspress::db()->execute($select);

foreach ($legacyImageEvennts as $legacyImageEvennt) {

    $event_id = $legacyImageEvennt['event_id'];

    \Newspress\Cli::uiMessage('event_id ID: ' . $event_id);

    \Newspress\Cli::uiMessage('media_id ID: ' . $legacyImageEvennt['media_id']);


    $insert = $sql->insert();
    $insert->into('object_links')
           ->columns(array('id', 'from_object', 'from_object_id', 'to_object', 'to_object_id', 'sort'))
           ->values(array(
                'id'             => null,
                'from_object'    => 4,
                'from_object_id' => $event_id,
                'to_object'      => 2,
                'to_object_id'   => $legacyImageEvennt['media_id'],
                'sort'           => 0,
            ));

    $statement = $sql->prepareStatementForSqlObject($insert);
    $statement->execute();

    $objectLinkId = \Newspress::db()->getInsertId();

    \Newspress\Cli::uiMessage('Added object link for image to EVENT with ID: ' . $objectLinkId);

}
