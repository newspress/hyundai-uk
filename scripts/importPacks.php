<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


// TRUNCATE `packs`
// DELETE FROM `object_links` WHERE `to_object` = 5

// Select all legacy packs
$sql = \Newspress::db()->sql();
$now = new \DateTime();

$select = $sql->select();
$select->from('legacy_models_packs')
       ->join('legacy_models_packs_sections', 'legacy_models_packs_sections.id = legacy_models_packs.sid', array('section_title' => 'title'))
       ->join('legacy_models_variant', 'legacy_models_variant.id = legacy_models_packs.vid')
       ->join('categories', 'categories.legacy_id = legacy_models_variant.mid', array('model_id' => 'id'));
$select->where
	   ->equalTo('categories.legacy_table', 'legacy_models')
	   ->equalTo('legacy_models_packs.lid', 1);

$packs = \Newspress::db()->execute($select);

foreach ($packs as $pack) {

	// Import the legacy packs into the current packs database

	// Import the legacy packs with different locales in the packs_translations database

	$insert = $sql->insert();
    $insert->into('packs')
           ->columns(array('id', 'type', 'title', 'content', 'status', 'modified', 'created'))
           ->values(array(
				'id'       => null,
				'type'     => strtolower(str_replace(' ', '-', $pack['section_title'])),
				'title'    => $pack['title'],
				'content'  => $pack['data'],
				'status'   => 'active',
				'modified' => $pack['timestamp_mod'],
				'created'  => $pack['timestamp_cre']
            ));

    \Newspress::db()->execute($insert);

    $packId = \Newspress::db()->getInsertId();

    // Insert pack into object_links table
    $insert = $sql->insert();
    $insert->into('object_links')
           ->columns(array('id', 'from_object', 'from_object_id', 'to_object', 'to_object_id', 'sort'))
           ->values(array(
				'id'             => null,
				'from_object'    => 7, // It's not a model object, it's a category
				'from_object_id' => (int) $pack['model_id'],
				'to_object'      => 5,
				'to_object_id'   => $packId,
				'sort'           => 0
            ));

    \Newspress::db()->execute($insert);

    \Newspress\Cli::uiMessage('Imported pack with ID: ' . $packId);

}
