#!/bin/sh
#
# Update copyright year
#
ack -l '@copyright Copyright \(c\) 2014 Newspress Ltd' | xargs perl -pi -E 's/2014/2015/g'