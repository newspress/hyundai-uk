<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


/**
 * Generates crops that have not yet been created and uploaded to S3
 */

$dontOverwriteExistingCrops = false;

$crops = array();

if (isset($argv[2])) {
    $crops = array_splice($argv, 2);
}


$imageIdOffset = 2450;


/**
 * Make a new instance of the Select class
 */
\Newspress\Cli::uiMessage('Creating an instance of the database class');
$sql = \Newspress::db()->sql();


/**
 * Select all images from the database
 */
\Newspress\Cli::uiMessage('Counting images');
$select = $sql->select();
$select->columns(array(
           'count' => new \Zend\Db\Sql\Expression('COUNT(*)')
       ))
       ->from('media');
$select->where
       ->greaterThan('id', $imageIdOffset)
       ->equalTo('type', 'image');

$results = \Newspress::db()->execute($select);

// Set defaults for batch select queries
$offset = 0;
$limit = 100;
$total = (int) $results->current()['count'];

/**
 * Start selecting images in batches
 */
\Newspress\Cli::uiMessage('Generating crops for ' . $total . ' images');

while ($total > $offset) {

    $select = $sql->select();
    $select->from('media')
           ->columns(array('id', 'name'));
    $select->where
           ->greaterThan('id', $imageIdOffset)
           ->equalTo('type', 'image');
    $select->order('id DESC')
           ->limit($limit)
           ->offset($offset);

    // Echo the query on the command line if needed (for debugging)
    // \Newspress\Cli::uiMessage('Image import batch query: ' . $sql->getSqlStringForSqlObject($select));

    $results = \Newspress::db()->execute($select);

    // Go through results of current batch
    foreach ($results as $result) {

        $imageId = (int) $result['id'];
       
        foreach ($crops as $key => $crop) {

            $image = new \Newspress\Asset\Image($imageId);

            // Check to see if the crop exists already
            if ($dontOverwriteExistingCrops && $image->objectExists($crop)) {
                \Newspress\Cli::uiMessage('Crop "' . $crop . '" with image ID: ' . $imageId . ' already exists... skipping');
                continue;
            }

            // Generate and upload the crop
            $image->createCrop($crop);

            \Newspress\Cli::uiMessage('Generated "' . $crop . '" crop for image with ID: ' . $imageId);

        }

        unset($imageId);

    }

    // Add the batch limit to the offset
    $offset += $limit;

}
