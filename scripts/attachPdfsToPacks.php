<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


/**
 * Import all pack PDFs and attach them to the pack.
 *
 * @todo Add functionality to see documents for a specific locale
 */

// Remove documents attached to packs from the object_links table
// and the packs table
// 
// SELECT * FROM `object_links`
// LEFT JOIN `packs` ON `packs`.`id` = `object_links`.`to_object_id`
// WHERE `object_links`.`from_object` = 5 AND `media`.`type` = 'document';
//
//
// SELECT * FROM `media`
// LEFT JOIN `object_links` ON `object_links`.`to_object_id` = `media`.`id`
// WHERE `object_links`.`from_object` = 5
// AND `object_links`.`to_object` = 2
// AND `media`.`type` = 'document';
// 

$sql = \Newspress::db()->sql();
$now = new \DateTime();

$select = $sql->select();
$select->from('packs');

\Newspress\Cli::uiMessage('Selecting all packs from the database');

$packs = \Newspress::db()->execute($select, 'object');

foreach ($packs as $pack) {

    $localeId = 1;

    // Do the English one first
    generatePdf($pack, $localeId);

    // Go through all pack translations
    $select = $sql->select();
    $select->from('packs_translations');
    $select->where
           ->equalTo('pack', (int) $pack->id)
           ->notEqualTo('locale', 1);

    $packTranslations = \Newspress::db()->execute($select, 'object');

    foreach ($packTranslations as $packTranslation) {
        $pack->title = empty($packTranslation->title) ? $pack->title : $packTranslation->title;
        $pack->content = empty($packTranslation->content) ? $pack->content : $packTranslation->content;
        generatePdf($pack, $packTranslation->locale);
    }

}

function generatePdf($pack, $localeId) {

    $sql = \Newspress::db()->sql();

    $select = $sql->select();
    $select->from('object_links')
           ->where
           ->equalTo('locale', (int) $localeId)
           ->equalTo('from_object', 5)
           ->equalTo('to_object', 2)
           ->equalTo('to_object_id', (int) $pack->id);

    $check = \Newspress::db()->execute($select);

    if ($check->count() > 0) {
        \Newspress\Cli::uiError('Object link already exists for pack with ID: ' . $pack->id . ' and locale ID: ' . $localeId);
        return false;
    }

    $release = $pack;
    $release->published = $pack->created;

    ob_start();
    include 'module/Releases/view/releases/partials/pdf.phtml';
    $html = ob_get_contents();
    ob_end_clean();

    $dompdf = new \DOMPDF();
    $dompdf->load_html($html);
    $dompdf->render();
    $string = $dompdf->output();

    $localFilename = tempnam("/tmp", 'php-document-');
    file_put_contents($localFilename, $string);

    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $mime = finfo_file($finfo, $localFilename);
    finfo_close($finfo);

    $size = @filesize($localFilename);
    if (empty($size)) {
        $size = null;
    }

    $now = new \DateTime($pack->published);
    $name = $pack->title;
    $filename = \Newspress\String::makeUrlFriendly($pack->title);

    $insert = $sql->insert();
    $insert->into('media')
           ->columns(array('id', 'legacy_id', 'type', 'name', 'alt', 'title', 'description', 'keywords', 'acl', 'mime', 'size', 'author', 'status', 'published', 'modified', 'created'))
           ->values(array(
                    'id'          => null,
                    'legacy_id'   => null,
                    'type'        => 'document',
                    'name'        => $filename,
                    'alt'         => null,
                    'title'       => $name,
                    'description' => null,
                    'keywords'    => null,
                    'acl'         => \Aws\S3\Enum\CannedAcl::PUBLIC_READ,
                    'mime'        => $mime,
                    'size'        => (int) $size,
                    'author'      => 1,
                    'status'      => 'active',
                    'published'   => $now->format('Y-m-d H:i:s'),
                    'modified'    => $now->format('Y-m-d H:i:s'),
                    'created'     => $now->format('Y-m-d H:i:s'),
                ));

    \Newspress::db()->execute($insert);
    $documentId = \Newspress::db()->getInsertId();

    \Newspress\Cli::uiMessage('Inserted document into media table with ID: ' . $documentId);

    \Newspress\Cli::uiMessage('Uploading document to S3 with ID: ' . $documentId);

    $document = new \Newspress\Asset\Document();
    $document->setId($documentId);
    $document->setName($filename);
    $document->setMime($mime);
    $document->upload($localFilename);

    unlink($localFilename);

    \Newspress\Cli::uiMessage('Completed upload for document with ID: ' . $documentId);

    $insert = $sql->insert();
    $insert->into('object_links')
           ->columns(array('id', 'locale', 'media_id', 'object', 'object_id'))
           ->values(array(
                'id'             => null,
                'locale'         => (int) $localeId,
                'from_object'    => 5,
                'from_object_id' => (int) $pack->id,
                'to_object'      => 2,
                'to_object_id'   => (int) $documentId,
                'sort'           => 0
            ));

    \Newspress::db()->execute($insert);
    $objectLinkId = \Newspress::db()->getInsertId();

    \Newspress\Cli::uiMessage('Attached document link into object_links table with object link ID: ' . $objectLinkId);

    return true;

}
