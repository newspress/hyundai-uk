<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


$sql = \Newspress::db()->sql();

$select = $sql->select();
$select->from('categories');
$select->order('id ASC');

$categories = \Newspress::db()->execute($select);

foreach ($categories as $category) {

    $categoryId = (int) $category['id'];
    $slug = \Newspress\Url::createSlug($categoryId, 'categories');

    $update = $sql->update();
    $update->table('categories')
           ->set(array(
                'slug' => $slug,
           ))
           ->where(array('id' => $categoryId));

    \Newspress::db()->execute($update);

    \Newspress\Cli::uiMessage('Updated category with ID: ' . $categoryId . ' with slug \'' . $slug . '\'');

}
