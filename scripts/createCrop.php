<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


// $crop = 'thumbnail';

// \Newspress\Cli::uiMessage('Width: '  . \Newspress::config()->image->crop->$crop->width);
// \Newspress\Cli::uiMessage('Height: ' . \Newspress::config()->image->crop->$crop->height);
// \Newspress\Cli::uiMessage('Size: ' . \Newspress::config()->image->crop->$crop->size);
// \Newspress\Cli::uiMessage('Colour: ' . \Newspress::config()->image->crop->$crop->colour);

// $image = new \Newspress\Asset\Image();
// $image->setName(time() . '-image.jpg');
// $image->setCrop($crop);
// $image->upload('/Users/oliver/Desktop/image.jpg');

// \Newspress\Cli::getSummary();

// $select = $sql->select();
// $select->from('media')
//        ->columns(array('id', 'name'));
// $select->where
//        ->greaterThan('id', $imageIdOffset)
//        ->equalTo('type', 'image');
// $select->order('id DESC')
//        ->limit($limit)
//        ->offset($offset);

// Echo the query on the command line if needed (for debugging)
// \Newspress\Cli::uiMessage('Image import batch query: ' . $sql->getSqlStringForSqlObject($select));

// $results = \Newspress::db()->execute($select);

$crop = 'dropdown';
$dontOverwriteExistingCrops = false;
$imageIds = array(2226, 1228, 3703, 4584, 2921, 2584, 4970, 2797, 2238, 2803, 207, 388, 304, 2738, 1871, 2793, 1842, 2135);

// Go through results of current batch
foreach ($imageIds as $imageId) {
   
    $image = new \Newspress\Asset\Image($imageId);

    // Check to see if the crop exists already
    if ($dontOverwriteExistingCrops && $image->objectExists($crop)) {
        \Newspress\Cli::uiMessage('Crop "' . $crop . '" with image ID: ' . $imageId . ' already exists... skipping');
        continue;
    }

    // Generate and upload the crop
    $image->createCrop($crop);

    \Newspress\Cli::uiMessage('Generated "' . $crop . '" crop for image with ID: ' . $imageId);

}