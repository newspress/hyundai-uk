<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


/**
 * Import all release PDFs and attach them to the release.
 *
 * @todo Add functionality to see documents for a specific locale
 */

// Remove documents attached to releases from the media_links table
// and the media table
// 
// SELECT * FROM `media_links`
// LEFT JOIN `media` ON `media`.`id` = `media_links`.`media_id`
// WHERE `media_links`.`object` = 1 AND `media`.`type` = 'document';
//
//
// SELECT * FROM `media`
// LEFT JOIN `media_links` ON `media_links`.`media_id` = `media`.`id`
// WHERE `media_links`.`object` = 1 AND `media`.`type` = 'document';
// 

// Change this variable if we want to overwrite the current PDF documents that
// have been uploaded to S3 originally. This will be used when we have changed
// the PDF template and want to update the release PDFs.
$overwrite = true;

$sql = \Newspress::db()->sql();
$now = new \DateTime();

$select = $sql->select();
$select->from('releases');
$select->order('id DESC');

\Newspress\Cli::uiMessage('Selecting all releases from the database');

$releases = \Newspress::db()->execute($select);

foreach ($releases as $release) {

    $localeId = 1;

    // Do the English one first
    generatePdf($release, $localeId, $overwrite);

    // Go through all release translations
    $select = $sql->select();
    $select->from('releases_translations');
    $select->where
           ->equalTo('release', (int) $release['id'])
           ->notEqualTo('locale', 1);

    $releaseTranslations = \Newspress::db()->execute($select);

    foreach ($releaseTranslations as $releaseTranslation) {
        $release['title'] = empty($releaseTranslation['title']) ? $release['title'] : $releaseTranslation['title'];
        $release['content'] = empty($releaseTranslation['content']) ? $release['content'] : $releaseTranslation['content'];
        generatePdf($release, $releaseTranslation['locale'], $overwrite);
    }

}

function generatePdf($release, $localeId, $overwrite = false) {

    $release = (object) $release;
    $sql = \Newspress::db()->sql();

    $select = $sql->select();
    $select->from('media_links')
           ->where
           ->equalTo('locale', (int) $localeId)
           ->equalTo('object', 1)
           ->equalTo('object_id', (int) $release->id);

    $check = \Newspress::db()->execute($select);

    if ($check->count() > 0 && $overwrite === false) {
        \Newspress\Cli::uiError('Media link already exists for release with ID: ' . $release->id . ' and locale ID: ' . $localeId);
        return false;
    }

    ob_start();
    include 'module/Releases/view/releases/partials/pdf.phtml';
    $html = ob_get_contents();
    ob_end_clean();

    $dompdf = new \DOMPDF();
    $dompdf->load_html($html);
    $dompdf->render();
    $string = $dompdf->output();

    $localFilename = tempnam("/tmp", 'php-document-');
    file_put_contents($localFilename, $string);

    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $mime = finfo_file($finfo, $localFilename);
    finfo_close($finfo);

    $size = @filesize($localFilename);
    if (empty($size)) {
        $size = null;
    }

    $now = new \DateTime($release->published);
    $name = $release->title;
    $filename = \Newspress\String::makeUrlFriendly($release->title);

    if ($check->count() > 0 && $overwrite) {

        $checkResult = $check->current();
        $documentId = (int) $checkResult['media_id'];

        $update = $sql->update();
        $update->table('media')
               ->set(array(
                    'name'  => $filename,
                    'title' => $name,
                    'acl'   => \Aws\S3\Enum\CannedAcl::PUBLIC_READ,
                    'mime'  => $mime,
                    'size'  => (int) $size
               ))
               ->where(array('id' => $documentId));

        \Newspress::db()->execute($update);

        \Newspress\Cli::uiMessage('Updated media table for document with ID: ' . $documentId);

    } else {

        $insert = $sql->insert();
        $insert->into('media')
               ->columns(array('id', 'legacy_id', 'type', 'name', 'alt', 'title', 'description', 'keywords', 'acl', 'mime', 'size', 'author', 'status', 'published', 'modified', 'created'))
               ->values(array(
                        'id'          => null,
                        'legacy_id'   => null,
                        'type'        => 'document',
                        'name'        => $filename,
                        'alt'         => null,
                        'title'       => $name,
                        'description' => null,
                        'keywords'    => null,
                        'acl'         => \Aws\S3\Enum\CannedAcl::PUBLIC_READ,
                        'mime'        => $mime,
                        'size'        => (int) $size,
                        'author'      => 1,
                        'status'      => 'active',
                        'published'   => $now->format('Y-m-d H:i:s'),
                        'modified'    => $now->format('Y-m-d H:i:s'),
                        'created'     => $now->format('Y-m-d H:i:s'),
                    ));

        \Newspress::db()->execute($insert);
        $documentId = \Newspress::db()->getInsertId();

        \Newspress\Cli::uiMessage('Inserted document into media table with ID: ' . $documentId);

    }

    \Newspress\Cli::uiMessage('Uploading document to S3 with ID: ' . $documentId);

    $document = new \Newspress\Asset\Document();
    $document->setId($documentId);
    $document->setName($filename);
    $document->setMime($mime);
    $document->upload($localFilename);

    unlink($localFilename);

    if ($check->count() == 0 || $overwrite === false) {

        \Newspress\Cli::uiMessage('Completed upload for document with ID: ' . $documentId);

        $insert = $sql->insert();
        $insert->into('media_links')
               ->columns(array('id', 'locale', 'media_id', 'object', 'object_id'))
               ->values(array(
                    'id'        => null,
                    'locale'    => (int) $localeId,
                    'media_id'  => (int) $documentId,
                    'object'    => 1,
                    'object_id' => (int) $release->id
                ));

        \Newspress::db()->execute($insert);
        $mediaLinkId = \Newspress::db()->getInsertId();

        \Newspress\Cli::uiMessage('Attached document link into media_links table with media link ID: ' . $mediaLinkId);
    } else {
        \Newspress\Cli::uiMessage('Completed replacement upload for document with ID: ' . $documentId);
    }

    return true;

}
