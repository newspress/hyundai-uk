<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';

\Newspress\Debug::startTimer('rss_feed');
\Newspress\Cli::uiMessage('Querying database and building RSS feed');

$name = \Newspress::config()->site->name;
$email = 'media@mclaren.com'; // \Newspress::config()->site->email;
$url = 'http://' . \Newspress::config()->site->url;

$feed = new \Zend\Feed\Writer\Feed();
$feed->setTitle($name . ' RSS Feed');
$feed->setDescription('Media website RSS feed. Use this feed to keep up to date with the lastest information published on the media website.');
$feed->setGenerator($name . ' (' . $url . ')');
$feed->setLink($url);
$feed->setFeedLink($url . '/rss', 'rss');
$feed->addAuthor(array(
    'name'  => $name,
    'email' => $email,
    'uri'   => $url,
));
$feed->setDateModified(time());

$sql = \Newspress::db()->sql();
$now = new \Newspress\Date();

$select = $sql->select();
$select->from('releases');
$select->where
       ->lessThan('published', $now->format('Y-m-d H:i:s'))
       ->equalTo('status', 'active');
$select->order('published DESC');

// Change this so the queries are limited (check the import.php script)

$releases = \Newspress::db()->execute($select);

foreach ($releases as $release) {
    $entry = $feed->createEntry();
    $entry->setTitle($release['title']);
    $entry->setLink($url . '/releases/' . $release['id']);
    $entry->setDateModified(new \DateTime($release['modified']));
    $entry->setDateCreated(new \DateTime($release['published']));
    if (!empty($release['excerpt'])) {
        $entry->setDescription($release['excerpt']);
    }
    if (!empty($release['content'])) {
        $entry->setContent($release['content']);
    }
    $feed->addEntry($entry);
}

\Newspress\Cli::uiMessage('Adding XML data to file');

file_put_contents(__DIR__ . '/../data/feed.xml', $feed->export('rss'));

\Newspress\Cli::uiMessage('XML generation has been completed and took ' . \Newspress\Debug::stopTimer('rss_feed') . 's');
