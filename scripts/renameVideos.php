<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


$sql = \Newspress::db()->sql();
$now = new \DateTime();
$videoIdStart = 3817;


$select = $sql->select();
$select->columns(array(
           'max' => new \Zend\Db\Sql\Expression('MAX(`legacy_id`)')
       ))
       ->from('media');
$select->where
       ->equalTo('type', 'video');

$results = \Newspress::db()->execute($select);

$max = ($results->current()['max'] === null) ? 0 : (int) $results->current()['max'];

\Newspress\Cli::uiMessage('Max legacy ID in media table: ' . $max);

\Newspress\Cli::uiMessage('Counting videos');
$select = $sql->select();
$select->columns(array(
            'count' => new \Zend\Db\Sql\Expression('COUNT(*)')
        ))
        ->from('media')
        ->where
        ->greaterThan('id', 2995)
        ->equalTo('type', 'video');

$results = \Newspress::db()->execute($select);

// Set defaults for batch select queries
$offset = 0;
$limit = 100;
$total = (int) $results->current()['count'];

/**
 * Start selecting videos in batches
 */
\Newspress\Cli::uiMessage('Starting rename of ' . $total . ' videos');
while ($total > $offset) {

    $select = $sql->select();
    $select->from('media')
           ->where
           ->greaterThan('id', 2995)
           ->equalTo('type', 'video');
    $select->order('id ASC')
           ->limit($limit)
           ->offset($offset);

    // Echo the query on the command line if needed (for debugging)
    // \Newspress\Cli::uiMessage('Video select query: ' . $sql->getSqlStringForSqlObject($select));
    // exit;

    $videos = \Newspress::db()->execute($select);

    foreach ($videos as $video) {

        \Newspress\Cli::uiMessage('Changing video ' . $video['name'] . ' ID from: ' . $video['id'] . ' to: ' . $videoIdStart);


        $videoObject = new \Newspress\Asset\Video((int) $video['id']);

        $sourceKeyname = $videoObject->getFileBucketPath();
        $sourceKeynameThumbnail = $videoObject->getFileBucketPath('thumbnail');
        $sourceId = $videoObject->getId();
        $targetBucket = $videoObject->getBucket();

        $update = $sql->update();
        $update->table('media')
               ->set(array(
                    'id' => $videoIdStart,
               ))
               ->where(array('id' => (int) $video['id']));

        // Echo the query on the command line if needed (for debugging)
        // \Newspress\Cli::uiMessage('Video update query: ' . $sql->getSqlStringForSqlObject($update));
        // exit;

        \Newspress::db()->execute($update);

        // Change the ID
        $videoObject->setId($videoIdStart);

        \Newspress\Cli::uiMessage('Starting rename of video');

        // Rename the video in S3
        try {

            $videoObject->s3->copyObject(array(
                'Bucket'     => $targetBucket,
                'Key'        => $videoObject->getFileBucketPath(),
                'CopySource' => "{$targetBucket}/{$sourceKeyname}",
            ));

        } catch (\Aws\S3\Exception\S3Exception $e) {
            \Newspress\Debug::logError('Could not copy video object with ID: ' . $sourceId . ' with message: ' . $e->getMessage());
            continue;
        }

        $result = $videoObject->s3->deleteObject(array(
            'Bucket' => $targetBucket,
            'Key'    => $sourceKeyname
        ));

        \Newspress\Cli::uiMessage('Completed rename of video');

        \Newspress\Cli::uiMessage('Starting rename of thumbnail');

        // Rename the thumbnail in S3
        try {

            $videoObject->s3->copyObject(array(
                'Bucket'     => $targetBucket,
                'Key'        => $videoObject->getFileBucketPath('thumbnail'),
                'CopySource' => "{$targetBucket}/{$sourceKeynameThumbnail}",
            ));

        } catch (\Aws\S3\Exception\S3Exception $e) {
            \Newspress\Debug::logError('Could not copy video thumbnail object with ID: ' . $sourceId . ' with message: ' . $e->getMessage());
            continue;
        }

        $result = $videoObject->s3->deleteObject(array(
            'Bucket' => $targetBucket,
            'Key'    => $sourceKeynameThumbnail
        ));

        \Newspress\Cli::uiMessage('Completed rename of thumbnail');

        
        \Newspress\Cli::uiMessage('Completed rename with new ID: ' . $videoIdStart);


        $videoIdStart++;

    }

    // Add the batch limit to the offset
    $offset += $limit;

}
