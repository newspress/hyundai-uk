<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


/**
 * Import releases from the old database into the new one.
 * This script should also be able to import other media websites.
 *
 * To run this script, ensure you have imported the old database using the
 * prefix 'legacy_' on all tables.
 */


// @todo: Add warning to this script as it should only be used once (before any data has been added)
// \Newspress\Cli::uiError('This script should only be used once, before the website is in production');
// exit;

// @todo: Add start time and timer, roughly work out an estimated time for each section?
// @todo: Upon counting, remove one so the last image that failed can be re-uploaded



/**
* utf Decode - these function will deal with any decoding issues, also 
* if you don't know how many times your string has been encoded it will handle it.
* Currently only used when inserting data into releases_translations for column title and content
*/

function _utf8_decode($string)
{
  $tmp = $string;
  $count = 0;
  
  while (mb_detect_encoding($tmp)=="UTF-8")
  {
    $tmp = utf8_decode($tmp);
    $count++;
  }
  
  for ($i = 0; $i < $count-1 ; $i++)
  {
    $string = utf8_decode($string);
    
  }
  return $string;
  
}


/**
 * Set functions to run (if not defined by the developer)
 * 
 * @usage php scripts/importReleases.php development (By default)
 *        php scripts/importReleases.php development releases images (By defined values)
 */
$runDefaults = array(
        'images',
        'videos',
        'documents',
        'document_links',
        'model_video_links',
        'releases',
        'categories',
        'truncate',
        'index',
    );

if (isset($argv[2])) {
    $run = array_splice($argv, 2);
    foreach ($run as $function) {
        if (!in_array($function, $runDefaults)) {
            \Newspress\Cli::uiError('One (or more) of your defined functions do not exist');
            exit;
        }
    }
} else {
    $run = $runDefaults;
}


/**
 * Make a new instance of the Select class
 */
\Newspress\Cli::uiMessage('Creating an instance of the database class');
$sql = \Newspress::db()->sql();


/**
 * Make a new instance of the Elasticsearch service
 */
\Newspress\Cli::uiMessage('Creating an instance of the Elasticsearch service');
$esClient = new \Newspress\Service\Elasticsearch();


/**
 * Delete the Elasticsearch to start from fresh
 */
if (in_array('index', $run)) {
    \Newspress\Cli::uiMessage('Deleting the current Elasticsearch index');
    $esClient->deleteIndex();
    \Newspress\Cli::uiMessage('Please re-run the script');
    exit;
}


/**
 * Make a new instance of the Rackspace Cloudfile API
 */
if (in_array('images', $run) || in_array('videos', $run) || in_array('documents', $run)) {
    \Newspress\Cli::uiMessage('Creating an instance of the Rackspace OpenCloud API');
    $rackspaceClient = new \OpenCloud\Rackspace(\OpenCloud\Rackspace::US_IDENTITY_ENDPOINT, array(
        'username' => \Newspress::config()->rackspace->api->username,
        'apiKey'   => \Newspress::config()->rackspace->api->key
    ));
    $rackspaceObjectStoreService = $rackspaceClient->objectStoreService(null, \Newspress::config()->rackspace->api->region);
    $rackspaceContainer = $rackspaceObjectStoreService->getContainer(\Newspress::config()->rackspace->api->container);
}

/**
 * Make an array of all the available locales including their old IDs.
 * Get an array of the old locales and assign the new locale to the array
 */
\Newspress\Cli::uiMessage('Fetching locales');
$locales = \Newspress::getLocales();

$select = $sql->select();
$select->from('legacy_locales');

$results = \Newspress::db()->execute($select);

$oldLocales = array();

foreach ($results as $result) {

    $result['id'] = (int) $result['id'];
    $result['name'] = trim($result['name']);

    foreach ($locales as $locale) {
        if ($result['name'] == $locale['name']) {
            $result['new_id'] = (int) $locale['id'];
        }
    }

    $oldLocales[$result['id']] = $result;
}


/**
 * Truncate all tables to import new data
 */
if (in_array('truncate', $run)) {
    \Newspress\Cli::uiMessage('Truncating tables');
    if (in_array('releases', $run)) {
        \Newspress::db()->truncate('releases');
        \Newspress::db()->truncate('releases_translations');
        \Newspress::db()->truncate('media_links');
    }
    if (in_array('images', $run)) {
        // \Newspress::db()->truncate('media'); // @todo: Think about... Hmmmm...
    }
    if (in_array('categories', $run)) {
        \Newspress::db()->truncate('categories');
        // \Newspress::db()->truncate('category_links');
    }
}
// @todo: Create image indexed database with links to releases (once you've done that, truncate it here)


/**
 * Delete ALL objects from S3
 *
 * @todo Change this to only include images
 */
if (in_array('truncate', $run) && in_array('images', $run)) {
    \Newspress\Cli::uiMessage('Removing assets from S3');
    $asset = new \Newspress\Asset\Image();
    $asset->clearBucket();
}



/**
 * Import categories
 */
if (in_array('categories', $run)) {

    \Newspress\Cli::uiMessage('Starting import of main categories');

    // Insert the main categories
    $mainCategories = array(
            'models',
            'images',
            'videos',
            'documents'
        );

    $now = new \DateTime();

    foreach ($mainCategories as $mainCategory) {

        $insert = $sql->insert();
        $insert->into('categories')
               ->columns(array('id', 'legacy_id', 'legacy_table', 'slug', 'parent', 'parent_path', 'name', 'sort', 'status', 'modified', 'created'))
               ->values(array(
                    'id'           => null,
                    'legacy_id'    => null,
                    'legacy_table' => null,
                    'slug'         => null,
                    'parent'       => null,
                    'parent_path'  => null,
                    'name'         => ucwords($mainCategory),
                    'sort'         => null,
                    'status'       => 'active',
                    'modified'     => null,
                    'created'      => $now->format('Y-m-d H:i:s')
                ));

        \Newspress::db()->execute($insert);

        $categoryId = \Newspress::db()->getInsertId();

        \Newspress\Cli::uiMessage('Imported category with ID: ' . $categoryId);

    }


    /**
     * Import model categories
     */

    $select = $sql->select();
    $select->from('legacy_models')
           ->columns(array('id', 'sid', 'name', 'sort'));
    $select->order('id ASC');

    // Echo the query on the command line if needed (for debugging)
    // \Newspress\Cli::uiMessage('Category import batch query: ' . $sql->getSqlStringForSqlObject($select));
    // exit;

    $results = \Newspress::db()->execute($select);

    // Go through results of current batch
    foreach ($results as $result) {

        $insert = $sql->insert();
        $insert->into('categories')
               ->columns(array('id', 'legacy_id', 'legacy_table', 'slug', 'parent', 'parent_path', 'name', 'sort', 'status', 'modified', 'created'))
               ->values(array(
                    'id'           => null,
                    'legacy_id'    => (int) $result['id'],
                    'legacy_table' => 'legacy_models',
                    'slug'         => null,
                    'parent'       => 1,
                    'parent_path'  => '1',
                    'name'         => str_replace('Mclaren', 'McLaren', ucwords(strtolower($result['name']))),
                    'sort'         => (int) $result['sort'],
                    'status'       => 'active',
                    'modified'     => null,
                    'created'      => $now->format('Y-m-d H:i:s')
                ));

        \Newspress::db()->execute($insert);

        $categoryId = \Newspress::db()->getInsertId();

        \Newspress\Cli::uiMessage('Imported category with ID: ' . $categoryId);

    }







    /**
     * Go through each category
     */

    $categories = array(
            0 => array(
                'mainCategoryId' => 2,
                'table' => 'legacy_images_categories',
            )
        );

    foreach ($categories as $category) {

        \Newspress\Cli::uiMessage('Counting categories');
        $select = $sql->select();
        $select->columns(array(
                   'count' => new \Zend\Db\Sql\Expression('COUNT(*)')
               ))
               ->from($category['table']);

        $results = \Newspress::db()->execute($select);

        // Set defaults for batch select queries
        $offset = 0;
        $limit = 100;
        $total = (int) $results->current()['count'];

        /**
         * Start selecting images in batches
         */
        \Newspress\Cli::uiMessage('Starting import of ' . $total . ' image categories');

        while ($total > $offset) {

            // Select the releases (within the specified batch) joining the English translation
            $select = $sql->select();
            $select->from($category['table'])
                   ->columns(array('id', 'pid', 'type', 'title', 'sort'));
            $select->order('pid ASC')
                   ->limit($limit)
                   ->offset($offset);

            // Echo the query on the command line if needed (for debugging)
            // \Newspress\Cli::uiMessage('Category import batch query: ' . $sql->getSqlStringForSqlObject($select));
            // exit;

            $results = \Newspress::db()->execute($select);

            // Go through results of current batch
            foreach ($results as $result) {

                $insert = $sql->insert();
                $insert->into('categories')
                       ->columns(array('id', 'legacy_id', 'legacy_table', 'slug', 'parent', 'parent_path', 'name', 'sort', 'status', 'modified', 'created'))
                       ->values(array(
                            'id'           => null,
                            'legacy_id'    => (int) $result['id'],
                            'legacy_table' => $category['table'],
                            'slug'         => null,
                            'parent'       => null,
                            'parent_path'  => null,
                            'name'         => str_replace('Mclaren', 'McLaren', ucwords(strtolower($result['title']))),
                            'sort'         => (int) $result['sort'],
                            'status'       => 'active',
                            'modified'     => null,
                            'created'      => $now->format('Y-m-d H:i:s')
                        ));

                \Newspress::db()->execute($insert);

                $categoryId = \Newspress::db()->getInsertId();

                \Newspress\Cli::uiMessage('Imported category with ID: ' . $categoryId);

            }

            // Add the batch limit to the offset
            $offset += $limit;

        }

        \Newspress\Cli::uiMessage('Counting new categories');
        $select = $sql->select();
        $select->columns(array(
                   'count' => new \Zend\Db\Sql\Expression('COUNT(*)')
               ))
               ->from('categories');
        $select->where
               ->isNotNull('legacy_id')
               ->equalTo('legacy_table', $category['table']);

        $results = \Newspress::db()->execute($select);

        // Set defaults for batch select queries
        $offset = 0;
        $limit = 100;
        $total = (int) $results->current()['count'];

        /**
         * Start selecting images in batches
         */
        \Newspress\Cli::uiMessage('Starting parent attachment for ' . $total . ' image categories');

        while ($total > $offset) {

            // Select the releases (within the specified batch) joining the English translation
            $select = $sql->select();
            $select->from('categories')
                   ->columns(array('id', 'legacy_id'));
            $select->where
                   ->isNotNull('legacy_id')
                   ->equalTo('legacy_table', $category['table']);
            $select->order('id ASC')
                   ->limit($limit)
                   ->offset($offset);

            // Echo the query on the command line if needed (for debugging)
            // \Newspress\Cli::uiMessage('Category import batch query: ' . $sql->getSqlStringForSqlObject($select));
            // exit;

            $results = \Newspress::db()->execute($select);

            // Go through results of current batch
            foreach ($results as $result) {

                $categoryId = (int) $result['id'];
                $legacyId = (int) $result['legacy_id'];

                $select = $sql->select();
                $select->from($category['table'])
                       ->columns(array('pid'));
                $select->where
                       ->equalTo('id', $legacyId);
                $select->limit(1);

                $legacyParentResult = \Newspress::db()->execute($select);
                $legacyParentResult = $legacyParentResult->current();

                $legacyParent = (int) $legacyParentResult['pid'];

                $parent = 0;
                $parentPath = (string) $category['mainCategoryId'];

                if ($legacyParent > 0) {

                    while (true) {

                        $select = $sql->select();
                        $select->from('categories')
                               ->columns(array('id'));
                        $select->where
                               ->equalTo('legacy_id', $legacyParent)
                               ->equalTo('legacy_table', $category['table']);
                        $select->limit(1);

                        $parentResult = \Newspress::db()->execute($select);
                        $parentResult = $parentResult->current();

                        $parent = (int) $parentResult['id'];

                        if ($parent === 0) {
                            break;
                        }

                        $parentPath .= ',' . $parent;

                        $select = $sql->select();
                        $select->from($category['table'])
                               ->columns(array('pid'));
                        $select->where
                               ->equalTo('id', $legacyParent);
                        $select->limit(1);

                        $legacyParentResult = \Newspress::db()->execute($select);
                        $legacyParentResult = $legacyParentResult->current();

                        $legacyParent = (int) $legacyParentResult['pid'];

                        if ($legacyParent === 0) {
                            $parent = $category['mainCategoryId'];
                            break;
                        }

                    }

                }

                // Get the parent from the parent path
                $parent = explode(',', $parentPath);
                $parent = end($parent);

                $update = $sql->update();
                $update->table('categories')
                       ->set(array(
                            'parent' => $parent,
                            'parent_path' => $parentPath,
                       ))
                       ->where(array('id' => $result['id']));

                \Newspress::db()->execute($update);

                \Newspress\Cli::uiMessage('Updated parent for category: ' . $result['id']);

            }

            // Add the batch limit to the offset
            $offset += $limit;

        }

    }


    /**
     * @todo: Update this to use object_links
     */
    // \Newspress\Cli::uiMessage('Counting category image links');
    // $select = $sql->select();
    // $select->columns(array(
    //            'count' => new \Zend\Db\Sql\Expression('COUNT(*)')
    //        ))
    //        ->from('legacy_images_cats');

    // $results = \Newspress::db()->execute($select);


    // // Set defaults for batch select queries
    // $offset = 0;
    // $limit = 100;
    // $total = (int) $results->current()['count'];

    // /**
    //  * Start selecting images in batches
    //  */
    // \Newspress\Cli::uiMessage('Starting import of ' . $total . ' image category links');

    // while ($total > $offset) {

    //     // Select the releases (within the specified batch) joining the English translation
    //     $select = $sql->select();
    //     $select->from('legacy_images_cats')
    //            ->columns(array('iid', 'cid', 'sort'));
    //     $select->order('iid ASC')
    //            ->limit($limit)
    //            ->offset($offset);

    //     // Echo the query on the command line if needed (for debugging)
    //     // \Newspress\Cli::uiMessage('Category import batch query: ' . $sql->getSqlStringForSqlObject($select));
    //     // exit;

    //     $results = \Newspress::db()->execute($select);

    //     // Go through results of current batch
    //     foreach ($results as $result) {

    //         // Get the image ID using the legacy_id
    //         $select = $sql->select();
    //         $select->from('media')
    //                ->columns(array('id'));
    //         $select->where
    //                ->equalTo('legacy_id', (int) $result['iid']);
    //         $mediaId = \Newspress::db()->execute($select);
    //         $mediaId = $mediaId->current();
    //         $mediaId = (int) $mediaId['id'];

    //         // Get the categry id with the legacy_id
    //         $select = $sql->select();
    //         $select->from('categories')
    //                ->columns(array('id'));
    //         $select->where
    //                ->equalTo('legacy_id', (int) $result['cid'])
    //                ->equalTo('legacy_table', 'legacy_images_categories');
    //         $categoryId = \Newspress::db()->execute($select);
    //         $categoryId = $categoryId->current();
    //         $categoryId = (int) $categoryId['id'];

    //         if ($categoryId == 0 || $mediaId == 0) {
    //             \Newspress\Cli::uiError('Skipping as category ID: ' . $categoryId . ' or media ID: ' . $mediaId . ' is null');
    //             continue;
    //         }

    //         $insert = $sql->insert();
    //         $insert->into('category_links')
    //                ->columns(array('id', 'category_id', 'object', 'object_id', 'sort'))
    //                ->values(array(
    //                     'id'          => null,
    //                     'category_id' => $categoryId,
    //                     'object'      => 2,
    //                     'object_id'   => $mediaId,
    //                     'sort'        => (int) $result['sort'],
    //                 ));

    //         \Newspress::db()->execute($insert);

    //         $categoryId = \Newspress::db()->getInsertId();

    //         \Newspress\Cli::uiMessage('Imported category link with ID: ' . $categoryId);

    //     }

    //     // Add the batch limit to the offset
    //     $offset += $limit;

    // }







    /**
     * @todo: Import video category_links
     */
    // \Newspress\Cli::uiMessage('Counting category video links');
    // $select = $sql->select();
    // $select->columns(array(
    //            'count' => new \Zend\Db\Sql\Expression('COUNT(*)')
    //        ))
    //        ->from('legacy_videos_cats');

    // $results = \Newspress::db()->execute($select);

    // // Set defaults for batch select queries
    // $offset = 0;
    // $limit = 100;
    // $total = (int) $results->current()['count'];

    // /**
    //  * Start selecting images in batches
    //  */
    // \Newspress\Cli::uiMessage('Starting import of ' . $total . ' video category links');

    // while ($total > $offset) {

    //     // Select the releases (within the specified batch) joining the English translation
    //     $select = $sql->select();
    //     $select->from('legacy_images_cats')
    //            ->columns(array('iid', 'cid', 'sort'));
    //     $select->order('iid ASC')
    //            ->limit($limit)
    //            ->offset($offset);

    //     // Echo the query on the command line if needed (for debugging)
    //     // \Newspress\Cli::uiMessage('Category import batch query: ' . $sql->getSqlStringForSqlObject($select));
    //     // exit;

    //     $results = \Newspress::db()->execute($select);

    //     // Go through results of current batch
    //     foreach ($results as $result) {

    //         // Get the image ID using the legacy_id
    //         $select = $sql->select();
    //         $select->from('media')
    //                ->columns(array('id'));
    //         $select->where
    //                ->equalTo('legacy_id', (int) $result['iid']);
    //         $mediaId = \Newspress::db()->execute($select);
    //         $mediaId = $mediaId->current();
    //         $mediaId = (int) $mediaId['id'];

    //         // Get the categry id with the legacy_id
    //         $select = $sql->select();
    //         $select->from('categories')
    //                ->columns(array('id'));
    //         $select->where
    //                ->equalTo('legacy_id', (int) $result['cid'])
    //                ->equalTo('legacy_table', 'legacy_images_categories');
    //         $categoryId = \Newspress::db()->execute($select);
    //         $categoryId = $categoryId->current();
    //         $categoryId = (int) $categoryId['id'];

    //         if ($categoryId == 0 || $mediaId == 0) {
    //             \Newspress\Cli::uiError('Skipping as category ID: ' . $categoryId . ' or media ID: ' . $mediaId . ' is null');
    //             continue;
    //         }

    //         $insert = $sql->insert();
    //         $insert->into('category_links')
    //                ->columns(array('id', 'category_id', 'object', 'object_id', 'sort'))
    //                ->values(array(
    //                     'id'          => null,
    //                     'category_id' => $categoryId,
    //                     'object'      => 2,
    //                     'object_id'   => $mediaId,
    //                     'sort'        => (int) $result['sort'],
    //                 ));

    //         \Newspress::db()->execute($insert);

    //         $categoryId = \Newspress::db()->getInsertId();

    //         \Newspress\Cli::uiMessage('Imported category link with ID: ' . $categoryId);

    //     }

    //     // Add the batch limit to the offset
    //     $offset += $limit;

    // }




    /**
     * Import the releases models links
     * @todo Update this to use object_lnks
     */

    // \Newspress\Cli::uiMessage('Starting import for releases model category links');

    // $select = $sql->select();
    // $select->from('legacy_releases_models')
    //        ->columns(array('rid', 'mid'))
    //        ->join('releases', 'legacy_releases_models.rid = releases.legacy_id', array('release_id' => 'id'))
    //        ->join('categories', 'legacy_releases_models.mid = categories.legacy_id', array('category_id' => 'id'));
    // $select->where
    //        ->equalTo('categories.legacy_table', 'legacy_models');

    // // Echo the query on the command line if needed (for debugging)
    // // \Newspress\Cli::uiMessage('Query: ' . $sql->getSqlStringForSqlObject($select));
    // // exit;

    // $results = \Newspress::db()->execute($select);

    // // Go through results of current batch
    // foreach ($results as $result) {

    //     $insert = $sql->insert();
    //     $insert->into('object_links')
    //            ->columns(array('id', 'category_id', 'object', 'object_id', 'sort'))
    //            ->values(array(
    //                 'id'          => null,
    //                 'category_id' => (int) $result['category_id'],
    //                 'object'      => 1,
    //                 'object_id'   => (int) $result['release_id'],
    //                 'sort'        => null,
    //             ));

    //     \Newspress::db()->execute($insert);

    //     $categoryLinkId = \Newspress::db()->getInsertId();

    //     \Newspress\Cli::uiMessage('Imported release category link with ID: ' . $categoryLinkId);

    // }


}



/**
 * Imports all images from the old website into the new one. It will download
 * images from Rackspace and upload them to S3 and create crops that are
 * defined in the local configuration as it runs.
 * 
 * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
 */
if (in_array('images', $run)) {

    /**
     * Import all images into the database (save the old ID)?
     */
    \Newspress\Cli::uiMessage('Getting the max ID from images');
    $select = $sql->select();
    $select->columns(array(
               'max' => new \Zend\Db\Sql\Expression('MAX(`legacy_id`)')
           ))
           ->from('media');
    $select->where
           ->equalTo('type', 'image');

    $results = \Newspress::db()->execute($select);

    $max = ($results->current()['max'] === null) ? 0 : (int) $results->current()['max'];

    \Newspress\Cli::uiMessage('Max legacy ID in media table: ' . $max);

    \Newspress\Cli::uiMessage('Counting images');
    $select = $sql->select();
    $select->columns(array(
               'count' => new \Zend\Db\Sql\Expression('COUNT(*)')
           ))
           ->from('legacy_images')
           ->where
           ->greaterThan('id', $max);

    $results = \Newspress::db()->execute($select);

    // Set defaults for batch select queries
    $offset = 0;
    $limit = 100;
    $total = (int) $results->current()['count'];

    /**
     * Start selecting images in batches
     */
    \Newspress\Cli::uiMessage('Starting import of ' . $total . ' images');
    while ($total > $offset) {

        // Select the releases (within the specified batch) joining the English translation
        $select = $sql->select();
        $select->from('legacy_images')
               ->columns(array('id', 'title', 'description', 'keywords', 'basename', 'extension', 'timestamp_cre', 'timestamp_mod', 'timestamp_pub'))
               ->where
               ->greaterThan('id', $max);
        $select->order('id ASC')
               ->limit($limit)
               ->offset($offset);

        // Echo the query on the command line if needed (for debugging)
        // \Newspress\Cli::uiMessage('Image import batch query: ' . $sql->getSqlStringForSqlObject($select));

        $results = \Newspress::db()->execute($select);

        // Go through results of current batch
        foreach ($results as $result) {

            // @todo: Get the image from Rackspace using the ID and/or other data
            $rackspaceObject = $rackspaceContainer->getObject('data/images/' . $result['basename'] . '.' . $result['extension']);
            $rackspaceObjectContent = $rackspaceObject->getContent();
            $rackspaceObjectContent->rewind();

            // Save file to local - should we delete this afterwards or at least use remote files?
            $localFilename = tempnam("/tmp", 'php-opencloud-');
            file_put_contents($localFilename, $rackspaceObjectContent->getStream());

            // Get the image mime type (this is faster than finfo_file() for images)
            $mime = getimagesize($localFilename)['mime'];

            // Use this in replacement of getimagesize for a wider variaty of mime types (like videos)
            // $finfo = finfo_open(FILEINFO_MIME_TYPE);
            // $mime = finfo_file($finfo, $localFilename);
            // finfo_close($finfo);

            // Insert into new database
            $insert = $sql->insert();
            $insert->into('media')
                   ->columns(array('id', 'legacy_id', 'type', 'name', 'alt', 'title', 'description', 'keywords', 'acl', 'mime', 'author', 'status', 'published', 'modified', 'created'))
                   ->values(array(
                            'id'          => null,
                            'legacy_id'   => $result['id'],
                            'legacy_id'   => 'image',
                            'name'        => $result['basename'],
                            'alt'         => $result['title'],
                            'title'       => $result['title'],
                            'description' => $result['description'],
                            'keywords'    => $result['keywords'],
                            'acl'         => \Aws\S3\Enum\CannedAcl::PUBLIC_READ,
                            'mime'        => $mime,
                            'author'      => 1,
                            'status'      => 'active',
                            'published'   => $result['timestamp_pub'],
                            'modified'    => $result['timestamp_mod'],
                            'created'     => $result['timestamp_cre']
                        ));

            \Newspress::db()->execute($insert);

            $imageId = \Newspress::db()->getInsertId();

            $image = new \Newspress\Asset\Image();
            $image->setId($imageId);
            $image->setName($result['basename']);
            $image->setMime($mime);
            $image->setCrop('thumbnail');
            // @todo: Add additional crops if needed (which we will need eventually)
            $image->upload($localFilename);

            // Delete temporary file
            unlink($localFilename);

            \Newspress\Cli::uiMessage('Imported image with ID: ' . $imageId);

        }

        // Add the batch limit to the offset
        $offset += $limit;

    }

}



/**
 * Imports videos from the old website into the new one.
 * 
 * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
 */
if (in_array('videos', $run)) {

    /**
     * Import all videos into the database (save the old ID)?
     */
    \Newspress\Cli::uiMessage('Getting the max ID from videos');
    $select = $sql->select();
    $select->columns(array(
               'max' => new \Zend\Db\Sql\Expression('MAX(`legacy_id`)')
           ))
           ->from('media');
    $select->where
           ->equalTo('type', 'video');

    $results = \Newspress::db()->execute($select);

    $max = ($results->current()['max'] === null) ? 0 : (int) $results->current()['max'];

    \Newspress\Cli::uiMessage('Max legacy ID in media table: ' . $max);

    \Newspress\Cli::uiMessage('Counting videos');
    $select = $sql->select();
    $select->columns(array(
               'count' => new \Zend\Db\Sql\Expression('COUNT(*)')
           ))
           ->from('legacy_videos')
           ->where
           ->greaterThan('id', $max);

    $results = \Newspress::db()->execute($select);

    // Set defaults for batch select queries
    $offset = 0;
    $limit = 100;
    $total = (int) $results->current()['count'];


/*               
 var_dump($sql->getSqlStringForSqlObject($select));
 exit;*/

    /**
     * Start selecting videos in batches
     */
    \Newspress\Cli::uiMessage('Starting import of ' . $total . ' videos');
    while ($total > $offset) {

        // Select the releases (within the specified batch) joining the English translation
        $select = $sql->select();
        $select->from('legacy_videos')
               ->columns(array('id', 'title', 'description', 'keywords', 'image', 'timestamp_cre', 'timestamp_mod', 'timestamp_pub'))
               ->join('legacy_videos_vers', 'legacy_videos.id = legacy_videos_vers.vid', array('extension' => 'ext', 'file', 'size'))
               ->where
               ->greaterThan('legacy_videos.id', $max)
               ->equalTo('legacy_videos_vers.ver', 0);
        $select->order('id ASC')
               ->limit($limit)
               ->offset($offset);

        // Echo the query on the command line if needed (for debugging)
        // \Newspress\Cli::uiMessage('Video import batch query: ' . $sql->getSqlStringForSqlObject($select));
        // exit;

        $results = \Newspress::db()->execute($select);

        // Go through results of current batch
        foreach ($results as $result) {

            \Newspress\Cli::uiMessage('Downloading video from Rackspace with ID: ' . $result['id']);
            $rackspaceObject = $rackspaceContainer->getObject('data/videos/' . $result['id'] . '/' . $result['file'] . '.' . $result['extension']);
            $rackspaceObjectContent = $rackspaceObject->getContent();
            $rackspaceObjectContent->rewind();

            // Save file to local - should we delete this afterwards or at least use remote files?
            $localFilename = tempnam("/tmp", 'php-opencloud-');
            file_put_contents($localFilename, $rackspaceObjectContent->getStream());

            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($finfo, $localFilename);
            finfo_close($finfo);

            // Insert into new database
            $insert = $sql->insert();
            $insert->into('media')
                   ->columns(array('id', 'legacy_id', 'type', 'name', 'alt', 'title', 'description', 'keywords', 'acl', 'mime', 'size', 'author', 'status', 'published', 'modified', 'created'))
                   ->values(array(
                            'id'          => null,
                            'legacy_id'   => $result['id'],
                            'type'        => 'video',
                            'name'        => $result['file'],
                            'alt'         => $result['title'],
                            'title'       => $result['title'],
                            'description' => $result['description'],
                            'keywords'    => $result['keywords'],
                            'acl'         => \Aws\S3\Enum\CannedAcl::PUBLIC_READ,
                            'mime'        => $mime,
                            'size'        => (int) $result['size'],
                            'author'      => 1,
                            'status'      => 'active',
                            'published'   => $result['timestamp_pub'],
                            'modified'    => $result['timestamp_mod'],
                            'created'     => $result['timestamp_cre']
                        ));

            // \Newspress\Cli::uiMessage('Video insert query: ' . $sql->getSqlStringForSqlObject($insert));

            \Newspress::db()->execute($insert);

            $videoId = \Newspress::db()->getInsertId();

            \Newspress\Cli::uiMessage('Downloading video thumbnail from Rackspace with ID: ' . $videoId);

            $rackspaceObject = $rackspaceContainer->getObject('data/videos/' . $result['id'] . '/' . $result['image']);
            $rackspaceObjectContent = $rackspaceObject->getContent();
            $rackspaceObjectContent->rewind();

            $localThumbnailFilename = tempnam("/tmp", 'php-opencloud-thumbnail-');
            file_put_contents($localThumbnailFilename, $rackspaceObjectContent->getStream());

            \Newspress\Cli::uiMessage('Uploading video to S3 with ID: ' . $videoId);

            $video = new \Newspress\Asset\Video();
            $video->setId($videoId);
            $video->setName($result['file']);
            $video->setMime($mime);
            $video->createThumbnail('thumbnail', $localThumbnailFilename);
            $video->upload($localFilename);

            // Delete temporary files
            unlink($localFilename);
            unlink($localThumbnailFilename);

            \Newspress\Cli::uiMessage('Imported video with ID: ' . $videoId);

        }

        // Add the batch limit to the offset
        $offset += $limit;

    }

}



/**
 * Imports documents from the old website into the new one. This will import
 * documents straight from Rackspace's cloud files, download them and upload
 * them to S3.
 *
 * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
 */
if (in_array('documents', $run)) {

    /**
     * Import all documents into the database (save the old ID)?
     */
    \Newspress\Cli::uiMessage('Getting the max ID from documents');
    $select = $sql->select();
    $select->columns(array(
               'max' => new \Zend\Db\Sql\Expression('MAX(`legacy_id`)')
           ))
           ->from('media');
    $select->where
           ->equalTo('type', 'document');

    $results = \Newspress::db()->execute($select);

    $max = ($results->current()['max'] === null) ? 0 : (int) $results->current()['max'];

    \Newspress\Cli::uiMessage('Max legacy ID in media table: ' . $max);

    \Newspress\Cli::uiMessage('Counting documents');
    $select = $sql->select();
    $select->columns(array(
               'count' => new \Zend\Db\Sql\Expression('COUNT(*)')
           ))
           ->from('legacy_documents')
           ->where
           ->greaterThan('id', $max);

    $results = \Newspress::db()->execute($select);

    // Set defaults for batch select queries
    $offset = 0;
    $limit = 100;
    $total = (int) $results->current()['count'];

    /**
     * Start selecting documents in batches
     */
    \Newspress\Cli::uiMessage('Starting import of ' . $total . ' documents');
    while ($total > $offset) {

        // Select the releases (within the specified batch) joining the English translation
        $select = $sql->select();
        $select->from('legacy_documents')
               ->columns(array('id', 'pid', 'lid', 'module', 'title', 'file', 'size', 'type', 'timestamp_cre', 'timestamp_mod', 'timestamp_pub'))
               ->where
               ->greaterThan('id', $max)
               ->equalTo('module', 1);
        $select->order('id ASC')
               ->limit($limit)
               ->offset($offset);




        // Echo the query on the command line if needed (for debugging)
        // \Newspress\Cli::uiMessage('Video import batch query: ' . $sql->getSqlStringForSqlObject($select));
        // exit;

        $results = \Newspress::db()->execute($select);

        // Go through results of current batch
        foreach ($results as $result) {

            \Newspress\Cli::uiMessage('Downloading document from Rackspace with ID: ' . $result['id']);

            try {
                $rackspaceObject = $rackspaceContainer->getObject('data/documents/' . $result['file']);
                $rackspaceObjectContent = $rackspaceObject->getContent();
                $rackspaceObjectContent->rewind();
            } catch (\OpenCloud\ObjectStore\Exception\ObjectNotFoundException $e) {
                \Newspress\Cli::uiError('File does not exist with ID: ' . $result['id']);
                continue;
            }

            // Save file to local - should we delete this afterwards or at least use remote files?
            $localFilename = tempnam("/tmp", 'php-opencloud-');
            file_put_contents($localFilename, $rackspaceObjectContent->getStream());

            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($finfo, $localFilename);
            finfo_close($finfo);

            // Insert into new database
            $insert = $sql->insert();
            $insert->into('media')
                   ->columns(array('id', 'legacy_id', 'type', 'name', 'alt', 'title', 'description', 'keywords', 'acl', 'mime', 'size', 'author', 'status', 'published', 'modified', 'created'))
                   ->values(array(
                            'id'          => null,
                            'legacy_id'   => $result['id'],
                            'type'        => 'document',
                            'name'        => $result['file'],
                            'alt'         => null,
                            'title'       => $result['title'],
                            'description' => null,
                            'keywords'    => null,
                            'acl'         => \Aws\S3\Enum\CannedAcl::PUBLIC_READ,
                            'mime'        => $mime,
                            'size'        => (int) $result['size'],
                            'author'      => 1,
                            'status'      => 'active',
                            'published'   => $result['timestamp_pub'],
                            'modified'    => $result['timestamp_mod'],
                            'created'     => $result['timestamp_cre']
                        ));

            // \Newspress\Cli::uiMessage('Video insert query: ' . $sql->getSqlStringForSqlObject($insert));

            \Newspress::db()->execute($insert);

            $documentId = \Newspress::db()->getInsertId();

            \Newspress\Cli::uiMessage('Uploading document to S3 with ID: ' . $documentId);

            $document = new \Newspress\Asset\Document();
            $document->setId($documentId);
            $document->setName($result['file']);
            $document->setMime($mime);
            $document->upload($localFilename);

            // Delete temporary files
            unlink($localFilename);

            \Newspress\Cli::uiMessage('Imported document with ID: ' . $documentId);

        }

        // Add the batch limit to the offset
        $offset += $limit;

    }

}



/**
 * Imports all releases from the old database into the new one.
 *
 * @todo Fix issue with encoding
 * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
 */
if (in_array('releases', $run)) {

    /**
     * Count the number of releases to import
     */
    \Newspress\Cli::uiMessage('Counting releases');
    $select = $sql->select();
    $select->columns(array(
               'count' => new \Zend\Db\Sql\Expression('COUNT(*)')
           ))
           ->from('legacy_releases')
           ->where
           ->equalTo('status', 1);
    $results = \Newspress::db()->execute($select);

    // Set defaults for batch select queries
    $offset = 0;
    $limit = 100;
    $total = (int) $results->current()['count'];


    /**
     * Start selecting releases in batches
     */
    \Newspress\Cli::uiMessage('Starting import of ' . $total . ' releases');
    while ($total > $offset) {

        // Select the releases (within the specified batch) joining the English translation
        $select = $sql->select();
        $select->from('legacy_releases')
               ->columns(array('id', 'status', 'preview', 'timestamp_mod', 'timestamp_pub', 'timestamp_cre'))
               ->join('legacy_releases_vers', 'legacy_releases.id = legacy_releases_vers.rid', array('title', 'content' => 'data'), 'left')
               ->where
               ->equalTo('lid', 1)
               ->equalTo('status', 1);
        $select->order('id ASC')
               ->limit($limit)
               ->offset($offset);

        // Echo the query on the command line if needed (for debugging)
        // \Newspress\Cli::uiMessage($sql->getSqlStringForSqlObject($select));
        // exit;

        $results = \Newspress::db()->execute($select);

        // Go through results of current batch
        foreach ($results as $result) {

            $result['status'] = ($result['status'] == 1) ? 'active' : 'inactive';
            $result['id'] = (int) $result['id'];

            $insertData = array(
                    'id'        => null,
                    'legacy_id' => $result['id'],
                    'slug'      => null, // @todo: Create slug (and check slug doesn't exist)
                    'title'     => utf8_encode($result['title']),
                    'excerpt'   => utf8_encode($result['preview']),
                    'content'   => utf8_encode($result['content']),
                    'media'     => null,
                    'author'    => 1,
                    'status'    => $result['status'],
                    'modified'  => $result['timestamp_mod'],
                    'published' => $result['timestamp_pub'],
                    'created'   => $result['timestamp_cre']
                );

            // Insert into new database
            $insert = $sql->insert();
            $insert->into('releases')
                   ->columns(array('id', 'slug', 'title', 'excerpt', 'content', 'author', 'status', 'modified', 'published', 'created'))
                   ->values($insertData);

            \Newspress::db()->execute($insert);

            $releaseId = \Newspress::db()->getInsertId();

            // Add to elasticsearch index
            unset($insertData['id']);
            $insertData['translations'] = array();
            $esData = array();
            $esData['id'] = $releaseId;
            $esData['type'] = 'release';
            $esData['body'] = $insertData;
            $esClient->index($esData);

            // @todo: Get any videos and documents relating to the release
            
            /**
             * Create links between images and releases.
             * The link is the indexed values between the `media_links` table and the `releases` table.
             * The alternative view is the `media` column within the `releases` table.
             */
            
            $mediaIds = array();
            $releaseObjectId = 1; // @todo: Get this from the database (but that's another query...)

            $select = $sql->select('legacy_releases_imgs');
            $select->join('media', 'media.legacy_id = legacy_releases_imgs.iid', array('id'), 'left');
            $select->where
                   ->equalTo('rid', $result['id']);

            $legacyImageLinkResults = \Newspress::db()->execute($select);

            foreach ($legacyImageLinkResults as $legacyImageLinkResult) {

                // Skip images that don't exist
                if ((int) $legacyImageLinkResult['id'] == 0) {
                    continue;
                }

                $mediaIds[] = (int) $legacyImageLinkResult['id'];
                \Newspress::db()->query("INSERT IGNORE INTO `media_links` SET `media_id` = " . (int) $legacyImageLinkResult['id'] . ", `object` = " . (int) $releaseObjectId . ", `object_id` = " . (int) $releaseId . ";");
            }

            $update = $sql->update();
            $update->table('releases')
                   ->set(array('media' => implode(',', $mediaIds)))
                   ->where(array('id' => $releaseId));

            $legacyImageLinkResults = \Newspress::db()->execute($update);



            /**
             * Select all translations for that particular release
             */
            $select = $sql->select();
            $select->from('legacy_releases_vers')
                   ->where
                   ->equalTo('rid', $result['id']);
            $select->order('lid ASC');

            $languages = \Newspress::db()->execute($select);
            $releaseTranslations = array();

            foreach ($languages as $language) {

                // @todo: Do not include english translations (update the main releases table)

                // Convert language ID to locale ID
                $localeId = $oldLocales[$language['lid']]['new_id'];

                $childInsertData = array(
                        'id'      => null,
                        'release' => $releaseId,
                        'locale'  => $localeId,
                        'title'   => utf8_decode(utf8_encode($language['title'])),
                        'excerpt' => null,
                        'content' => utf8_decode(utf8_encode($language['data'])),
                    );

                // Insert language content into new database
                $insert = $sql->insert();
                $insert->into('releases_translations')
                       ->columns(array('id', 'release', 'locale', 'title', 'excerpt', 'content'))
                       ->values($childInsertData);

                // Echo the query on the command line if needed (for debugging)
                // \Newspress\Cli::uiMessage($sql->getSqlStringForSqlObject($insert));
                // exit;

                try {

                    \Newspress::db()->execute($insert);

                    $childInsertData['id'] = \Newspress::db()->getInsertId();

                    $releaseTranslations[] = array(
                            // @todo: Maybe change the key $localeId to the text version?
                            $localeId => $childInsertData
                        );

                    \Newspress\Cli::uiMessage('Imported release translation with ID: ' . $releaseId);

                } catch(\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
                    \Newspress\Cli::uiError('Could not insert translation for release: ' . $releaseId . ' with locale: ' . $localeId);
                }

                unset($childInsertData['release']);
            }

            // Update the elasticsearch index with the new translation data

            // $insertData['translations'] = $releaseTranslations;
            // $esData = array();
            // $esData['id'] = $releaseId;
            // $esData['type'] = 'release';
            // $esData['body']['doc'] = $insertData;
            // $esClient->update($esData);

            // Think about access control lists (permissions)

            \Newspress\Cli::uiMessage('Imported release with ID: ' . $releaseId);
        }


        // Add the batch limit to the offset
        $offset += $limit;
    }

}





if (in_array('document_links', $run)) {


    /**
     * Import document links with releases
     */
    \Newspress\Cli::uiMessage('Starting import for document links to releases');


    /**
     * @todo: Create links between documents and releases
     */
    $select = $sql->select('legacy_documents');
    $select->join('media', 'media.legacy_id = legacy_documents.id', array('media_id' => 'id'));
    $select->join('releases', 'releases.legacy_id = legacy_documents.pid', array('release_id' => 'id'));
    $select->where
           ->equalTo('media.type', 'document')
           ->equalTo('lid', 1) // @todo: Add documents for different locales
           ->equalTo('module', 1);

    // \Newspress\Cli::uiMessage($sql->getSqlStringForSqlObject($select));
    // exit;

    $results = \Newspress::db()->execute($select);

    foreach ($results as $result) {

        $insert = $sql->insert();
        $insert->into('media_links')
               ->columns(array('id', 'media_id', 'object', 'object_id'))
               ->values(array(
                    'id'        => null,
                    'media_id'  => $result['media_id'],
                    'object'    => 1,
                    'object_id' => $result['release_id']
                ));

        \Newspress::db()->execute($insert);

        $mediaLinkId = \Newspress::db()->getInsertId();

        \Newspress\Cli::uiMessage('Imported document category link with ID: ' . $mediaLinkId);

    }

}




if (in_array('model_video_links', $run)) {


    /**
     * Import document links with releases
     */
    \Newspress\Cli::uiMessage('Starting import for model video links to releases');


    /**
     * @todo: Create links between documents and releases
     */
    $select = $sql->select('legacy_models_vids');
    $select->join('media', 'media.legacy_id = legacy_models_vids.vid', array('media_id' => 'id'));
    $select->join('categories', 'categories.legacy_id = legacy_models_vids.mid', array('category_id' => 'id'));
    $select->where
           ->equalTo('media.type', 'video')
           ->equalTo('categories.legacy_table', 'legacy_models');

    // \Newspress\Cli::uiMessage($sql->getSqlStringForSqlObject($select));
    // exit;

    $results = \Newspress::db()->execute($select);

    foreach ($results as $result) {

        $insert = $sql->insert();
        $insert->into('media_links')
               ->columns(array('id', 'media_id', 'object', 'object_id'))
               ->values(array(
                    'id'        => null,
                    'media_id'  => $result['media_id'],
                    'object'    => 1,
                    'object_id' => $result['category_id']
                ));

        \Newspress::db()->execute($insert);

        $mediaLinkId = \Newspress::db()->getInsertId();

        \Newspress\Cli::uiMessage('Imported document category link with ID: ' . $mediaLinkId);

    }

}
