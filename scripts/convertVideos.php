<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


/**
 * Convert video files
 */

$sql = \Newspress::db()->sql();
$now = new \DateTime();

$select = $sql->select();
$select->from('media');
$select->where
       ->equalTo('type', 'video');

$videos = \Newspress::db()->execute($select, 'array');

\Newspress\Cli::uiMessage('Starting creation of jobs for ' . count($videos) . ' videos');

foreach ($videos as $video) {

    $video = new \Newspress\Asset\Video($video['id']);

    if ($video->getId() === null) {
        \Newspress\Cli::uiError('Could not get video with ID: ' . $video->getId());
        continue;
    }

    if ($video->s3->doesObjectExist($video->getBucket(), 'videos/converted/' . $video->getId() . '-' . $video->getName() . '.mp4')) {
        \Newspress\Cli::uiError('Video already converted with ID: ' . $video->getId());
        continue;
    }

    $video->convertVideo();

    \Newspress\Cli::uiMessage('Created job to convert video with ID: ' . $video->getId());

}
