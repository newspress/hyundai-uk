<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


/**
 * Updates the size and dimensions for all images
 */
$sql = \Newspress::db()->sql();
$now = new \DateTime();

$select = $sql->select();
$select->from('media')
       ->where
       ->equalTo('type', 'image')
       ->nest()
       ->isNull('size')
       ->or
       ->isNull('width')
       ->or
       ->isNull('height')
       ->unnest();
$select->order('id DESC');

$medias = \Newspress::db()->execute($select);

foreach ($medias as $media) {

    $media = new \Newspress\Asset\Image($media['id']);

    $stream = $media->getStreamUrl();
    $url = $media->getUrl();

    if (file_exists($stream)) {

        $size = getimagesize($url);

        $width = $size[0];
        $height = $size[1];
        $size = filesize($stream);

        try {

            $update = $sql->update();
            $update->table('media')
                   ->set(array(
                        'size' => $size,
                        'width' => $width,
                        'height' => $height,
                   ))
                   ->where(array('id' => $media->getId()));

            \Newspress::db()->execute($update);

            \Newspress\Cli::uiMessage('Updated size for media with ID: ' . $media->getId());

        } catch(\PDOException $e) {

            \Newspress\Cli::uiError('Could not update size for media with ID: ' . $media->getId());

        }

    } else {
    
        \Newspress\Cli::uiError('Could not find file for media with ID: ' . $media->getId());

    }

}
