<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';

// DELETE FROM `object_links` WHERE `from_object` = 7 AND `to_object` = 1

/**
 * Removes double extension names from S3 and the database
 */
$sql = \Newspress::db()->sql();
$now = new \DateTime();

$select = $sql->select();
$select->from('legacy_releases_models')
       ->join('releases', 'legacy_releases_models.rid = releases.legacy_id', array('release_id' => 'id'));

// var_dump($sql->getSqlStringForSqlObject($select));

$legacyReleaseModels = \Newspress::db()->execute($select);

$modelArray = array(
        // Old ID ::: New ID
        1 => 5,
        2 => 6,
        3 => 7,
        4 => 8,
        5 => 9,
        6 => 14,
        7 => 8,
        8 => 11,
        9 => 12,
        10 => 13,
        // 11 => x,
        12 => 15,
        13 => 16,
        14 => 17,
        15 => 18,
        16 => 19,
        17 => 20
    );

foreach ($legacyReleaseModels as $legacyReleaseModel) {

    if (!isset($modelArray[$legacyReleaseModel['mid']])) { continue; }

    $categoryId = $modelArray[$legacyReleaseModel['mid']];

    $insert = $sql->insert();
    $insert->into('object_links')
           ->columns(array('id', 'from_object', 'from_object_id', 'to_object', 'to_object_id', 'sort'))
           ->values(array(
                'id'             => null,
                'from_object'    => 7,
                'from_object_id' => $categoryId,
                'to_object'      => 1,
                'to_object_id'   => (int) $legacyReleaseModel['release_id'],
                'sort'           => 0,
            ));

    $statement = $sql->prepareStatementForSqlObject($insert);
    $statement->execute();

    $objectLinkId = \Newspress::db()->getInsertId();

    \Newspress\Cli::uiMessage('Added object link for release with ID: ' . $objectLinkId);

}
