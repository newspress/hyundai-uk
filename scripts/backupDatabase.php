<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


/**
 * Backup the database to S3
 *
 * @todo Remove older backups
 *       Hourly  (last 7 days)
 *       Weekly  (last month)
 *       Monthly (last year)
 */

if (!\Newspress::isProduction()) {
	\Newspress\Cli::uiError('You cannot run this script in a development or staging environment.');
	exit;
}

// Register the S3 stream wrapper
$archive = new \Newspress\Asset\Archive();

// Get database credentials
$name = \Newspress::config()->database->name;
$host = \Newspress::config()->database->host;
$username = \Newspress::config()->database->username;
$password = \Newspress::config()->database->password;

// Set the file name
$file = $name . '-' . date('Y-m-d-H-i-s') . '.gz';

$command = 'mysqldump --opt -h ' . $host . ' -u' . $username . ' -p' . $password . ' ' . $name . ' | gzip';

$fp = popen($command, 'r');
$bufsize = 8192;
$buff = '';

// File will be set to private as we're using streaming
$fpopen = fopen('s3://' . $archive->getBucket() . '/backups/database/' . $file, 'w');

while (!feof($fp)) {
    $buff = fread($fp, $bufsize);
    fwrite($fpopen, $buff);
}

fclose($fpopen);
pclose($fp);
