<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


$cron = new \Newspress\Cron();
$cron->initialise();

// @todo: Turn off messages by default (only show if in verbose mode)

$sqsQueue = new \Newspress\Queue\Sqs();
$localQueue = new \Newspress\Queue\Local();


// Run the local queue first incase we need to add items to the core queue

if ($localMessages = $localQueue->fetch(10)) {
    foreach ($localMessages as $localMessage) {
        if (strpos($localMessage->subject, \Newspress\Queue\Variables::QUEUE_IMAGE_CROP) !== false) {
            $object = $localMessage->body->object;
            $objectId = $localMessage->body->object_id;
            $crop = $localMessage->body->crop;

            if ($object == 2) {
                \Newspress\Cli::uiMessage('Starting cropping job for image');
                $image = new \Newspress\Asset\Image($objectId);
                $image->createCrop($crop);
                \Newspress\Cli::uiMessage('Completed cropping job for image with ID: ' . $objectId);
            }
        }
        $localQueue->delete($localMessage->id);
    }    
} else {
    \Newspress\Cli::uiMessage('No items in the local queue');
}


// Run the SQS queue

if ($sqsMessages = $sqsQueue->fetch(5)) {
    foreach ($sqsMessages as $sqsMessage) {
        $messageBody = $sqsMessage->Body;
        if (strpos($messageBody->Subject, 'Amazon Elastic Transcoder') !== false) {
            if ($messageBody->Message->state == 'COMPLETED') {
                \Newspress\Cli::uiMessage('Starting cropping job for video');

                $videoId = explode('/', $messageBody->Message->input->key);
                $videoId = end($videoId);
                $videoId = explode('-', $videoId);
                $videoId = (int) $videoId[0];

                if ($videoId > 0) {
                    $videoObject = new \Newspress\Asset\Video($videoId);
                    $videoObject->createThumbnail('thumbnail');
                    $videoObject->createThumbnail('small');

                    \Newspress\Cli::uiMessage('Completed cropping job for video with ID: ' . $videoId);
                }
            }
        }
        $sqsQueue->delete($sqsMessage->ReceiptHandle);
    }
} else {
    \Newspress\Cli::uiMessage('No items in the SQS queue');
}

$cron->terminate();
