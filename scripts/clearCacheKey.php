<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


if (!isset($argv[2])) {
    \Newspress\Cli::uiError('Please enter the cache key you would like to clear');
    exit;
}

$cache = \Newspress::cache();
$cache->setItem($argv[2], null);

\Newspress\Cli::uiMessage('Cache with key: ' . $argv[2] . ' has been cleared');
