<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


/**
 * Removes double extension names from S3 and the database
 */
$sql = \Newspress::db()->sql();
$now = new \DateTime();

$select = $sql->select();
$select->from('category_links');

$categoryLinks = \Newspress::db()->execute($select);

foreach ($categoryLinks as $categoryLink) {

    $insert = $sql->insert();
    
    $insert->into('object_links')
           ->columns(array('id', 'from_object', 'from_object_id', 'to_object', 'to_object_id', 'sort'))
           ->values(array(
                'id'             => null,
                'from_object'    => 7, // Category
                'from_object_id' => (int) $categoryLink['category_id'],
                'to_object'      => ($categoryLink['object'] == 1) ? 1 : 2,
                'to_object_id'   => (int) $categoryLink['object_id'],
                'sort'           => (int) $categoryLink['sort'],
            ));

    \Newspress::db()->execute($insert);

    $objectLinkId = \Newspress::db()->getInsertId();

    \Newspress\Cli::uiMessage('Added object link with ID: ' . $objectLinkId);

}
