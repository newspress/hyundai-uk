<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


/**
 * Generates crops that have not yet been created and uploaded to S3
 */
$crops = array();

if (isset($argv[2])) {
    $crops = array_splice($argv, 2);
}


$imageIdOffset = 0;


/**
 * Make a new instance of the Select class
 */
\Newspress\Cli::uiMessage('Creating an instance of the database class');
$sql = \Newspress::db()->sql();


/**
 * Select all images from the database
 */
\Newspress\Cli::uiMessage('Counting images');
$select = $sql->select();
$select->columns(array(
           'count' => new \Zend\Db\Sql\Expression('COUNT(*)')
       ))
       ->from('media');
$select->where
       ->greaterThan('id', $imageIdOffset)
       ->equalTo('type', 'image');

$results = \Newspress::db()->execute($select);

// Set defaults for batch select queries
$offset = 0;
$limit = 100;
$total = (int) $results->current()['count'];

/**
 * Start selecting images in batches
 */
\Newspress\Cli::uiMessage('Populating crop table for ' . $total . ' images');

while ($total > $offset) {

    $select = $sql->select();
    $select->from('media')
           ->columns(array('id'));
    $select->where
           ->greaterThan('id', $imageIdOffset)
           ->equalTo('type', 'image');
    $select->order('id DESC')
           ->limit($limit)
           ->offset($offset);

    // Echo the query on the command line if needed (for debugging)
    // \Newspress\Cli::uiMessage('Image import batch query: ' . $sql->getSqlStringForSqlObject($select));
    
    $results = \Newspress::db()->execute($select);

    // Go through results of current batch
    foreach ($results as $result) {

        $imageId = (int) $result['id'];
       
        foreach ($crops as $crop) {

            $image = new \Newspress\Asset\Image($imageId);

            // Check to see if the crop exists already
            if (!$image->objectExists($crop)) {
                $image->createCrop($crop);
                // \Newspress\Cli::uiError('Crop "' . $crop . '" with image ID: ' . $imageId . ' does not exist');
                // continue;
            }

            $cropStream = $image->getStreamUrl($crop);
            $cropUrl = $image->getUrl($crop);

            $dimensions = getimagesize($cropUrl);
            $size = filesize($cropStream);

            $dimensions = $dimensions ? $dimensions : null;
            $size = $size ? $size : null;

            $width = null;
            $height = null;

            if ($dimensions !== null) {
                $width = $dimensions[0];
                $height = $dimensions[1];
            }

            // Check crop doesn't already exist in the database
            $select = $sql->select();
            $select->from('crops');
            $select->where
                   ->equalTo('media', $imageId)
                   ->equalTo('name', $crop);

            $check = \Newspress::db()->execute($select);

            if ($check->count() > 0) {
                \Newspress\Cli::uiError('Crop ' . $crop . ' already exists in database with ID: ' . $check->current()['id']);
                continue;
            }

            $now = new \DateTime();

            $insert = $sql->insert();
            $insert->into('crops')
                   ->columns(array('id', 'media', 'name', 'size', 'width', 'height', 'x', 'y', 'modified', 'created'))
                   ->values(array(
                        'id'       => null,
                        'media'    => $imageId,
                        'name'     => $crop,
                        'size'     => $size,
                        'width'    => $width,
                        'height'   => $height,
                        'x'        => null,
                        'y'        => null,
                        'modified' => null,
                        'created'  => $now->format('Y-m-d H:i:s')
                    ));

            \Newspress::db()->execute($insert);

            $cropId = \Newspress::db()->getInsertId();

            \Newspress\Cli::uiMessage('Added crop "' . $crop . '" to table with ID: ' . $cropId);

        }

        unset($imageId);

    }

    // Add the batch limit to the offset
    $offset += $limit;

}
