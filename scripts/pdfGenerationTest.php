<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


\Newspress\Cli::uiMessage('Generating test PDF');

\Newspress\Debug::startTimer('pdf_test');

$date = new DateTime();
$date = $date->format('Y-m-d H:i:s');

$release = (object) array(
    'title'     => 'Test',
    'content'   => 'Test data',
    'published' => $date
);

ob_start();
include 'module/Releases/view/releases/partials/pdf.phtml';
$html = ob_get_contents();
ob_end_clean();

$dompdf = new \DOMPDF();
$dompdf->load_html($html);
$dompdf->render();
$string = $dompdf->output();

$time = \Newspress\Debug::stopTimer('pdf_test');

\Newspress\Cli::uiMessage('Time took: ' . $time);
