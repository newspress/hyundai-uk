<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


/**
 * Removes double extension names from S3 and the database
 */
$sql = \Newspress::db()->sql();
$now = new \DateTime();

$select = $sql->select();
$select->from('legacy_models_variant');

$variants = \Newspress::db()->execute($select);

foreach ($variants as $variant) {

    $update = $sql->update();
    $update->table('media')
           ->set(array(
                'size' => $size,
                'width' => $width,
                'height' => $height,
           ))
           ->where(array('id' => $media->getId()));

    \Newspress::db()->execute($update);

    \Newspress\Cli::uiMessage('Inserted variant with category ID: ' . $categoryId);

}
