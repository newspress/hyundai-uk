<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


/**
 * Make a new instance of the Elasticsearch service
 * @todo Add elasticsearch indexing update to this script
 */
// \Newspress\Cli::uiMessage('Creating an instance of the Elasticsearch service');
// $esClient = new \Newspress\Service\Elasticsearch();


/**
 * Updating media links in releases table
 */

\Newspress\Cli::uiMessage('Starting update for release media links');


$objectId = 1; // Release
$sql = \Newspress::db()->sql();

$select = $sql->select('releases');

$releases = \Newspress::db()->execute($select);

foreach ($releases as $release) {

    $select = $sql->select('media_links');
    $select->where
           ->equalTo('object', $objectId)
           ->equalTo('object_id', $release['id']);

    $results = \Newspress::db()->execute($select);

    $mediaIds = array();
    foreach ($results as $result) {
        $mediaIds[] = (int) $result['media_id'];
    }

    if (count($mediaIds) == 0) {
        \Newspress\Cli::uiError('No medias to update for release: ' . $release['id']);
        continue;
    }

    $update = $sql->update();
    $update->table('releases')
           ->set(array('media' => implode(',', $mediaIds)))
           ->where(array('id' => $release['id']));

    \Newspress::db()->execute($update);

    \Newspress\Cli::uiMessage('Media string updated for release: ' . $release['id']);

}
