<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';

/**
 * Removes double extension names from S3 and the database
 */

$sql = \Newspress::db()->sql();
$now = new \DateTime();


    \Newspress\Cli::uiMessage('Getting the max ID from videos');
    $select = $sql->select();
    $select->columns(array(
               'max' => new \Zend\Db\Sql\Expression('MAX(`legacy_id`)')
           ))
           ->from('media');
    $select->where
           ->equalTo('type', 'video');

    $results = \Newspress::db()->execute($select);

    $max = ($results->current()['max'] === null) ? 0 : (int) $results->current()['max'];

    \Newspress\Cli::uiMessage('Max legacy ID in media table: ' . $max);

    \Newspress\Cli::uiMessage('Counting videos');
    $select = $sql->select();
    $select->columns(array(
               'count' => new \Zend\Db\Sql\Expression('COUNT(*)')
           ))
           ->from('legacy_videos')
           ->where
           ->greaterThan('id', $max);

    $results = \Newspress::db()->execute($select);

    // Set defaults for batch select queries
    $offset = 0;
    $limit = 100;
    $total = (int) $results->current()['count'];

    /**
     * Start selecting videos in batches
     */
    \Newspress\Cli::uiMessage('Starting import of ' . $total . ' videos');
    while ($total > $offset) {

        // Select the releases (within the specified batch) joining the English translation
        $select = $sql->select();
        $select->from('legacy_videos')
               ->columns(array('id', 'title', 'description', 'keywords', 'image', 'timestamp_cre', 'timestamp_mod', 'timestamp_pub'))
               ->join('legacy_videos_vers', 'legacy_videos.id = legacy_videos_vers.vid', array('extension' => 'ext', 'file', 'size'))
               ->where
               ->greaterThan('legacy_videos.id', $max)
               ->equalTo('legacy_videos_vers.ver', 0);
        $select->order('id ASC')
               ->limit($limit)
               ->offset($offset);

        // Echo the query on the command line if needed (for debugging)
        // \Newspress\Cli::uiMessage('Video import batch query: ' . $sql->getSqlStringForSqlObject($select));
        // exit;

/*               
 var_dump($sql->getSqlStringForSqlObject($select));
 exit;*/


        $results = \Newspress::db()->execute($select);

        // Go through results of current batch
        foreach ($results as $result) {

            \Newspress\Cli::uiMessage('Downloading video from Rackspace with ID: ' . $result['id']);
            $rackspaceObject = $rackspaceContainer->getObject('data/videos/' . $result['id'] . '/' . $result['file'] . '.' . $result['extension']);
            $rackspaceObjectContent = $rackspaceObject->getContent();
            $rackspaceObjectContent->rewind();

            // Save file to local - should we delete this afterwards or at least use remote files?
            $localFilename = tempnam("/tmp", 'php-opencloud-');
            file_put_contents($localFilename, $rackspaceObjectContent->getStream());

            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($finfo, $localFilename);
            finfo_close($finfo);

            // Insert into new database
            $insert = $sql->insert();
            $insert->into('media')
                   ->columns(array('id', 'legacy_id', 'type', 'name', 'alt', 'title', 'description', 'keywords', 'acl', 'mime', 'size', 'author', 'status', 'published', 'modified', 'created'))
                   ->values(array(
                            'id'          => null,
                            'legacy_id'   => $result['id'],
                            'type'        => 'video',
                            'name'        => $result['file'],
                            'alt'         => $result['title'],
                            'title'       => $result['title'],
                            'description' => $result['description'],
                            'keywords'    => $result['keywords'],
                            'acl'         => \Aws\S3\Enum\CannedAcl::PUBLIC_READ,
                            'mime'        => $mime,
                            'size'        => (int) $result['size'],
                            'author'      => 1,
                            'status'      => 'active',
                            'published'   => $result['timestamp_pub'],
                            'modified'    => $result['timestamp_mod'],
                            'created'     => $result['timestamp_cre']
                        ));

            // \Newspress\Cli::uiMessage('Video insert query: ' . $sql->getSqlStringForSqlObject($insert));

            \Newspress::db()->execute($insert);

            $videoId = \Newspress::db()->getInsertId();

            \Newspress\Cli::uiMessage('Downloading video thumbnail from Rackspace with ID: ' . $videoId);

            $rackspaceObject = $rackspaceContainer->getObject('data/videos/' . $result['id'] . '/' . $result['image']);
            $rackspaceObjectContent = $rackspaceObject->getContent();
            $rackspaceObjectContent->rewind();

            $localThumbnailFilename = tempnam("/tmp", 'php-opencloud-thumbnail-');
            file_put_contents($localThumbnailFilename, $rackspaceObjectContent->getStream());

            \Newspress\Cli::uiMessage('Uploading video to S3 with ID: ' . $videoId);

            $video = new \Newspress\Asset\Video();
            $video->setId($videoId);
            $video->setName($result['file']);
            $video->setMime($mime);
            $video->createThumbnail('thumbnail', $localThumbnailFilename);
            $video->upload($localFilename);

            // Delete temporary files
            unlink($localFilename);
            unlink($localThumbnailFilename);

            \Newspress\Cli::uiMessage('Imported video with ID: ' . $videoId);

        }

        // Add the batch limit to the offset
        $offset += $limit;

    }
