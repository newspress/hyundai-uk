<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';

// DELETE FROM `object_links` WHERE `from_object` = 7 AND `to_object` = 1

/**
 * Removes double extension names from S3 and the database
 */
$sql = \Newspress::db()->sql();
$now = new \DateTime();

$select = $sql->select();
$select->from('legacy_images_models')
       ->join('categories', 'categories.legacy_id = legacy_images_models.mid', array('category_id' => 'id'))
       ->join('media', 'media.legacy_id = legacy_images_models.iid', array('media_id' => 'id'));
$select->where
       ->equalTo('categories.legacy_table', 'legacy_models')
       ->equalTo('media.type', 'image');

$legacyImageModels = \Newspress::db()->execute($select);

/* var_dump($sql->getSqlStringForSqlObject($select));
 exit;
*/
 /*
$modelArray = array(
        // Old ID ::: New ID
        1 => 5,
        2 => 6,
        3 => 7,
        4 => 8,
        5 => 9,
        6 => 14,
        7 => 8,
        8 => 11,
        9 => 12,
        10 => 13,
        // 11 => x,
        12 => 15,
        13 => 16,
        14 => 17,
        15 => 18,
        16 => 19,
        17 => 20
    );
*/

/*$modelArray = array(
    // Old ID :: New ID
    1 => 5, // = 'McLAREN 12C'
    4 => 6, // = 'McLAREN GT'
    5 => 7, // = 'F1'
    6 => 8, // = 'MP4/1'
    7 => 9, // = 'M6GT'
    DONE ===  8 => 10, // = 'McLAREN P1'
    9 => 11, // = '12C GT CAN-AM EDITION
    10 => 12, // ='12C GT SPRINT'
    11 => 13, // = 'McLAREN 650S COUPE'
    12 => 14, // = 'McLAREN 12C SPIDER'
    13 => 15, // = 'McLAREN 650S '
    13 => 79, // = 'McLAREN 650S SPIDER'
    14 => 16, // = 'McLAREN P1 GTR' 
    15 => 17, // = '650S GT3'
    16 => 18, // = '650S SPRINT'
    DONE ===  17 => 19, // = 'SPORTS SERIES'
    DONE === 19 => 20 // = 'SPORTS SERIES'
    
);
*/

$modelArray1 = array(
    // Old ID :: New ID

   // 19 => 20, // = 'SPORTS SERIES' => DONE
   // 14 => 16, // = 'McLAREN P1 GTR'
    //9  => 10, // = '12C GT CAN-AM EDITION' (Going) > NEW id = (10) 'McLAREN P1'  => DONE
    //5  => 5,  // = 'F1'  (Going) > NEW id = (5)'McLAREN 12C'
    //8  => 5,  // = 'McLAREN P1' > NEW id = (14)'McLAREN 12C SPIDER'
    //3  => 7, // F1
   // 6  => 8, // = 'MP4/1'
    //10 => 11, // = '12C GT SPRINT' (Going) > NEW id = (11)'12C GT CAN-AM EDITION
    


);


$modelArray = array(
    // Old ID :: New ID

    12 => 15, // = 'McLAREN 650S Spider '
    10 => 13, // = 'McLaren 650S Coupe'
    14 => 17, // = (17)'650S GT3'
    2  => 77, // = 12C GT3
    15 => 18, // = '650S SPRINT'
    5  => 9,  // = 'M6GT'
    9  => 12, // = 12c GT Sprint
    7  => 10, // = McLaren P1
    3  => 7,  // = McLaren F1
    4  => 8,  // = MP4/1
    8  => 11, // = 12C GT CAN-AM
    1  => 5,  // = McLaren 12C
    6  => 14, // = McLaren 12C Spider
    13 => 16, // = McLaren P1 GTR
    17 => 20, // = McLaren 675LT
    16 => 78, // = McLaren 570S


);

$unasigned__legacy_ids = array();

/*
$first_import = array(20,16,10, 5, 7, 8, 11,13);*/
$first_import = array();

foreach ($legacyImageModels as $legacyImageModel) {


    if (!isset($modelArray[$legacyImageModel['category_id']])) {
       // \Newspress\Cli::uiError('Could not match model with ID: ' . $legacyImageModel['category_id']);
       // continue;
    }


    if (! in_array($legacyImageModel["mid"],$unasigned__legacy_ids) && ! in_array($legacyImageModel["category_id"],$first_import) )
    {
        //var_dump(array_unique($legacyImageModel));
   
   
        $categoryId = $modelArray[$legacyImageModel['mid']];

        //$categoryId = $legacyImageModel['category_id'];
       

        \Newspress\Cli::uiMessage('LEGACY ID: ' . $legacyImageModel["mid"]);
        \Newspress\Cli::uiMessage('categoryId ID: ' . $categoryId);

        \Newspress\Cli::uiMessage('media_id ID: ' . $legacyImageModel['media_id']);


        
        $insert = $sql->insert();
        $insert->into('object_links')
               ->columns(array('id', 'from_object', 'from_object_id', 'to_object', 'to_object_id', 'sort'))
               ->values(array(
                    'id'             => null,
                    'from_object'    => 7,
                    'from_object_id' => $categoryId,
                    'to_object'      => 2,
                    'to_object_id'   => $legacyImageModel['media_id'],
                    'sort'           => 0,
                ));

        $statement = $sql->prepareStatementForSqlObject($insert);
        $statement->execute();

        $objectLinkId = \Newspress::db()->getInsertId();

        \Newspress\Cli::uiMessage('Added object link for image model with ID: ' . $objectLinkId);

        \Newspress\Cli::uiMessage('##################################' . $objectLinkId . '##################################');
      
     }

}
