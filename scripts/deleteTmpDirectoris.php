<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


/**
 * Refresh a local or staging database to the latest database backup in S3
 */

/*if (\Newspress::isProduction()) {
	\Newspress\Cli::uiError('This script cannot be run in production');
	exit;
}*/

/* 
 * php delete function that deals with directories recursively
 */
/*
function delete_files($target) {

    if(is_dir($target)){

        $files = glob( $target . '*download-*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned
        
        foreach( $files as $file )
        {
            delete_files( $file );      
        }
       
    if ((filectime($target) + 50) < time()) { 
        rmdir( $target );
        unlink( $target );
		\Newspress\Cli::uiMessage('DIR removed');
  	}

    } elseif(is_file($target)) {

    	if ((filectime($target) + 50) < time()) { 
        	unlink( $target ); 

			\Newspress\Cli::uiMessage('FILES removed');
  		}

    }
}
*/
// When the directory is not empty:
 /*function delete_files2($dir) {
   if (is_dir($dir)) {
   	    
     $objects = scandir($dir . '*download-*', GLOB_MARK);
     foreach ($objects as $object) {
       if ($object != "." && $object != "..") {


    	if ((filectime($dir) + 50) < time()) { 
         if (filetype($dir."/".$object) == "dir") {
         		delete_files2($dir."/".$object); 

				\Newspress\Cli::uiMessage('DIR 2 removed');
         	}else{
         		 unlink($dir."/".$object);

					\Newspress\Cli::uiMessage('FILES 2 removed');
         		}
       	}
       }
     }

    	if ((filectime($dir) + 50) < time()) { 
		     reset($objects);
		     rmdir($dir);

			\Newspress\Cli::uiMessage('FILES 2 removed');
		 }
   }
 }
*/


// cron job setup to delete downlaod dir every 3 minutes 
function deleteTmpDirectory() {

		    system('find /tmp/download-* -mmin +3 -exec rm -R  {} \;', $retval);

			\Newspress\Cli::uiMessage('FILES 2 removed ' . $retval);
		    return $retval == 0; // UNIX commands return zero on success
}

deleteTmpDirectory();
 

