<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';


/**
 * Refresh a local or staging database to the latest database backup in S3
 */

if (\Newspress::isProduction()) {
	\Newspress\Cli::uiError('This script cannot be run in production');
	exit;
}

// Register the S3 stream wrapper
$archive = new \Newspress\Asset\Archive();

// Get database credentials
$name = \Newspress::config()->database->name;
$host = \Newspress::config()->database->host;
$username = \Newspress::config()->database->username;
$password = \Newspress::config()->database->password;

// Get the S3 database backup path
$path = 's3://' . $archive->getBucket() . '/backups/database/';
$temp = realpath('data') . '/dumps/temporary.gz';

$file = '';
$latest = 0;
$d = dir($path);

\Newspress\Cli::uiMessage('Fetching last database backup');

// Get the last database backup name
while (false !== ($entry = $d->read())) {

	$filepath = "{$path}/{$entry}";

	if (is_file($filepath) && filectime($filepath) > $latest) {
		$latest = filectime($filepath);
		$file = $entry;
	}

}

\Newspress\Cli::uiMessage('Downloading database backup');

// Download the S3 file
$data = file_get_contents('s3://' . $archive->getBucket() . '/backups/database/' . $file);
file_put_contents($temp, $data);


\Newspress\Cli::uiMessage('Unzipping the database backup');

exec('gunzip ' . $temp);
$temp = substr($temp, 0, -3);

\Newspress\Cli::uiMessage('Dropping database and creating a fresh one');

\Newspress::db()->query('DROP DATABASE `' . $name . '`');
\Newspress::db()->query('CREATE DATABASE `' . $name . '`');

\Newspress\Cli::uiMessage('Importing new database data');

exec('mysql -h ' . $host . ' -u' . $username . ' -p' . $password . ' ' . $name . ' < ' . $temp);
unlink($temp);

\Newspress\Cli::uiMessage('Database refresh complete');
