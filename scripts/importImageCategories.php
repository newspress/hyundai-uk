<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

require dirname(__DIR__) . '/init_autoloader.php';

// DELETE FROM `object_links` WHERE `from_object` = 7 AND `to_object` = 1
$sql = \Newspress::db()->sql();
$now = new \DateTime();


$select = $sql->select();
$select->from('legacy_images_cats')
       ->join('categories', 'categories.legacy_id = legacy_images_cats.cid', array('category_id' => 'id'))
       ->join('media', 'media.legacy_id = legacy_images_cats.iid', array('media_id' => 'id'));
$select->where
       ->equalTo('categories.legacy_table', 'legacy_images_categories')
       ->equalTo('media.type', 'image');

$legacyImageCategories = \Newspress::db()->execute($select);

 var_dump($sql->getSqlStringForSqlObject($select));
 exit;

foreach ($legacyImageCategories as $legacyImageCategorie) {

     $categoryId = $legacyImageCategorie['category_id'];

    \Newspress\Cli::uiMessage('categoryId ID: ' . $categoryId);

    \Newspress\Cli::uiMessage('media_id ID: ' . $legacyImageCategorie['media_id']);


    $insert = $sql->insert();
    $insert->into('object_links')
           ->columns(array('id', 'from_object', 'from_object_id', 'to_object', 'to_object_id', 'sort'))
           ->values(array(
                'id'             => null,
                'from_object'    => 7,
                'from_object_id' => $categoryId,
                'to_object'      => 2,
                'to_object_id'   => $legacyImageCategorie['media_id'],
                'sort'           => 0,
            ));

    $statement = $sql->prepareStatementForSqlObject($insert);
    $statement->execute();

    $objectLinkId = \Newspress::db()->getInsertId();

    \Newspress\Cli::uiMessage('Added object link for image model with ID: ' . $objectLinkId);

}
