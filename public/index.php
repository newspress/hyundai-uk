<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

//This prevents Click-jacking
header('X-Frame-Options: SAMEORIGIN');

//This will give all cookies the HTTP-ONLY attribute
header( "Set-Cookie: name=value; httpOnly" );

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}

// Setup autoloading
require dirname(__DIR__) . '/init_autoloader.php';

// Run the application!
Zend\Mvc\Application::init(require dirname(__DIR__) . '/config/application.config.php')->run();