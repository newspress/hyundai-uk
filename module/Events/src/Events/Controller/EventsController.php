<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Events\Controller;

use Newspress\Mvc\Controller\BaseActionController;
use Newspress\Mvc\Controller\CoreReleasesController;
use Releases\Controller\ReleasesController;
use Zend\View\Model\ViewModel;
use Zend\Db\Sql\Expression;

class EventsController extends BaseActionController
{
    protected $downloads;
    
    /**
     * Index Action
     *
     * @return object ViewModel
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    public function indexAction()
    {
        return $this->redirect()->toRoute('error');
    }

    public function detailAction()
    {
        $stringSanitizer = $this->getServiceLocator()->get('StringSanitizer');

        $slug = $stringSanitizer->filter($this->params()->fromRoute('id'));

        $sql = \Newspress::db()->sql();
        $now = new \DateTime();

        $select = $sql->select();
        $select->from('events')
               ->where
               ->equalTo('slug', $slug)
               ->lessThan('published', $now->format('Y-m-d H:i:s'));
               

        $event = \Newspress::db()->execute($select);
        $event = $event->current() ? (object) $event->current() : null;

        $eventLinks = array();
        $eventLinksArray = array();

        if ($event) {
            $select = $sql->select();
            $select->from('object_links')
                ->where
                ->equalTo('from_object', 4)
                ->equalTo('from_object_id', $event->id);
            $select->order('sort ASC');

            $eventLinks = \Newspress::db()->execute($select);
        } else {
            // initialize event to avoid errors on the view rendering
            $event = (object) array(
                'title' => null,
                'hero'  => null,
                'id'    => null
            );
        }

        foreach ($eventLinks as $eventLink) {
            $eventLinksArray[$eventLink['to_object']][] = $eventLink['to_object_id'];
        }

        if (count($eventLinksArray) == 0) {
            // @todo: Work out where this should redirect if the client does not want a models page
            return $this->redirect()->toRoute('error');
        }

        $releases = false;
        if (isset($eventLinksArray[1])) {

            $select = $sql->select();
            $select->from('releases');
            $select->where
                   ->in('id', $eventLinksArray[1])
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('status', 'active');
            $select->limit(6)
                    ->order('published desc');

            $releases = \Newspress::db()->execute($select);

            if ($releases->count() > 0) {
                $releasesArray = array();
                foreach ($releases as $release) {

                    $releasesArray[] = CoreReleasesController::addAdditionalData($release);
                }
                $releases = $releasesArray;
            }

        }

        $images    = false;
        $videos    = false;
        $documents = false;

        if (isset($eventLinksArray[2])) {

            $select = $sql->select();
            $select->from('media');
            $select->where
                   ->in('id', $eventLinksArray[2])
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('type', 'image')
                   ->equalTo('status', 'active');
            $select->order(array(new Expression('FIELD (id, ' . implode(',', $eventLinksArray[2]) . ')')))
                   ->limit(6);

            $imageResult = \Newspress::db()->execute($select);

            if ($imageResult->count() > 0) {

                $imageArray = array();

                foreach ($imageResult as $image) {
                    $image = new \Newspress\Asset\Image($image['id']);
                    $imageArray[] = $image;
                }

                $images = $imageArray;

            }

            $select = $sql->select();
            $select->from('media');
            $select->where
                   ->in('id', $eventLinksArray[2])
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('type', 'video')
                   ->equalTo('status', 'active');
            $select->order(array(new Expression('FIELD (id, ' . implode(',', $eventLinksArray[2]) . ')')))
                   ->limit(6);

            $videosResult = \Newspress::db()->execute($select);

            if ($videosResult->count() > 0) {

                $videosArray = array();

                foreach ($videosResult as $video) {
                    $video = new \Newspress\Asset\Video($video['id']);
                    $videosArray[] = $video;
                }

                $videos = $videosArray;

            }

            $select = $sql->select();
            $select->from('media');
            $select->where
                   ->in('id', $eventLinksArray[2])
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('type', 'document')
                   ->equalTo('status', 'active');
            $select->order(array(new Expression('FIELD (id, ' . implode(',', $eventLinksArray[2]) . ')')))
                   ->limit(6);

            $documentsResult = \Newspress::db()->execute($select);

            if ($documentsResult->count() > 0) {

                $documentsArray = array();

                foreach ($documentsResult as $document) {
                    $document = new \Newspress\Asset\Document($document['id']);
                    $documentsArray[] = $document;
                }

                $documents = $documentsArray;

            }

        }

        $packs = false;

        if (isset($eventLinksArray[5])) {

            $select = $sql->select();
            $select->from('packs');
            $select->where
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->in('id', $eventLinksArray[5])
                   ->equalTo('status', 'active');
            $select->order(array(new Expression('FIELD (id, ' . implode(',', $eventLinksArray[5]) . ')')));

            $packs = \Newspress::db()->execute($select);
            
            $packsArray = array();

            foreach ($packs as $pack) {
                $packsArray[$pack['type']][] = $pack;
            }

            $packs = $packsArray;

        }

        return $this->viewModel->setVariables(array(
            'releases'  => $releases,
            'images'    => $images,
            'videos'    => $videos,
            'downloads' => $this->getDownloadsModel(),
            'documents' => $documents,
            'packs'     => $packs,
            'event'     => $event,
            'browser'   => $this->getBrowser()
        ));
    }

    public function getDownloadsModel()
    {
        if (!$this->downloads) {
            $sm = $this->getServiceLocator();
            $this->downloads = $sm->get('DownloadsModel');
        }
        return $this->downloads;
    }
}
