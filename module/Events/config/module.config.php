<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Events\Controller\Events' => 'Events\Controller\EventsController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'events' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/events',
                    'defaults' => array(
                        'controller' => 'Events\Controller\Events',
                        'action' => 'index'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'detail' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    => '/:id',
                            'constraints' => array(
                                'id' => '([0-9]+|[a-zA-Z][a-zA-Z0-9_\/-]*)',
                            ),
                            'defaults' => array(
                                'controller' => 'Events\Controller\Events',
                                'action'     => 'detail',
                            ),
                        ),
                    ),
                ),
            )
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    )
);
