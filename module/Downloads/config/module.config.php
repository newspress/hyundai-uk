<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Downloads\Controller\Downloads' => 'Downloads\Controller\DownloadsController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'downloads' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/downloads[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'    => '[0-9]+(,[0-9]+)*',
                    ),
                    'defaults' => array(
                        'controller' => 'Downloads\Controller\Downloads',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    )
);