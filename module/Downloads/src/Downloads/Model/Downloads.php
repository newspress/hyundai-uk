<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Downloads\Model;

use Newspress\Asset\Archive;

class Downloads
{
    protected $session;

    /**
     * Start a new downloads session
     *
     * @return void
     * @todo   At the moment these get saved to session. Enable core
     *         functionality to enable this to easily switch to a database so
     *         users have to log in to access their downloads and can access
     *         them via different machines when logged in to the media website.
     * @todo   Move this model to a core model
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    public function __construct()
    {
        if ($this->session === null) {
            $this->session = \Newspress::session('downloads');
        }
    }

    /**
     * Add an item to the session, ready for downloading
     * 
     * @param  int    $id   The ID of the item from the database
     * @param  string $type The type (image, video, document)
     * @param  string $crop The name of the crop
     * @param  string $user_id The name of the crop
     * @return bool
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     * @author Thomas Forster <thomas.forster@newspress.co.uk>
     */

    public function addItem($id, $type, $title, $crop = null, $user_id = null)
    {

        if (!$downloads = $this->session->offsetGet('downloads')) {
            $downloads = array();
        }

        if (!$this->getItem($id, $type, $crop)) {

            $now = new \DateTime();

            $downloads[] = array(
                    'id'    => $id,
                    'type'  => $type,
                    'crop'  => $crop,
                    'title' => $title,
                    'added' => $now->format('Y-m-d H:i:s')
                );

            if(!empty($user_id)) {

                $sql = \Newspress::db()->sql();
                $insert = $sql->insert();
                $insert->into('user_downloads')
                    ->values(array(
                        'id' => null,
                        'user_id' => $user_id,
                        'media_id' => $id,
                        'title' => $title,
                        'media_type' => $type,
                        'crop_name' => ( empty($crop) || $crop=='false' )?NULL:$crop,
                        'added' => $now->format('Y-m-d H:i:s')
                    ));
                \Newspress::db()->execute($insert);


//              Count records and delete oldest if over 20
                $select = $sql->select();
                $select->from('user_downloads');
                $select->where
                    ->equalTo('user_id', $user_id);
                $results = \Newspress::db()->execute($select);

                if ($results->count() > 20) {

                    $sql = \Newspress::db()->sql();

                    $select = $sql->select();
                    $select->from('user_downloads')
                        ->where
                        ->equalTo('user_id', $user_id);

                    $select->order('id DESC');
                    $select->limit(100);
                    $select->offset(20);


                    $results = \Newspress::db()->execute($select, 'object', false);

                    $sql = \Newspress::db()->sql();
                    $delete = $sql->delete('user_downloads');

                    $ids = [];

                    foreach ($results as $key => $value) {
                        $ids[] = $value->id;
                    }

                    $delete->where->in('id', $ids);

                    \Newspress::db()->execute($delete);
                }

            }
        } else {
            return false;
        }

        $this->session->offsetSet('downloads', $downloads);

        return true;
    }

    /**
     * Remove an item from the session
     * 
     * @return bool
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    public function removeItem($id, $type, $crop = null)
    {
        if (!$downloads = $this->session->offsetGet('downloads')) {
            return false;
        }

        foreach ($downloads as $key => $item) {
            if ($item['id'] == $id && $item['type'] == $type && $item['crop'] == $crop) {
                unset($downloads[$key]);
            }
        }

        $this->session->offsetSet('downloads', $downloads);

        return true;
    }

    /**
     * Remove items from the session
     *
     * @return bool
     * @author Richard Skene <richard.skene@newspress.co.uk>
     */
    public function removeItems($items)
    {
        if (!$items) { return false; }
        if (!$downloads = $this->session->offsetGet('downloads')) {
            return false;
        }

        foreach ($items as $item) {
            $this->removeItem($item['id'], $item['type'], $item['crop']);
        }

        return true;
    }

    /**
     * Remove items from the database
     *
     * @return bool
     * @author Thomas Forster <thomas.forster@newspress.co.uk>
     */
    public function removeRecentItems($items, $user_id)
    {
        if (!$items) { return false; }

        foreach ($items as $item) {

            $sql = \Newspress::db()->sql();
            $delete = $sql->delete();
            $delete->from('user_downloads')
                ->where
                ->equalTo('user_id', $user_id)
                ->and
                ->equalTo('media_id', $item['id']);


            \Newspress::db()->execute($delete);


        }

        return true;
    }

    /**
     * Get all the items from the session
     * 
     * @return array
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    public function getItems($items = null, $user_id = false)
    {


        if( !$user_id ){

            if (!$downloads = $this->session->offsetGet('downloads')) {
                return false;
            }

        } else {

            $sql = \Newspress::db()->sql();
            $select = $sql->select();
            $select->from('user_downloads')
            ->where->equalTo('user_id', $user_id);
            $results = \Newspress::db()->execute($select);
            $downloads = [];

            foreach ($results as $result) {

                $downloads[] = [
                    'id' => $result['media_id'],
                    'type' => $result['media_type'],
                    'crop' => $result['crop_name'],
                    'title' => $result['title'],
                    'added' => $result['added']
                ];
            }

        }

        if ($items === null) {
            return $downloads;
        }

        $returnItems = [];

        foreach ($items as $item) {
            foreach ($downloads as $downloadItem) {
                if ($downloadItem['id'] == $item['id'] && $downloadItem['type'] == $item['type'] && $downloadItem['crop'] == $item['crop']) {
                    $returnItems[] = $downloadItem;
                }
            }
        }



        return $returnItems;

    }

    public function getItemCount()
    {
        if (!$items = $this->getItems()) {
            return 0;
        }

        return count($items);
    }


    /**
     * Get all the items split into array of types
     *
     * move to its
     *
     * @return array
     * @author Richard Skene <richard.skene@newspress.co.uk>
     */
    public function getItemsFromIdArrayOfTypes($user_id)
    {

        // query the user_recent_downloads based on the current user id. make sure to check if user id exists before you query other wise there error ;
            $sql = \Newspress::db()->sql();
            $select = $sql->select();
            $select->from('user_downloads')
                ->where->equalTo('user_id', $user_id);
                $select->order('id DESC');
                $select->limit(20);

            $results = \Newspress::db()->execute($select);

        if ( !$results) {
            return false; // check db results inace null or emty
        }

        $formattedData = [];

        foreach ($results as $result) {

            $formattedData[] = [
                'id' => $result['media_id'],
                'type' => $result['media_type'],
                'crop' => $result['crop_name'],
                'title' => $result['title'],
                'added' => $result['added']
            ];
        }

        $items = [];

        foreach ($formattedData as $result) {
            switch ($result['type']) {
                case 'image':
                    $items['images'][] = $result;
                    break;
                case 'video':
                    $items['videos'][] = $result;
                    break;
                case 'document':
                    $items['documents'][] = $result;
                    break;
            }
        }

        return $items;
    }

    /**
     * Get all the items split into array of types
     * 
     * @return array
     * @author Richard Skene <richard.skene@newspress.co.uk>
     */
    public function getItemsArrayOfTypes()
    {
        if (!$downloads = $this->session->offsetGet('downloads')) {
            return false;
        }

        $items = [];

        foreach ($downloads as $download) {
            switch ($download['type']) {
                case 'image':
                    $items['images'][] = $download;
                    break;
                case 'video':
                    $items['videos'][] = $download;
                    break;
                case 'document':
                    $items['documents'][] = $download;
                    break;
            }
        }

//        \Newspress\Debug::dump($items);

        return $items;
    }

    /**
     * Gets an array of the item based on object if and object type
     * 
     * @param  int    $id   The ID of the item
     * @param  string $type 
     * @return array|false
     */
    public function getItem($id, $type, $crop = null)
    {
        $array = array();
        $items = $this->getItems();

        if ($items) {
            foreach ($items as $item) {
                if ($item['id'] == $id && $item['type'] == $type && $item['crop'] == $crop) {
                    $array[] = $item;
                }
            }
            return $array;
        }
        return false;
    }

    /**
     * Takes an array for media data and formats it for use with the view.
     *
     * @param  array
     * @return array
     * @author  Thomas Forster <thomas.forster@newspress.co.uk>
     */
    public function formatData($items){

        $images = array();
        $videos = array();
        $documents = array();

        if (isset($items['images'])) {

            foreach ($items['images'] as $image) {
                $image['object'] = new \Newspress\Asset\Image($image['id']);
                $images[] = $image;
            }

        }

        $items['media'] = $images;

        if (isset($items['videos'])) {

            foreach ($items['videos'] as $video) {
                $video['object'] = new \Newspress\Asset\Video($video['id']);
                $videos[] = $video;
            }

            $items['media'] = array_merge($items['media'], $videos);
        }

        if (isset($items['documents'])) {

            foreach ($items['documents'] as $document) {
                $document['object'] = new \Newspress\Asset\Document($document['id']);
                $documents[] = $document;
            }

            $items['documents'] = $documents;
        }

        return $items;
    }

    /**
     * Gets an array of the item from their index in the session array
     * 
     * @param  string    $ids   Comma separated string of ids
     * @return array
     * @author  Richard Skene <richard.skene@newspress.co.uk>
     */
    public function getItemsFromSessionIds($ids)
    {
        $items = array();
        $ids = explode(',', $ids);

        $downloads = $this->session->offsetGet('downloads');

        foreach ($ids as $id) {
            $item = $downloads[$id];
            $items[] = $item;
        }

        return $items;
    }

    public function getItemsForDownload($items = array())
    {
        $files = array();

        if (!$items) {
            $items = $this->getItems();
        }

        if ($items && count($items) > 0) {
            foreach ($items as $item) {

                switch(strtolower($item['type'])){
                    case 'image':
                        $media = new \Newspress\Asset\Image($item['id']);
                        break;
                    case 'video':
                        $media = new \Newspress\Asset\Video($item['id']);
                        break;
                    case 'document':
                        $media = new \Newspress\Asset\Document($item['id']);
                        break;
                }

                if(isset($media)){
                    $file['object'] = $media;
                    $file['crop'] = \Newspress\Filter::getBool($item['crop']);
                    $file['url'] = $media->getStreamUrl($file['crop']);
                    $file['url'] = escapeshellcmd($file['url']);
                    $files[] = $file;
                }

            }
        }

//        \Newspress\Debug::dump($files, true);

        if (count($files) == 0) {
            // No files to download
            return false;
        }

        $temporaryDirectory = tempnam(sys_get_temp_dir(), 'download-');

        if (file_exists($temporaryDirectory)) {
            unlink($temporaryDirectory);
        }

        mkdir($temporaryDirectory);

        if (!is_dir($temporaryDirectory)) {
            \Newspress\Debug::logError('Could not make temporary directory');
        }

        $temporaryFiles = array();

        foreach ($files as $file) {
            $cropDirectory = (!$file['crop'] ? '' : $file['crop'] . '-');
            $fileName = $temporaryDirectory . '/' . $cropDirectory . $file['object']->getId() . str_replace(' ', '-', $file['object']->getName()) . '.' . $file['object']->getExtension();
            file_put_contents($fileName, file_get_contents($file['url']));
            $temporaryFiles[] = $fileName;
        }

        return $temporaryFiles;
    }

    /**
     * [downloadItems description]
     * @param  boolean $items [description]
     * @return void
     */
    public function downloadItems($items = array())
    {

        $temporaryFiles = $this->getItemsForDownload($items);

        if(is_array($temporaryFiles)){
            header('Content-Type: application/z-zip');
            header('Content-disposition: attachment; filename="download.zip"');

            $fp = popen('zip -rj - ' . implode(' ', $temporaryFiles), 'r');
            $bufsize = 8192;
            $buff = '';

            if($fp !== false) {
                while (!feof($fp)) {
                    $buff = fread($fp, $bufsize);
                    echo $buff;
                }

                pclose($fp);

                $this->removeTemporaryFiles($temporaryFiles);

            }
        }

        exit;

    }

    public function removeTemporaryFiles($temporaryFiles)
    {
        if(is_array($temporaryFiles)) {
            foreach ($temporaryFiles as $temporaryFile) {
                if (file_exists($temporaryFile)) {
                    unlink($temporaryFile);
                }
            }
        }
    }

    public function emailItems($items = false, $name, $email)
    {

        $archive = new Archive();
        $temporaryFiles = $this->getItemsForDownload($items);

        $fp = popen('zip -rj - ' . implode(' ', $temporaryFiles), 'r');
        $bufsize = 8192;
        $buff = '';

        $key = md5(implode(' ', $temporaryFiles));

        // @todo: Check file doesn't already exist

        $fpopen = fopen('s3://' . $archive->getBucket() . '/archive/download-' . $key . '.zip', 'w');

        if($fp !== false) {
            while (!feof($fp)) {
                $buff = fread($fp, $bufsize);
                fwrite($fpopen, $buff);
            }
        }

        fclose($fpopen);
        pclose($fp);

        $this->removeTemporaryFiles($temporaryFiles);
        $url = $archive->getTemporaryLocation('archive/download-' . $key . '.zip', '+24hours');

        $mailer = new DownloadsMailer();



        $mailer->sendDownloadEmail($email, $url, $name, $items);
    }
}