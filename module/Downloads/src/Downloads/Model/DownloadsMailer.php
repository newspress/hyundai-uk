<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Downloads\Model;

use Newspress\Mailer;

class DownloadsMailer extends Mailer
{
	public function downloadEmail($email, $url, $name, $items)
	{
		$this->mail()->addTo($email);

		$this->addData('title', '<span class="file-count">' . count($items) . '</span> Files available for download');
		$this->addData('name', $name);
		$this->addData('items', $items);
		$this->addData('url', \Newspress\Url::sanitiseS3Url($url));

		$this->setSubject('Media download available');
	}
}
