<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Models\Controller\Models' => 'Models\Controller\ModelsController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'models' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/models',
                    'defaults' => array(
                        'controller' => 'Models\Controller\Models',
                        'action' => 'index'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'detail' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    => '/:slug',
                            'constraints' => array(
                                'slug' => '([0-9]+|[a-zA-Z][a-zA-Z0-9_\/-]*)',
                            ),
                            'defaults' => array(
                                'controller' => 'Models\Controller\Models',
                                'action'     => 'detail',
                            ),
                        ),
                    ),
                ),
            )
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    )
);