<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Models\Controller;

use Newspress\Mvc\Controller\BaseActionController;
use Newspress\Mvc\Controller\CoreReleasesController;
use Zend\Db\Sql\Expression;
use Index\Controller\IndexController;
use Newspress\Social\Facebook;
use Newspress\Social\Twitter;
use Newspress\Social\Instagram;
use Application\Model\Object;

class ModelsController extends BaseActionController
{
    protected $downloads;
    protected $categories;

    public function indexAction()
    {
        return $this->redirect()->toRoute('error');
    }

    public function getCategoryChildren($category)
    {
        $sql = \Newspress::db()->sql();

        $select = $sql->select();
        $select->from('categories')
               ->where
               ->equalTo('parent', $category->id)
               ->notEqualTo('id', $category->id);

        $results = \Newspress::db()->execute($select, 'object');

        if (count($results) > 0) {
            foreach ($results as $result) {
                $this->categories[] = $result->id;
                $this->getCategoryChildren($result);
            }
        }
    }
    /**
     * @todo  sort out category/model confusion
     */
    public function detailAction()
    {

        $stringSanitizer = $this->getServiceLocator()->get('StringSanitizer');

        $categories = array();
        $sql = \Newspress::db()->sql();
        $now = new \DateTime();

        $select = $sql->select();
        $select->from('categories');

        $slug = $stringSanitizer->filter($this->params()->fromRoute('slug'));

        if (is_numeric($slug)) {
            $select->where('`id` = ' . (int) $slug . ' AND FIND_IN_SET(1, `parent_path`)');
        } else {
            $select->where('`slug` = \'' . $slug . '\' AND FIND_IN_SET(1, `parent_path`)');
        }
        $select->where->lessThan('published', $now->format('Y-m-d H:i:s'));
        $category = \Newspress::db()->execute($select);

        if (!$category->count() > 0) {
            return $this->redirect()->toRoute('error');
        }

        $category = (object) $category->current();

        $this->page->setCanonicalLink('/models/' . $category->slug);

        $this->categories[] = $category->id;
        if ($category->id == 13) {
            $this->categories[] = 15;
        }
        $this->getCategoryChildren($category);

        $categoryLinksArray = array();

        $select = $sql->select();
        $select->from('object_links')
               ->where
               ->equalTo('from_object', 6)
               ->in('from_object_id', $this->categories);
        $select->order('sort ASC');

        $categoryLinks = \Newspress::db()->execute($select);

        foreach ($categoryLinks as $categoryLink) {
            $categoryLinksArray[$categoryLink['to_object']][] = $categoryLink['to_object_id'];
        }

        if (count($categoryLinksArray) == 0) {
            // @todo: Work out where this should redirect if the client does not want a models page
            return $this->redirect()->toRoute('error');
        }

        $releases = false;
        if (isset($categoryLinksArray[1])) {

            $select = $sql->select();
            $select->from('releases');
            $select->where
                   ->in('id', $categoryLinksArray[1])
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('status', 'active');
            $select->order('published DESC')
                   ->limit(6);

            $releases = \Newspress::db()->execute($select);

            if ($releases->count() > 0) {
                $releasesArray = array();
                foreach ($releases as $release) {
                    $releasesArray[] = CoreReleasesController::addAdditionalData($release);
                }
                $releases = $releasesArray;
            }
        }

        $images            = false;
        $videos            = false;
        $youTubeVideos     = false;
        $documents         = false;
        $mediaCount        = 0;
        $videoCount        = 0;
        $imageCount        = 0;
        $youTubeVideoCount = 0;


        if (isset($categoryLinksArray[2])) {

            // $select = $sql->select();
            // $select->from('media');
            // $select->where
            //        ->in('id', $categoryLinksArray[2])
            //        ->lessThan('published', $now->format('Y-m-d H:i:s'))
            //        ->equalTo('type', 'video')
            //        ->equalTo('status', 'active')
            //        ->isNull('mime');
            // $select->order('published DESC')
            //        ->limit(6);

            // $youTubeVideosResult = \Newspress::db()->execute($select);

            $select = $sql->select();
            $select->from('media')
                   ->columns(array('mediaCount' => new \Zend\Db\Sql\Expression('COUNT(*)')));
            $select->where
                   ->in('id', $categoryLinksArray[2])
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('status', 'active')
                   ->isNull('mime');
            $select->order('published DESC');
            $youTubeVideoCount = \Newspress::db()->execute($select, 'array');
            $youTubeVideoCount = $youTubeVideoCount[0];

            $select = $sql->select();
            $select->from('media');
            $select->where
                   ->in('id', $categoryLinksArray[2])
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('type', 'video')
                   ->equalTo('status', 'active')
                   ->isNotNull('mime');
            $select->order('published DESC')
                   ->limit(3);

            $videosResult = \Newspress::db()->execute($select);

            $totalCount = 3 - $videosResult->count();
            $imageLimit = 3 + $totalCount;


            $select = $sql->select();
            $select->from('media');
            $select->where
                   ->in('id', $categoryLinksArray[2])
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('type', 'image')
                   ->equalTo('lead_image', 0)
                   ->equalTo('status', 'active');
            $select->order('published DESC')
                   ->limit($imageLimit);

            $imageResult = \Newspress::db()->execute($select);

            if ($imageResult->count() > 0) {
                $imageArray = array();
                foreach ($imageResult as $image) {
                    $image = new \Newspress\Asset\Image($image['id']);
                    $imageArray[] = $image;
                }
                $images = $imageArray;
            }

            if ($videosResult->count() > 0) {
                $videosArray = array();
                $youTubeVideosArray = array();
                foreach ($videosResult as $video) {
                    $video = new \Newspress\Asset\Video($video['id']);
                    $videosArray[] = $video;
                }
                $videos = $videosArray;
            }

            // if ($youTubeVideosResult->count() > 0) {
            //     $videosArray = array();
            //     $youTubeVideosArray = array();
            //     foreach ($youTubeVideosResult as $video) {
            //         $youTubeVideo = new \Newspress\Asset\Video($video['id']);
            //         if ($youTubeVideo->isThirdParty()) {
            //             $youTubeVideosArray[] = $youTubeVideo;
            //         }
            //     }
            //     $youTubeVideos = $youTubeVideosArray;
            // }

            $select = $sql->select();
            $select->from('media');
            $select->where
                   ->in('id', $categoryLinksArray[2])
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('type', 'document')
                   ->equalTo('status', 'active');
            $select->order('published DESC')
                   ->limit(6);

            $documentsResult = \Newspress::db()->execute($select);

            if ($documentsResult->count() > 0) {
                $documentsArray = array();
                foreach ($documentsResult as $document) {
                    $document = new \Newspress\Asset\Document($document['id']);
                    $documentsArray[] = $document;
                }
                $documents = $documentsArray;
            }
        }    

        $imagesCount = (isset($images) && $images) ? count($images) : 0;
        $videosCount = (isset($videos) && $videos) ? count($videos) : 0;
        $mediaCount = $imagesCount + $videosCount;

        if ($mediaCount >= 6) {

            $select = $sql->select();
            $select->from('object_links')
                   ->where
                   ->equalTo('from_object', 7)
                   ->in('from_object_id', $this->categories);
            $select->order('sort ASC');

            $categoryLinks = \Newspress::db()->execute($select);


            $categoryLinksArray = array();
            
            foreach ($categoryLinks as $categoryLink) {
                $categoryLinksArray[$categoryLink['to_object']][] = $categoryLink['to_object_id'];
            }

            /*
            // It only took images of one category
            $mediaObject = new Object(null, 7, (array) $category->id, 2);
            $mediaObjectIds = $mediaObject->getObjectLinks(true);
            */

            $select = $sql->select();
            $select->from('media');
            $select->where
                ->in('id', $categoryLinksArray[2])
                ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('type', 'image')
                   ->equalTo('status', 'active');
            $imageResults = \Newspress::db()->execute($select);
            $imageCount = $imageResults->count();

            $select = $sql->select();
            $select->from('media');
            $select->where
                    ->in('id', $categoryLinksArray[2])
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('type', 'video')
                   ->equalTo('status', 'active');
            $videoResults = \Newspress::db()->execute($select);
            $videoCount = $videoResults->count();
        }



        $packs = false;

        if (isset($categoryLinksArray[5])) {

            $select = $sql->select();
            $select->from('packs');
            $select->where
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->in('id', $categoryLinksArray[5]);
            $select->order('sort');

            $packs = \Newspress::db()->execute($select);
            
            $packsArray = array();

            foreach ($packs as $pack) {
                $packsArray[$pack['type']][] = $pack;
            }

            $packs = $packsArray;

        }

        $facebook  = false;
        $twitter   = false;
        $instagram = false;
        $pitwall   = false;
        
        $mcLarenGtId = 6;
        $mcLarenGtPitwallLink = 'http://media.mclarenautomotive.com/pitwall';

        if ($category->id == $mcLarenGtId) {
            $facebook  = new Facebook();
            $twitter   = new Twitter();
            $instagram = new Instagram();

            $facebook->setUsername('mclarenautomotive');
            $twitter->setUsername('McLarenAuto');
            $instagram->setUsername('mclarenauto');

            $select = $sql->select();
            $select->from('pitwall');

            // @todo: Remove this once the images for the Pitwall have been fixed
            $select->where->equalTo('id', 819);

            $select->limit(1);

            $pitwall = \Newspress::db()->execute($select, 'object');
            $pitwall = current($pitwall);

        }




        $select = $sql->select();
        $select->from('object_links')
               ->where
               ->equalTo('from_object', 7)
               ->in('from_object_id', $this->categories);
        $select->order('sort ASC');

        $categoryLinks = \Newspress::db()->execute($select);
        foreach ($categoryLinks as $categoryLink) {
            $categoryLinksArray[$categoryLink['to_object']][] = $categoryLink['to_object_id'];
        }

        $select = $sql->select();
        $select->from('media');
        $select->where
               ->in('id', $categoryLinksArray[2])
               ->lessThan('published', $now->format('Y-m-d H:i:s'))
               ->equalTo('type', 'video')
               ->equalTo('status', 'active')
               ->isNull('mime');
        $select->order('published DESC')
               ->limit(6);

        $youTubeVideosResult = \Newspress::db()->execute($select);

        if ($youTubeVideosResult->count() > 0) {
            $videosArray = array();
            $youTubeVideosArray = array();
            foreach ($youTubeVideosResult as $video) {
                $youTubeVideo = new \Newspress\Asset\Video($video['id']);
                if ($youTubeVideo->isThirdParty()) {
                    $youTubeVideosArray[] = $youTubeVideo;
                }
            }
            $youTubeVideos = $youTubeVideosArray;
        }

        return $this->viewModel->setVariables(array(
            'releases'             => $releases,
            'images'               => $images,
            'imageCount'           => $imageCount,
            'videos'               => $videos,
            'videoCount'           => $videoCount,
            'youTubeVideos'        => $youTubeVideos,
            'youTubeVideoCount'    => $youTubeVideoCount,
            'downloads'            => $this->getDownloadsModel(),
            'documents'            => $documents,
            'mediaCount'           => $mediaCount,
            'packs'                => $packs,
            'category'             => $category,
            'mcLarenGtId'          => $mcLarenGtId,
            'mcLarenGtPitwallLink' => $mcLarenGtPitwallLink,
            'facebook'             => $facebook,
            'twitter'              => $twitter,
            'instagram'            => $instagram,
            'pitwall'              => $pitwall,
            'browser'              => $this->getBrowser()
        ));
    }

    public function getDownloadsModel()
    {
        if (!$this->downloads) {
            $sm = $this->getServiceLocator();
            $this->downloads = $sm->get('DownloadsModel');
        }
        return $this->downloads;
    }
}
