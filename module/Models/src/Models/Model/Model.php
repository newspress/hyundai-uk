<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Models\Model;

class Model
{
    public $id;

    public function __construct($id = null)
    {
        if ($id !== null) {
            $this->id = (int) $id;
        }
    }

    public function getNavigationHtml($titleLink = false, $classes = array())
    {
        $sql = \Newspress::db()->sql();
        $select = $sql->select('categories');
        $select->where
               ->equalTo('id', $this->id);

        $model = \Newspress::db()->execute($select, 'array');

        if (count($model) == 0) {
            return '';
        }

        $model = $model[0];

        $image = false;
        if (isset($model['dropdown']) && $model['dropdown'] !== null) {
            $image = new \Newspress\Asset\Image((int) $model['dropdown']);
            $image = $image->getUrl('dropdown');
        }

        $html = '';

        if (!$titleLink) {
            $html .= '<li>';
        }

        $html .= '<a href="/models/' . ($model['slug'] !== null ?  $model['slug'] : $this->id) . '"' . ($image ? ' data-image="' . $image . '"' : '') . (count($classes) > 0 ? 'class="' . implode(' ', $classes) . '"' : '') . '>' . \Index\Model\Index::strtoupper($model['name']) . '</a>';
        
        if (!$titleLink) {
            $html .= '</li>';
        }
        return $html;
        
    }
}
