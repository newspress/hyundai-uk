<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Search\Controller\Search' => 'Search\Controller\SearchController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'search' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'       => '[/:locale]/search',
                    'constraints' => array(
                        'locale' => '[a-z]{2}-[a-z]{2}|[a-z]{2}',
                    ),
                    'defaults' => array(
                        'controller' => 'Search\Controller\Search',
                        'action'     => 'index',
                        'locale'     => 'en-gb',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    )
);
