<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Search\Model;

use FiltersAdmin\Model\Filter;

class Search
{
	public function getSearchFilters($queryFilter, $fieldsToRemove=array())
    {
        //var_dump($queryFilter);die;
        $filter  = new Filter();
        $parents = $filter->getParentFilters();

        $filterHtml = null;
        $filterViewHtml = null;
        $filterSortByHtml = null;
        $filterCheckboxHtml = null;
        $now = (new \DateTime())->format('Y-m-d H:i:s');
        foreach ($parents as $parent) {

            // This array contains all the fields that should not
            // be displayed in the search area.
            // This is done to allow use this same method in
            // more than one page.
            if (!empty($fieldsToRemove)) {
                if (in_array($parent['slug'], $fieldsToRemove) !== false) {
                    continue;
                }
            }


            if ($parent['status'] == 'inactive' || $parent['published'] > $now) {
                continue;
            }


            $children = $filter->getChildrenFilters($parent['id']);


            // Opening tags - Start
            if ($parent['name'] == 'View') {

                $filterViewHtml .= '
                            <div class="grid__item one-third l-one-third m-one-third s-full align--center mobile--hidden tablet--hidden">
                                <div class="sort__sizes">
                                    <span class="sort__label sort__label--vertical padding--right-15">' . $parent['name'] . '</span>';

            } elseif ($parent['name'] == 'Sort By') {

                $value = (isset($queryFilter[$parent['slug']]) ? $queryFilter[$parent['slug']] : '');
                $select_value = 'Select';
                if ($value) {
                    $value = $this->sanitizeFilterText($value);
                } else {
                    $value = 'published-desc';
                }

                foreach ($children as $child) {
                    if ($child['slug'] == $value) {
                        $select_value = $child['name'];
                    }
                }

                $filterSortByHtml .= '
                            <div class="grid__item one-third l-one-third m-one-third s-full align--right align--center-mobile tablet--float-right">
                                <label class="sort__label">' . $parent['name'] . '</label>
                                <div class="select display--inline-block">
                                    <input type="hidden" class="select__input" name="' . $parent['slug'] . '" value="' . $value . '">
                                    <p class="select__text">' . $select_value . '</p>
                                    <ul class="select__list">';

            } elseif ($parent['name'] != 'Types') {

                $filterHtml .= '
                            <div class="grid__item one-third l-one-third m-one-half s-full">';

            }

            $value = (isset($queryFilter['queryParams'], $queryFilter['queryParams'][$parent['slug']]) ? $queryFilter['queryParams'][$parent['slug']] : '');
            if ($parent['name'] == 'Models') {

                $select_value = 'Select';
                if ($value) {
                    $select_value = $this->sanitizeFilterText($value, 'display');
                    $value = $this->sanitizeFilterText($value);
                }
                $filterHtml .= '
                                <label class="filters__label">' . $parent['name'] . '</label>
                                <div class="select">
                                    <input type="hidden" class="select__input" name="' . $parent['slug'] . '" value="' . $value . '">
                                    <p class="select__text">' . $select_value . '</p>
                                    <ul class="select__list">';

            } elseif ($parent['name'] == 'Language') {
                $language = null;
                if ($value) {
                    $language = $this->sanitizeFilterText($value);
                }
                $filterHtml .= '
                                <label class="filters__label">' . $parent['name'] . '</label>
                                <div class="select">
                                    <input type="hidden" class="select__input" name="' . $parent['slug'] . '" value="' . $value . '">';

            } elseif ($parent['name'] != 'Advanced Search' && $parent['name'] != 'Types' && $parent['name'] != 'View' && $parent['name'] != 'Sort By') {

                $select_value = 'Select';
                if ($value) {
                    $select_value = $this->sanitizeFilterText($value, 'display');
                    $value = $this->sanitizeFilterText($value);
                }
                $filterHtml .= '
                                <label class="filters__label">' . $parent['name'] . '</label>
                                <div class="select">
                                    <input type="hidden" class="select__input" name="' . $parent['slug'] . '" value="' . $value . '">
                                    <p class="select__text">' . $select_value . '</p>
                                    <ul class="select__list">';

            }
            // Opening tags - End


            // Main content - Start
            $level    = 0;
            if ($parent['name'] == 'Types') {

                $filterCheckboxHtml = $this->recursiveGenerateFiltersHtml($parent, $children, $filter, $filterCheckboxHtml, $level, $queryFilter);

            } elseif ($parent['name'] == 'View') {

                $filterViewHtml = $this->recursiveGenerateFiltersHtml($parent, $children, $filter, $filterViewHtml, $level, $queryFilter);

            } elseif ($parent['name'] == 'Sort By') {

                $filterSortByHtml = $this->recursiveGenerateFiltersHtml($parent, $children, $filter, $filterSortByHtml, $level, $queryFilter);

            } elseif ($parent['name'] == 'Language') {

                $filterHtml = $this->getLanguagesHtml($filterHtml, $language);

            } else {

                $filterHtml = $this->recursiveGenerateFiltersHtml($parent, $children, $filter, $filterHtml, $level, $queryFilter);

            }
            // Main content - End


            // Closing tags - Start
            if ($parent['name'] == 'Sort By') {

                $filterSortByHtml .= '
                                    </ul>
                                </div>';

            } else if ($parent['name'] != 'Advanced Search' && $parent['name'] != 'Types' && $parent['name'] != 'View') {

                $filterHtml .= '
                                    </ul>
                                </div>';

            }

            if ($parent['name'] == 'View') {

                $filterViewHtml .= '
                                </div>
                            </div>';

            } elseif ($parent['name'] == 'Sort By') {

                $filterSortByHtml .= '
                            </div>';

            } elseif ($parent['name'] != 'Types') {

                $filterHtml .= '
                            </div>';

            }
            // Closing tags - End

        }

        return array (
            'filterHtml' => $filterHtml,
            'filterViewHtml' => $filterViewHtml,
            'filterSortByHtml' => $filterSortByHtml,
            'filterCheckboxHtml' => $filterCheckboxHtml
        );

    }

    public function recursiveGenerateFiltersHtml($parent, $children, $filter, $filterHtml, $level, $queryFilter)
    {

        $childCount = count($children);

        $level++;

        $now = (new \DateTime())->format('Y-m-d H:i:s');
        for ($c=0; $c<$childCount; $c++) {

            if ($children[$c]['status'] == 'inactive' || $children[$c]['published'] > $now) {
                continue;
            }

            if ($level == 1) {
                if ($parent['name'] != 'Advanced Search' && $parent['name'] != 'Types') {

                    if ($parent['name'] == 'Models') {

                        $filterHtml .= '
                                        <li class="select__options select__options--parent ' . $children[$c]['slug'] .
                                            '" data-id="' . $children[$c]['id'] . '" data-value="' . $children[$c]['name'] . '">' . $children[$c]['name'] . '</li>';

                    } elseif ($parent['name'] == 'View') {

                        if ($children[$c]['name'] == 'Change Column Number') {

                            $filterHtml .= '
                                            <div class="sort__sizes__options cf">
                                                <a href="#" class="sort__size icon icon--grid-large-active"></a>
                                                <a href="#" class="sort__size icon icon--grid-small-inactive"></a>
                                            </div>';

                        } elseif ($children[$c]['name'] == 'All' && isset($queryFilter['displayShowAll']) && $queryFilter['displayShowAll']) {

                            $showAll = 'sort__size--show-all-inactive';
                            $value = '';
                            if (isset($queryFilter['showAll']) && $queryFilter['showAll']) {
                                $showAll = 'sort__size--show-all-active';
                                $value = 'true';
                            }
                            $filterHtml .= '
                                            <div class="sort__sizes__options cf">
                                                <a href="#" class="js-sort-option sort__size sort__label icon ' . $showAll . '">' . $children[$c]['name'] . '</a>
                                                <input name="' . $children[$c]['slug'] . '" type="hidden" value="' . $value . '">
                                            </div>';

                        }

                    } elseif ($parent['name'] == 'Sort By') {

                        $filterHtml .= '
                                        <li class="select__options js-sort-option" data-value="' . $children[$c]['slug'] . '">' . $children[$c]['name'] . '</li>';

                    } else {

                        $slug = str_replace('-', '', $children[$c]['slug']);
                        $filterHtml .= '
                                        <li class="select__multi ' . $slug . '" data-id="' . $children[$c]['id'] . '" data-value="' . $children[$c]['name'] . '">
                                            <span class="select__multi-text-' . $slug . '">' . $children[$c]['name'] . '</span>
                                            <span class="icon icon--plus"></span>
                                        </li>';

                    }


                } elseif ($parent['name'] == 'Types') {

                    $checked = '';
                    $types = isset($queryFilter['types']) ? $queryFilter['types'] : array();
                    if (in_array($children[$c]['slug'], $types)) {
                        $checked = 'checked="checked"';
                    }
                    $filterHtml .= '
                            <label class="sort__label--checkbox margin--left-10">
                                <label class="checkbox">
                                    <input class="js-sort-option" name="'.$parent['slug'].'[]" type="checkbox" '.$checked.' value="' . $children[$c]['slug'] . '">
                                    <span></span>
                                </label>
                                <span>' . $children[$c]['name'] . '</span>
                            </label>';

                }

            } elseif ($level > 1) {

                $filterHtml .= '
                                            <li class="select__options select__options--child ' . $children[$c]['slug'] .
                                                '" data-id="' . $children[$c]['id'] . '" data-value="'.$children[$c]['name'].'">'.$children[$c]['name'].'</li>';
            }

            $filterHtml = $this->recursiveGenerateFiltersHtml($children[$c], $filter->getChildrenFilters($children[$c]['id']), $filter, $filterHtml, $level, $queryFilter);

        }

        if ($parent['name'] == 'Advanced Search') {

            $search = isset($queryFilter['queryParams'], $queryFilter['queryParams']['search']) ? $queryFilter['queryParams']['search'] : '';
            $filterHtml .= '
                                    <label class="filters__label">' . $parent['name'] . '</label>
                                    <input type="text" name="' . $parent['slug'] . '" class="filters__input float--left ' . $parent['slug'] . '" value="' . $search . '">';

        }

        return $filterHtml;
    }

    public function sanitizeFilterText($str, $type=null)
    {
        if ($type == 'display') {
            $str = explode('|', $str);
            $str = implode(' | ', $str);
        }
        $str = str_replace('  ', ' ', $str);

        return $str;
    }

    public function getLanguagesHtml($filterHtml, $language)
    {
        $locales = \Newspress::getLocales();

        $html = null;
        $select_value = ' Select';
        foreach ($locales as $locale) {

            $html .= '
                                        <li class="select__options" data-value="' . $locale['id'] . '" >' . $locale['name'] . '</li >';
            if ($locale['id'] == $language) {
                $select_value = $locale['name'];
            }

        }

        $filterHtml = $filterHtml .'
                                    <p class="select__text">' . $select_value . '</p>
                                    <ul class="select__list">';
        $filterHtml = $filterHtml . $html;

        return $filterHtml;
    }

}
