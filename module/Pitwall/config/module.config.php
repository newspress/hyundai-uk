<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Pitwall\Controller\Pitwall' => 'Pitwall\Controller\PitwallController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'pitwall' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/gt-pitwall',
                    'defaults' => array(
                        'controller' => 'Pitwall\Controller\Pitwall',
                        'action' => 'index'
                    )
                )
            )
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    )
);
