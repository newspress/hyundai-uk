<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Pitwall\Controller;

use Newspress\Mvc\Controller\BaseActionController;

class PitwallController extends BaseActionController
{
    public function indexAction()
    {
    	$sql = \Newspress::db()->sql();

        $select = $sql->select();
        $select->from('pitwall');
        $select->order('created DESC');

        $posts = \Newspress::db()->execute($select, 'array');

        return $this->viewModel->setVariables(array(
            'posts' => $posts
        ));
    }
}
