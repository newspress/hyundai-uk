<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Contacts\Controller;

use Newspress\Mvc\Controller\BaseActionController;

class ContactsController extends BaseActionController
{
    protected $contactTable;
    /**
     * Index Action
     *
     * @return object ViewModel
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    public function indexAction()
    {
        $sql = \Newspress::db()->sql();

        $now = (new \DateTime())->format('Y-m-d H:i:s');
        $select = $sql->select();
        $select->from('contacts');
        $select->where
            ->equalTo('status', 'active')
            ->lessThan('published', $now);
        $select->order('sort ASC');

        $contacts = \Newspress::db()->execute($select, 'object', false);

        return $this->viewModel->setVariables(array(
                'contacts' => $contacts
            ));
    }

}
