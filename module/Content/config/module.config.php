<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Content\Controller\Content' => 'Content\Controller\ContentController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'content' => array(
                'type' => 'segment',
                'options' => array(
                    // The 'parent' is just to fit a URL routing need
                    // It's not being used
                    'route' => '/:parent[/:slug]',
                    'constraints' => array(
                        'parent' => '[a-zA-Z0-9_-]+',
                        'slug' => '[a-zA-Z0-9_-]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Content\Controller\Content',
                        'action' => 'index'
                    )
                ),
                'priority' => -1000,
            ),
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    )
);
