<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Content\Model;

use Newspress\Model\Model;

class Content extends Model
{
    public $table = 'content';

    public function getContent($slug)
    {
        $now = new \DateTime();

        $sql = \Newspress::db()->sql();

        $select = $sql->select();
        $select->from('content')
               ->where
               ->equalTo('slug', $slug)
               ->equalTo('status', 'active')
               ->lessThan('published', $now->format('Y-m-d H:i:s'));

        $results = \Newspress::db()->execute($select, 'object');

        if (count((array) $results) === 1) {
            return current($results);
        }

        return false;
    }
}
