<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Content\Controller;

use Newspress\Mvc\Controller\BaseActionController;
use Releases\Controller\ReleasesController;
use Zend\View\Model\ViewModel;
use Content\Model\Content;
use Zend\Db\Sql\Expression;

class ContentController extends BaseActionController
{
    protected $content;
    protected $downloads;

    /**
     * __construct method using depenency injection
     */
    public function __construct()
    {
        parent::__construct();
        $this->content = new Content();
    }

    /**
     * Index Action
     *
     * @return object ViewModel
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    public function indexAction()
    {
        $stringSanitizer = $this->getServiceLocator()->get('StringSanitizer');

        $parent = $stringSanitizer->filter($this->params()->fromRoute('parent'));
        $slug = $stringSanitizer->filter($this->params()->fromRoute('slug'));

        $slug = ($slug == null ? $parent : $slug);

        $content = $this->content->getContent($slug);

        // If the content page does not exist, check for redirects and then finish
        // on a 404 page back in the index controller.

        if (!$content) {
            return $this->forward()->dispatch('Index\Controller\Index', array(
                'action' => 'redirects',
                'slug'   => $slug
            ));
        }

        $contentId = (int) $content->id;
        $content = (object) $content;

        $now = new \DateTime();
        $contentLinksArray = array();

        $sql = \Newspress::db()->sql();
        $select = $sql->select();
        $select->from('object_links')
               ->where
               ->equalTo('from_object', 8) // content
               ->equalTo('from_object_id', $content->id);
        $select->order('sort ASC');

        $contentLinks = \Newspress::db()->execute($select);

        foreach ($contentLinks as $contentLink) {
            $contentLinksArray[$contentLink['to_object']][] = $contentLink['to_object_id'];
        }

        $releases = false;
        if (isset($contentLinksArray[1])) {
            $select = $sql->select();
            $select->from('releases');
            $select->where
                   ->in('id', $contentLinksArray[1])
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('status', 'active');
            $select->order(array(new Expression('FIELD (id, ' . implode(',', $contentLinksArray[1]) . ')')))
                   ->limit(6);

            $releases = \Newspress::db()->execute($select);

            if ($releases->count() > 0) {
                $releasesArray = array();
                foreach ($releases as $release) {
                    $releasesArray[] = CoreReleasesController::addAdditionalData($release);
                }
                $releases = $releasesArray;
            }
        }

        $images             = false;
        $videos             = false;
        $documents          = false;
        $mediaCount         = 0;
        $videoCount         = 0;
        $imageCount         = 0;
        $youTubeVideoCount  = 0;

        if (isset($contentLinksArray[2])) {
            $select = $sql->select();
            $select->from('media');
            $select->where
                   ->in('id', $contentLinksArray[2])
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('type', 'image')
                   ->equalTo('status', 'active');
            $select->order(array(new Expression('FIELD (id, ' . implode(',', $contentLinksArray[2]) . ')')))
                   ->limit(6);

            $imageResult = \Newspress::db()->execute($select);

            if ($imageResult->count() > 0) {
                $imageArray = array();
                foreach ($imageResult as $image) {
                    $image = new \Newspress\Asset\Image($image['id']);
                    $imageArray[] = $image;
                }
                $images = $imageArray;
            }

            $select = $sql->select();
            $select->from('media');
            $select->where
                   ->in('id', $contentLinksArray[2])
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('type', 'video')
                   ->equalTo('status', 'active');
            $select->order(array(new Expression('FIELD (id, ' . implode(',', $contentLinksArray[2]) . ')')))
                   ->limit(6);

            $videosResult = \Newspress::db()->execute($select);

            if ($videosResult->count() > 0) {
                $videosArray = array();
                foreach ($videosResult as $video) {
                    $video = new \Newspress\Asset\Video($video['id']);
                    $videosArray[] = $video;
                }
                $videos = $videosArray;
            }

            $select = $sql->select();
            $select->from('media');
            $select->where
                   ->in('id', $contentLinksArray[2])
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('type', 'document')
                   ->equalTo('status', 'active');
            $select->order(array(new Expression('FIELD (id, ' . implode(',', $contentLinksArray[2]) . ')')))
                   ->limit(6);

            $documentsResult = \Newspress::db()->execute($select);

            if ($documentsResult->count() > 0) {
                $documentsArray = array();
                foreach ($documentsResult as $document) {
                    $document = new \Newspress\Asset\Document($document['id']);
                    $documentsArray[] = $document;
                }
                $documents = $documentsArray;
            }
        }


        $imagesCount = (isset($images) && $images) ? count($images) : 0;
        $videosCount = (isset($videos) && $videos) ? count($videos) : 0;
        $mediaCount = $imagesCount + $videosCount;



        if ($mediaCount >= 6) {

            $contentId = ($content->id == 1 ? 89 : $content->id);

            $sql = \Newspress::db()->sql();
            $select = $sql->select();
            $select->from('object_links')
                   ->where
                   ->equalTo('from_object', 7) // content
                   ->equalTo('from_object_id', $contentId);
            $select->order('sort ASC');

            $contentLinks = \Newspress::db()->execute($select);

            $contentLinksArray = array();

            foreach ($contentLinks as $contentLink) {
                $contentLinksArray[$contentLink['to_object']][] = $contentLink['to_object_id'];
            }

            if($contentLinksArray){

                $select = $sql->select();
                $select->from('media');
                $select->where
                    ->in('id', $contentLinksArray[2])
                    ->lessThan('published', $now->format('Y-m-d H:i:s'))
                       ->equalTo('type', 'image')
                       ->equalTo('status', 'active');
                $imageResults = \Newspress::db()->execute($select);
                $imageCount = $imageResults->count();

                $select = $sql->select();
                $select->from('media');
                $select->where
                        ->in('id', $contentLinksArray[2])
                       ->lessThan('published', $now->format('Y-m-d H:i:s'))
                       ->equalTo('type', 'video')
                       ->equalTo('status', 'active');
                $videoResults = \Newspress::db()->execute($select);
                $videoCount = $videoResults->count();
            }

            $mediaCount = $imageCount + $videoCount;

        }



        $packs = false;

        if (isset($contentLinksArray[5])) {
            $select = $sql->select();
            $select->from('packs');
            $select->where
                   ->in('id', $contentLinksArray[5]);
            $select->order(array(new Expression('FIELD (id, ' . implode(',', $contentLinksArray[5]) . ')')));

            $packs = \Newspress::db()->execute($select);

            $packsArray = array();
            foreach ($packs as $pack) {
                $packsArray[$pack['type']][] = $pack;
            }
            $packs = $packsArray;
        }

        return $this->viewModel->setVariables(array(

            'releases'          => $releases,
            'images'            => $images,
            'imageCount'        => $imageCount,
            'videos'            => $videos,
            'videoCount'        => $videoCount,
            'downloads'         => $this->getDownloadsModel(),
            'documents'         => $documents,
            'mediaCount'        => $mediaCount,
            'packs'             => $packs,
            'content'           => $content,
            'browser'   => $this->getBrowser()

        ));
    }

    public function getDownloadsModel()
    {
        if (!$this->downloads) {
            $sm = $this->getServiceLocator();
            $this->downloads = $sm->get('DownloadsModel');
        }
        return $this->downloads;
    }
}
