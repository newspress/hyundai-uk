<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'PressKits\Controller\PressKits' => 'PressKits\Controller\PressKitsController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'press_kits' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '[/:locale]/press-kits',
                    'constraints' => array(
                        'locale' => '[a-z]{2}-[a-z]{2}|[a-z]{2}',
                    ),
                    'defaults' => array(
                        'controller' => 'PressKits\Controller\PressKits',
                        'action'     => 'index',
                        'locale'     => 'en-gb',
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'detail' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    => '/:id',
                            'constraints' => array(
                                'id' => '([0-9]+|[a-zA-Z][a-zA-Z0-9_\/-]*)',
                            ),
                            'defaults' => array(
                                'controller' => 'PressKits\Controller\PressKits',
                                'action'     => 'detail',
                            ),
                        ),
                    ),
                ),
            )
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    )
);
