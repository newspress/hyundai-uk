<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace PressKits\Model;

class PressKit
{
    public $id;

    public function __construct($id = null)
    {
        if ($id !== null) {
            $this->id = (int) $id;
        }
    }

    public function getNavigationHtml()
    {
        $sql = \Newspress::db()->sql();
        $select = $sql->select('press_kits');
        $select->where
               ->equalTo('id', $this->id);

        $pressKit = \Newspress::db()->execute($select, 'array');

        if (count($pressKit) == 0) {
            return '';
        }

        $pressKit = $pressKit[0];

        $image = false;
        if ($pressKit['dropdown'] !== null) {
            $image = new \Newspress\Asset\Image((int) $pressKit['dropdown']);
            $image = $image->getUrl('dropdown');
        }




        return '<li><a href="/press-kits/' . ($pressKit['slug'] !== null ?  $pressKit['slug'] : $this->id)  . '"' . ($image ? ' data-image="' . $image . '"' : '') . '>' . \Index\Model\Index::strtoupper($pressKit['title']) . '</a></li>';
    }
}
