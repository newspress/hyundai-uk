<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace PressKits\Controller;

use Newspress\Mvc\Controller\BaseActionController;
use Newspress\Mvc\Controller\CoreReleasesController;
use Zend\Db\Sql\Expression;
use Application\Model\Object;

class PressKitsController extends BaseActionController
{
    protected $downloads;

    /**
     * Index Action
     *
     * @return object ViewModel
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    public function indexAction()
    {
        return $this->redirect()->toRoute('error');
    }

    public function detailAction()
    {
        $stringSanitizer = $this->getServiceLocator()->get('StringSanitizer');

        $slug = $stringSanitizer->filter($this->params()->fromRoute('id'));

        $sql = \Newspress::db()->sql();
        $now = new \DateTime();

        $select = $sql->select();
        $select->from('press_kits')
            ->where
            ->equalTo('slug', $slug)
            ->lessThan('published', $now->format('Y-m-d H:i:s'));

        $pressKit = \Newspress::db()->execute($select, 'array');

        if (count($pressKit) == 0) {
            // Could not find press kit
            return $this->redirect()->toRoute('error');
        }

        $pressKit = (object) $pressKit[0];

        $routeLocale = $stringSanitizer->filter($this->params()->fromRoute('locale'));
        $locale = \Newspress\Locale::getLocaleUrl($routeLocale);

        // Redirect if the URL friendly route is not the same as the current route
        if ($locale != $routeLocale) {
            return $this->redirect()->toRoute('releases/detail', array(
                    'locale' => $locale,
                    'id' => $pressKit['id']
                ));
        }



        // Get the correct locale string
        $locale = \Newspress\Locale::getLocaleFromUrl($locale);

        // Get the locale ID (default is false)
        $localeId = false;
        $locales = \Newspress::getLocales();
        foreach ($locales as $configLocale) {
            if ($configLocale['locale'] == $locale) {
                $localeId = $configLocale['id'];
            }
        }

        // If we can't find the locale, redirect to default
        if ($localeId === false) {
            // @todo: Make sure this doesn't redirect more than once
            return $this->redirect()->toRoute('press-kits/detail', array(
                    'id' => $pressKit['id']
                ));
        }


        $pressKitLinksArray = array();

        $pressKitLinkObject = new Object(null, 3, (int) $pressKit->id);
        $pressKitLinks = $pressKitLinkObject->getObjectLinks();

        foreach ($pressKitLinks as $pressKitLink) {
            $toObjectIdsArray[$pressKitLink['to_object_id']] = $pressKitLink['to_object_id'];
            $pressKitLinksArray[$pressKitLink['to_object']][] = $pressKitLink['to_object_id'];
        }

        // get media locales
        $select = $sql->select();
        $select->from('media_links');
        $select->where
                ->equalTo('object_id', $pressKit->id)
                ->in('media_id', $toObjectIdsArray);

        $localesArray = \Newspress::db()->execute($select);

        foreach ($localesArray as $media_locale) {
            $pressKitLinksLocaleArray[$media_locale['media_id']] = $media_locale['locale'];
        }

        if (count($pressKitLinksArray) == 0) {
            // @todo: Work out where this should redirect if the client does not want a models page
            return $this->redirect()->toRoute('error');
        }

        $releases = false;
        if (isset($pressKitLinksArray[1])) {

            $select = $sql->select();
            $select->from('releases');
            $select->where
                   ->in('id', $pressKitLinksArray[1])
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('status', 'active');
            $select->order(array(new Expression('FIELD (id, ' . implode(',', $pressKitLinksArray[1]) . ')')))
                   ->limit(6);

            $releases = \Newspress::db()->execute($select);

            if ($releases->count() > 0) {
                $releasesArray = array();
                foreach ($releases as $release) {
                    $releasesArray[] = CoreReleasesController::addAdditionalData($release);
                }
                $releases = $releasesArray;
            }
        }

        $images    = false;
        $videos    = false;
        $documents = false;

        if (isset($pressKitLinksArray[2])) {

            $select = $sql->select();
            $select->from('media');
            $select->where
                   ->in('id', $pressKitLinksArray[2])
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('type', 'image')
                   ->equalTo('lead_image', 0)
                   ->equalTo('status', 'active');
            $select->order(array(new Expression('FIELD (id, ' . implode(',', $pressKitLinksArray[2]) . ')')))
                   ->limit(6);

            $imageResult = \Newspress::db()->execute($select);

            if ($imageResult->count() > 0) {
                $imageArray = array();

                foreach ($imageResult as $image) {
                    $image = new \Newspress\Asset\Image($image['id']);
                    $imageArray[] = $image;
                }

                $images = $imageArray;

            }

            $select = $sql->select();
            $select->from('media');
            $select->where
                   ->in('id', $pressKitLinksArray[2])
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('type', 'video')
                   ->equalTo('status', 'active');
            $select->order(array(new Expression('FIELD (id, ' . implode(',', $pressKitLinksArray[2]) . ')')))
                   ->limit(6);

            $videosResult = \Newspress::db()->execute($select);

            if ($videosResult->count() > 0) {

                $videosArray = array();

                foreach ($videosResult as $video) {
                    $video = new \Newspress\Asset\Video($video['id']);
                    $videosArray[] = $video;
                }

                $videos = $videosArray;

            }

            $select = $sql->select();
            $select->from('media');
            $select->where
                   ->in('id', $pressKitLinksArray[2])
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('type', 'document')
                   ->equalTo('status', 'active');
            $select->order(array(new Expression('FIELD (id, ' . implode(',', $pressKitLinksArray[2]) . ')')))
                   ->limit(6);

            $documentsResult = \Newspress::db()->execute($select);

            if ($documentsResult->count() > 0) {

                $documentsArray = array();

                foreach ($documentsResult as $document) {

                    // if the document has the locale set
                    // check if it is equal to the current locale
                    // if it's not, the file must not be displayed
                    if (isset($pressKitLinksLocaleArray[$document['id']]) && $pressKitLinksLocaleArray[$document['id']] !== null) {
                        if ($pressKitLinksLocaleArray[$document['id']] !== $localeId) {
                            continue;
                        }
                    }
                    $document = new \Newspress\Asset\Document($document['id']);
                    $documentsArray[] = $document;
                }

                $documents = $documentsArray;

            }

        }


        $imagesCount = (isset($images) && $images) ? count($images) : 0;
        $videosCount = (isset($videos) && $videos) ? count($videos) : 0;
        $mediaCount = $imagesCount + $videosCount;

        if ($mediaCount >= 6) {
            $mediaObject = new Object(null, 7, (int) $pressKit->category_id, 2);
            $mediaObjectIds = $mediaObject->getObjectLinks(true);
            $mediaCount = count($mediaObjectIds);

            $select = $sql->select();
            $select->from('media');
            $select->where
                   ->in('id', $mediaObjectIds)
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('type', 'image')
                   ->equalTo('lead_image', 0)
                   ->equalTo('status', 'active');
            $imageResults = \Newspress::db()->execute($select);
            $imageCount = $imageResults->count();

            $select = $sql->select();
            $select->from('media');
            $select->where
                   ->in('id', $mediaObjectIds)
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->equalTo('type', 'video')
                   ->equalTo('status', 'active');
            $videoResults = \Newspress::db()->execute($select);
            $videoCount = $videoResults->count();

            $mediaCount = $imageCount + $videoCount;
        }

        $packs = false;

        $packIdArray = array();
        $languages = array();

        if (isset($pressKitLinksArray[5])) {

            $packs = \Newspress::db()->query('SELECT * FROM `packs` WHERE `id` IN (' . implode(',', $pressKitLinksArray[5]) . ') AND `status` = \'active\' AND `published` < \'' . $now->format('Y-m-d H:i:s') . '\' ORDER BY -sort DESC');
            $packsArray = array();

            foreach ($packs as $pack) {

                // Query releases with release ID and language ID
                $select = $sql->select();
                $select->from('packs_translations')
                       ->where
                       ->equalTo('pack', $pack['id'])
                       ->equalTo('locale', $localeId);
                $select->limit(1);

                $results = \Newspress::db()->execute($select);

                $packTranslation = $results->current();

                $pack['title']   = ($packTranslation['title']) ? $packTranslation['title'] : $pack['title'];
                $pack['content'] = ($packTranslation['content']) ? $packTranslation['content'] : $pack['content'];

                $packsArray[$pack['type']][] = $pack;
                $packIdArray[] = $pack['id'];
            }

            $packs = $packsArray;

            $packTranslationObjects = \Newspress::db()->query('SELECT * FROM `object_links` WHERE `from_object` = 5 AND FIND_IN_SET(`from_object_id`, \'' . implode(',', $pressKitLinksArray[5]) . '\') AND `locale` = ' . $localeId . ' ORDER BY `sort` ASC;');

            foreach ($packTranslationObjects as $packTranslationObject) {
                if (!$documents) {
                    $documents = array();
                }
                $documents[] = (int) $packTranslationObject['to_object_id'];
            }


            if (count($packIdArray) > 0) {
                $select = $sql->select();
                $select->from('packs_translations')
                       ->where
                       ->in('pack', $packIdArray);

                $results = \Newspress::db()->execute($select, 'array');

                foreach ($results as $language) {
                    if ($language['locale'] == 1) { continue; }
                    $languages[] = $language['locale'];
                }

                $languages = array_unique($languages);
            }
        }



        return $this->viewModel->setVariables(array(
            'releases'   => $releases,
            'images'     => $images,
            'imageCount' => $imageCount,
            'videos'     => $videos,
            'videoCount' => $videoCount,
            'mediaCount' => $mediaCount,
            'locale'     => $locale,
            'languages'  => $languages,
            'downloads'  => $this->getDownloadsModel(),
            'documents'  => $documents,
            'packs'      => $packs,
            'pressKit'   => $pressKit,
            'browser'    => $this->getBrowser()
        ));
    }

    public function getDownloadsModel()
    {
        if (!$this->downloads) {
            $sm = $this->getServiceLocator();
            $this->downloads = $sm->get('DownloadsModel');
        }
        return $this->downloads;
    }
}
