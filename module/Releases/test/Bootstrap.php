<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace ReleasesTest;

use Zend\Loader\AutoloaderFactory;
use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;
use RuntimeException;

error_reporting(E_ALL | E_STRICT);
chdir(__DIR__);

class Bootstrap
{
    protected static $serviceManager;

    public static function init()
    {
        $zf2ModulePaths = array(dirname(dirname(__DIR__)));
        if (($path = static::findParentPath('vendor'))) {
            $zf2ModulePaths[] = $path;
        }
        if (($path = static::findParentPath('core'))) {
            $zf2ModulePaths[] = $path;
        }
        if (($path = static::findParentPath('module')) !== $zf2ModulePaths[0]) {
            $zf2ModulePaths[] = $path;
        }

        static::initAutoloader();

        // use ModuleManager to load this module and it's dependencies
        $config = array(
            'module_listener_options' => array(
                'module_paths' => $zf2ModulePaths,
            ),
            'modules' => array(
                'Releases'
            )
        );

        var_dump(\Newspress::isProduction());
        exit;

        $serviceManager = new ServiceManager(new ServiceManagerConfig());
        $serviceManager->setService('ApplicationConfig', $config);
        $serviceManager->get('ModuleManager')->loadModules();
        static::$serviceManager = $serviceManager;
    }

    public static function chroot()
    {
        $rootPath = dirname(static::findParentPath('module'));
        chdir($rootPath);
    }

    public static function getServiceManager()
    {
        return static::$serviceManager;
    }

    protected static function initAutoloader()
    {
        $vendorPath = static::findParentPath('vendor');

        if (file_exists('vendor/autoload.php')) {
            $loader = include 'vendor/autoload.php';
        }

        $zf2Path = false;

        if (is_dir('vendor/ZF2/library')) {
            $zf2Path = 'vendor/ZF2/library';
        } elseif (getenv('ZF2_PATH')) {      // Support for ZF2_PATH environment variable or git submodule
            $zf2Path = getenv('ZF2_PATH');
        } elseif (get_cfg_var('zf2_path')) { // Support for zf2_path directive value
            $zf2Path = get_cfg_var('zf2_path');
        }

        $corePath = false;

        if (is_dir('core')) {
            $corePath = 'core';
        } elseif (getenv('CORE_PATH')) {      // Support for CORE_PATH environment variable or git submodule
            $corePath = getenv('CORE_PATH');
        } elseif (get_cfg_var('core_path')) { // Support for core_path directive value
            $corePath = get_cfg_var('core_path');
        }

        if ($zf2Path) {
            if (isset($loader)) {
                $loader->add('Zend', $zf2Path);
                $loader->add('ZendXml', $zf2Path);
                $loader->add('Core', $corePath);
            } else {
                include $zf2Path . '/Zend/Loader/AutoloaderFactory.php';
                Zend\Loader\AutoloaderFactory::factory(array(
                    'Zend\Loader\StandardAutoloader' => array(
                        'autoregister_zf' => true,
                        'namespaces' => array(
                            __NAMESPACE__ => __DIR__ . '/' . __NAMESPACE__,
                        ),
                    )
                ));
            }
        }

        // $zf2Path = getenv('ZF2_PATH');
        // if (!$zf2Path) {
        //     if (defined('ZF2_PATH')) {
        //         $zf2Path = ZF2_PATH;
        //     } elseif (is_dir($vendorPath . '/ZF2/library')) {
        //         $zf2Path = $vendorPath . '/ZF2/library';
        //     } elseif (is_dir($vendorPath . '/zendframework/zendframework/library')) {
        //         $zf2Path = $vendorPath . '/zendframework/zendframework/library';
        //     }
        // }

        // if (!$zf2Path) {
        //     throw new RuntimeException(
        //         'Unable to load ZF2. Run `php composer.phar install` or'
        //         . ' define a ZF2_PATH environment variable.'
        //     );
        // }

        // if (file_exists($vendorPath . '/autoload.php')) {
        //     include $vendorPath . '/autoload.php';
        // }

        // include $zf2Path . '/Zend/Loader/AutoloaderFactory.php';
        // AutoloaderFactory::factory(array(
        //     'Zend\Loader\StandardAutoloader' => array(
        //         'autoregister_zf' => true,
        //         'namespaces' => array(
        //             __NAMESPACE__ => __DIR__ . '/' . __NAMESPACE__,
        //         ),
        //     ),
        // ));
    }

    protected static function findParentPath($path)
    {
        $dir = __DIR__;
        $previousDir = '.';
        while (!is_dir($dir . '/' . $path)) {
            $dir = dirname($dir);
            if ($previousDir === $dir) {
                return false;
            }
            $previousDir = $dir;
        }
        return $dir . '/' . $path;
    }
}

Bootstrap::init();
Bootstrap::chroot();
