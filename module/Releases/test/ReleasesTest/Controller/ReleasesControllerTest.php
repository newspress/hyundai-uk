<?php

namespace ReleasesTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class ReleasesControllerTest extends AbstractHttpControllerTestCase
{
    public function setUp()
    {
        $this->setApplicationConfig(
            include '/Users/oliver/Sites/mclaren/config/application.config.php'
        );
        parent::setUp();
    }

    public function testIndexActionCanBeAccessed()
	{
	    $this->dispatch('/releases');
	    $this->assertResponseStatusCode(200);

	    $this->assertModuleName('Releases');
	    $this->assertControllerName('Releases\Controller\Releases');
	    $this->assertControllerClass('ReleasesController');
	    $this->assertMatchedRouteName('releases');
	}
}