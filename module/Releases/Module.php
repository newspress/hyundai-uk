<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Releases;

use ReleasesAdmin\Model\Release;
use ReleasesAdmin\Model\ReleaseTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'ReleasesAdmin\Model\ReleaseTable' => function($sm) {
                    $tableGateway = $sm->get('ReleaseTableGateway');
                    $table = new ReleaseTable($tableGateway);
                    return $table;
                },
                'ReleaseTableGateway' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Release());
                    return new TableGateway('releases', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }
}
