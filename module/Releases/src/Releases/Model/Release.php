<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Releases\Model;

use Newspress\Model\Model;

class Release extends Model
{
	public $table    = 'releases';
	// public $indexing = true;

	public function getAvailableLocales()
	{
		// Get available locales for this release
	}
}
