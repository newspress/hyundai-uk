<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Releases\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Ddl\Column;
use Zend\Db\Sql\Ddl\Constraint;

class ReleasesStructure extends Structure
{
	protected $adapter;

	public function __construct(Adapter $adapter)
	{
		$this->adapter = $adapter;
	}

	public function createTable()
	{
		$sql = new Sql($this->adapter);

		$table = new Ddl\CreateTable('releases');

		$table->addColumn(new Column\Integer('id'));
		$table->addColumn(new Column\Varchar('name', 255));

		$table->addConstraint(new Constraint\PrimaryKey('id'));

		$this->adapter->query(
		    $sql->getSqlStringForSqlObject($ddl),
		    $this->adapter::QUERY_MODE_EXECUTE
		);	
	}
}
