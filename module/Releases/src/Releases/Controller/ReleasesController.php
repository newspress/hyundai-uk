<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Releases\Controller;

use MediaAdmin\Model\Media;
use Newspress\Mvc\Controller\CoreReleasesController;
use Releases\Model\Release;
use ReleasesAdmin\Model\Release as ReleaseAdmin;
use Search\Model\Search;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class ReleasesController extends CoreReleasesController
{
    protected $release;
    protected $releaseTable;
    protected $downloads;
    protected $categories;

    public function __construct()
    {
        parent::__construct();
        $this->release = new Release();
    }

    public function indexAction()
    {
        $stringSanitizer = $this->getServiceLocator()->get('StringSanitizer');

        $releases = array();
        
        $pageCount  = 0;
        $totalCount = 0;
        $showing    = 0;
        $showingTo  = 0;

        $mediaObj    = new Media();
        $queryFilter = $mediaObj->getMediaFilter($this->params());

        if (count($queryFilter['queryParams']) > 0 ) {

            $select = $mediaObj->prepareMediaQuery($queryFilter, 'release');
            if ($select) {

                $pagination = $this->getReleaseTable()->fetchPaginated($select);

            } else {

                $pagination = $this->getReleaseTable()->fetchAllByStatusAndLessThanPublishedAndOrderByPublished(true, 'active', (new \DateTime())->format('Y-m-d H:i:s'), 'DESC');

            }

        } elseif (isset($queryFilter['order']) && $queryFilter['order'] != 'relevance') {

            $pagination = $this->getReleaseTable()->fetchAllByStatusAndLessThanPublishedAndOrderByPublished(true, 'active', (new \DateTime())->format('Y-m-d H:i:s'), $queryFilter['orderBy']);

        } else {

            $pagination = $this->getReleaseTable()->fetchAllByStatusAndLessThanPublishedAndOrderByPublished(true, 'active', (new \DateTime())->format('Y-m-d H:i:s'), 'DESC');

        }

        if ($pagination && count($pagination) > 0) {
            $page = $stringSanitizer->filter($this->params()->fromQuery('page', 1));
            $pagination->setCurrentPageNumber((int) $page);
            $pagination->setItemCountPerPage(12);
            $pagination->setPagerange(5);
            $releases = CoreReleasesController::addAdditionalDataToArray($pagination);

            $pageCount = $pagination->getItemCountPerPage();
            $totalCount = $pagination->getTotalItemCount();

            $showing = $this->getShowing($pagination);
            $showingTo = $this->getShowingTo($pagination);
        }

        // @todo: Update the page count to get the current page results (check last page of any results)

        if ($pageCount > $totalCount) {
            $pageCount = $totalCount;
        }

        $search = new Search();
        $queryFilter['displayShowAll'] = ($totalCount > $pageCount);
        $searchFilters = $search->getSearchFilters($queryFilter, array('category', 'region', 'company', 'events', ));

        $searchFilters['filterSortByHtml'] = str_replace(' tablet--float-right', '', $searchFilters['filterSortByHtml']);
        $searchFilters['filterSortByHtml'] = str_replace('-third', '-half', $searchFilters['filterSortByHtml']);

        return $this->viewModel->setVariables(array(
            'pagination'  => $pagination,
            'releases'    => $releases,
            'queryParams' => $queryFilter['queryParams'],
            'sort'        => $queryFilter['sort'],
            'order'       => $queryFilter['order'],
            'orderBy'     => $queryFilter['orderBy'],
            'showing'     => $showing,
            'showingTo'   => $showingTo,
            'pageCount'   => $pageCount,
            'totalCount'  => $totalCount,
            'browser'     => $this->getBrowser(),
            'filterHtml'  => $searchFilters['filterHtml'],
            'filterSortByHtml'   => $searchFilters['filterSortByHtml'],
        ));
    }

    public function detailAction()
    {
        $stringSanitizer = $this->getServiceLocator()->get('StringSanitizer');

        // $slug = $this->params()->fromRoute('slug');
        // $release = $this->release->findByIdAndStatus($slug, 'active');

        $id = (int) $stringSanitizer->filter($this->params()->fromRoute('id'));
        $routeLocale = $stringSanitizer->filter($this->params()->fromRoute('locale'));
        $locale = \Newspress\Locale::getLocaleUrl($routeLocale);

        // Redirect if the URL friendly route is not the same as the current route
        if ($locale != $routeLocale) {
            return $this->redirect()->toRoute('releases/detail', array(
                    'locale' => $locale,
                    'id' => $id
                ));
        }

        // Find the release using the ID specified
        $release = $this->release->findByIdAndStatus($id, 'active');

        // @todo: Add flash message saying release does not exist
        if ($release->count() == 0) {
            return $this->redirect()->toRoute('releases');
        }

        // Check if the release is published
        $date = new \DateTime();
        if ($date->format('Y-m-d H:i:s') < $release->published) {
            return $this->redirect()->toRoute('releases');
        }
        
        // Get the correct locale string
        $urlLocale = $locale;
        $locale    = \Newspress\Locale::getLocaleFromUrl($locale);

        // Get the locale ID (default is false)
        $localeId = false;
        $locales = \Newspress::getLocales();
        foreach ($locales as $configLocale) {
            if ($configLocale['locale'] == $locale) {
                $localeId = $configLocale['id'];
            }
        }

        // If we can't find the locale, redirect to default
        if ($localeId === false) {
            // @todo: Make sure this doesn't redirect more than once
            return $this->redirect()->toRoute('releases/detail', array(
                    'id' => $id
                ));
        }

        // Query releases with release ID and language ID
        $sql = \Newspress::db()->sql();
        $select = $sql->select();
        $select->from('releases_translations')
               ->where
               ->equalTo('release', $id)
               ->equalTo('locale', $localeId);
        $select->limit(1);

        $results = \Newspress::db()->execute($select);

        // If there is no release available in that language, redirect
        if ($results->count() == 0 && $routeLocale != 'en-gb') {
            return $this->redirect()->toRoute('releases/detail', array(
                    'id' => $id
                ));
        }

        $releaseTranslation = $results->current();

        $release->title   = ($releaseTranslation['title']) ? $releaseTranslation['title'] : $release->title;
        $release->excerpt = ($releaseTranslation['excerpt']) ? $releaseTranslation['excerpt'] : $release->excerpt;
        $release->content = ($releaseTranslation['content']) ? $releaseTranslation['content'] : $release->content;

        $release = CoreReleasesController::addAdditionalData($release);

        $now = new \DateTime();
        $select = $sql->select();

        if ($localeId == 1) {
            $select->from('media_links')
                ->join('media', 'media.id = media_links.media_id', array('media_id' => 'id', 'media_type' => 'type'))
                ->where
                ->equalTo('media.status', 'active')
                ->equalTo('media.lead_image', 0)
                ->lessThan('media.published', $now->format('Y-m-d H:i:s'))
                ->equalTo('media_links.object', 1)
                ->equalTo('media_links.object_id', $id)
                ->nest()
                ->isNull('media_links.locale')
                ->or
                ->equalTo('media_links.locale', 1)
                ->unnest();
        } else {
            $select->from('media_links')
                ->join('media', 'media.id = media_links.media_id', array('media_id' => 'id', 'media_type' => 'type'))
                ->where
                ->equalTo('media.status', 'active')
                ->equalTo('media.lead_image', 0)
                ->lessThan('media.published', $now->format('Y-m-d H:i:s'))
                ->equalTo('media_links.object', 1)
                ->equalTo('media_links.object_id', $id)
                ->nest()
                ->equalTo('media_links.locale', $localeId)
                ->or
                ->isNull('media_links.locale')
                ->unnest();
        }

        $linkedMedias = \Newspress::db()->execute($select);

        $medias = array();
        foreach ($linkedMedias as $linkedMedia) {
            $medias[\Newspress\String::pluralise($linkedMedia['media_type'])][] = $linkedMedia['media_id'];
        }

        $images    = (isset($medias['images']) && count($medias['images'])) ? $medias['images'] : array();
        $videos    = (isset($medias['videos']) && count($medias['videos'])) ? $medias['videos'] : array();
        $documents = (isset($medias['documents']) && count($medias['documents'])) ? $medias['documents'] : array();

        $previousAndNextId = $this->getPreviousAndNextId($id);
        $previousReleaseId = $previousAndNextId['previous'];
        $nextReleaseId     = $previousAndNextId['next'];

        $this->viewModel->setVariables(array(
                'release'           => $release,
                'locale'            => $locale,
                'urlLocale'         => $urlLocale,
                'images'            => $images,
                'videos'            => $videos,
                'documents'         => $documents,
                'downloads'         => $this->getDownloadsModel(),
                'previousReleaseId' => $previousReleaseId,
                'nextReleaseId'     => $nextReleaseId,
                'browser'   => $this->getBrowser()
            ));

        return $this->viewModel;
    }

    /**
     * 
     * @param  [type] $releaseId [description]
     * @return [type]            [description]
     */
    public function getPreviousAndNextId($releaseId)
    {
        $date = new \DateTime();
        $sql = \Newspress::db()->sql();

        $select = $sql->select();
        $select->from('object_links')
               ->where
               ->equalTo('from_object', 7)
               ->equalTo('to_object', 1)
               ->equalTo('to_object_id', $releaseId);
        $select->limit(1);

        $category = \Newspress::db()->execute($select);
        $category = $category->current();
        $category = (int) $category['from_object_id'];

        $previous = $sql->select();
        $previous->from('object_links')
                 ->join('releases', 'releases.id = object_links.to_object_id')
                 ->where
                 ->equalTo('object_links.from_object', 7)
                 ->equalTo('object_links.from_object_id', $category)
                 ->equalTo('object_links.to_object', 1)
                 ->lessThan('object_links.to_object_id', $releaseId)
                 ->lessThan('releases.published', $date->format('Y-m-d H:i:s'))
                 ->equalTo('releases.status', 'active');
        $previous->order(array('releases.published DESC', 'releases.id DESC'))
                 ->limit(1);

        $next = $sql->select();
        $next->from('object_links')
             ->join('releases', 'releases.id = object_links.to_object_id')
             ->where
             ->equalTo('object_links.from_object', 7)
             ->equalTo('object_links.from_object_id', $category)
             ->equalTo('object_links.to_object', 1)
             ->greaterThan('object_links.to_object_id', $releaseId)
             ->lessThan('releases.published', $date->format('Y-m-d H:i:s'))
             ->equalTo('releases.status', 'active');
        $next->order(array('releases.published ASC', 'releases.id ASC'))
             ->limit(1);

        $previous->combine($next);
        
        $previousAndNextResults = \Newspress::db()->execute($previous);

        $previousReleaseId = false;
        $nextReleaseId     = false;

        $previousAndNext = array();
        foreach ($previousAndNextResults as $previousAndNextResult) {
            $previousAndNext[] = (int) $previousAndNextResult['id'];
        }

        if (count($previousAndNext) == 2) {
            $previousReleaseId = $previousAndNext[0];
            $nextReleaseId = $previousAndNext[1];
        } elseif (count($previousAndNext) == 1) {
            if ($previousAndNext[0] < $releaseId) {
                $previousReleaseId = $previousAndNext[0];
            } else {
                $nextReleaseId = $previousAndNext[0];
            }
        }

        return array(
                'previous' => $previousReleaseId,
                'next'     => $nextReleaseId,
            );
    }

    public function downloadAction()
    {
        $stringSanitizer = $this->getServiceLocator()->get('StringSanitizer');

        $id = (int) $stringSanitizer->filter($this->params()->fromRoute('id'));
        $locale = \Newspress\Locale::getLocaleUrl($stringSanitizer->filter($this->params()->fromRoute('locale')));

        // Find the release using the ID specified
        $release = $this->release->findByIdAndStatus($id, 'active');

        // @todo: Add flash message saying release does not exist
        if ($release->count() == 0) {
            return $this->redirect()->toRoute('releases');
        }

        // Check if the release is published
        $date = new \DateTime();
        if ($date->format('Y-m-d H:i:s') < $release->published) {
            return $this->redirect()->toRoute('releases');
        }

        ob_start();
        include 'module/Releases/view/releases/partials/pdf.phtml';
        $html = ob_get_contents();
        ob_end_clean();

        $dompdf = new \DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();
        // $dompdf->stream(\Newspress\String::makeUrlFriendly($release->id . '-release.pdf', array('Attachment' => 0));
        $dompdf->stream(\Newspress\String::makeUrlFriendly($release->id . '-release.pdf'));
    }

    public function getCategoryChildren($category)
    {
        $sql = \Newspress::db()->sql();

        $select = $sql->select();
        $select->from('categories')
               ->where('`parent` = ' . $category->id);

        $results = \Newspress::db()->execute($select, 'object');
        
        if (count($results) > 0) {
            foreach ($results as $result) {
                $this->categories[] = $result->id;
                $this->getCategoryChildren($result);
            }
        }
    }

    public function categoryAction()
    {
        $stringSanitizer = $this->getServiceLocator()->get('StringSanitizer');

        $categoryId = (int) $stringSanitizer->filter($this->params()->fromRoute('id'));

        $order   = $stringSanitizer->filter($this->params()->fromQuery('order', false));
        $orderBy = $stringSanitizer->filter($this->params()->fromQuery('order-by', 'asc'));

        if ($order && (!in_array($order, array('published')))) {
            $order = false;
        }
        if (!in_array($orderBy, array('asc', 'desc'))) {
            $orderBy = 'asc';
        }

        $sql = \Newspress::db()->sql();

        $select = $sql->select();
        $select->from('categories')
               ->where
               ->equalTo('id', $categoryId);

        $category = \Newspress::db()->execute($select);
        if (!$category->count() > 0) {
            return $this->redirect()->toRoute('error');
        }
        $category = (object) $category->current();


        $this->categories[] = $category->id;
        if ($categoryId == 13 && $this->getRequest()->getQuery('referer', false) == 'models') {
            $this->categories[] = 15;
        }
        $this->getCategoryChildren($category);

        $now = new \DateTime();
        $categoryLinksArray = array();

        $select = $sql->select();
        $select->from('object_links')
               ->where
               ->equalTo('from_object', 6)
               ->in('from_object_id', $this->categories)
               ->equalTo('to_object', 1);
        $select->order('sort ASC');

        $categoryLinks = \Newspress::db()->execute($select);

        foreach ($categoryLinks as $categoryLink) {
            $categoryLinksArray[$categoryLink['to_object']][] = $categoryLink['to_object_id'];
        }

        if (count($categoryLinksArray) == 0) {
            return $this->redirect()->toRoute('releases');
        }

        $select = new Select('releases');
        $select->where
               ->in('id', $categoryLinksArray[1])
               ->lessThan('published', $now->format('Y-m-d H:i:s'))
               ->equalTo('status', 'active');

        if ($order && $order != 'relevance') {
            $select->order($order . ' ' . $orderBy);
        } else {
            $select->order(array(new Expression('FIELD (id, ' . implode(',', $categoryLinksArray[1]) . ')')));
        }

        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new ReleaseAdmin());

        $paginatorAdapter = new DbSelect(
            $select,
            $this->getReleaseTable()->getTableGateway()->getAdapter(),
            $resultSetPrototype
        );

        $pagination = new Paginator($paginatorAdapter);
        $page = $stringSanitizer->filter($this->params()->fromQuery('page', 1));
        $pagination->setCurrentPageNumber((int) $page);
        $pagination->setItemCountPerPage(12);
        $pagination->setPagerange(5);

        $pageCount = $pagination->getItemCountPerPage();
        $totalCount = $pagination->getTotalItemCount();

        if ($pageCount > $totalCount) {
            $pageCount = $totalCount;
        }

        $releases = CoreReleasesController::addAdditionalDataToArray($pagination);

        return $this->viewModel->setVariables(array(
                'pagination' => $pagination,
                'releases' => $releases,
                'category' => $category,
                'order' => $order,
                'orderBy' => $orderBy,
                'pageCount' => $pageCount,
                'totalCount' => $totalCount,
                'browser'   => $this->getBrowser()
            ));
    }

    public static function getReleases()
    {
        $releases = array(
                0 => (object) array(
                        'date' => new \DateTime('now'),
                        'image_count' => 16,
                        'video_count' => 3,
                        'languages' => array('gb'),
                        'excerpt' => 'McLaren automotive partners with Cordier Mestrezat to host a joint VIP experience during Vinexpo 2013',
                    ),
                1 => (object) array(
                        'date' => new \DateTime('-1 month'),
                        'image_count' => 8,
                        'video_count' => 3,
                        'languages' => array('gb'),
                        'excerpt' => 'McLaren automotive uncovers new digital home providing the most detailed insight into the brand yet',
                    ),
                2 => (object) array(
                        'date' => new \DateTime('-2 months'),
                        'image_count' => 8,
                        'video_count' => 2,
                        'languages' => array('gb', 'de', 'fr'),
                        'excerpt' => 'McLaren M838T engine claims victory at International Engine of the Year Awards',
                    ),
            );
        return $releases;
    }


    public function getReleaseTable()
    {
        if (!$this->releaseTable) {
            $sm = $this->getServiceLocator();
            $this->releaseTable = $sm->get('ReleasesAdmin\Model\ReleaseTable');
        }
        return $this->releaseTable;
    }

    public function getDownloadsModel()
    {
        if (!$this->downloads) {
            $sm = $this->getServiceLocator();
            $this->downloads = $sm->get('DownloadsModel');
        }
        return $this->downloads;
    }

    public function getShowing($media){
        if($media->getCurrentPageNumber() == 1){
            return 1;
        }else if($media->getCurrentPageNumber() == $media->count()){
            return $media->getTotalItemCount() - $media->getCurrentItemCount() + 1;
        }else{
            return $media->getItemCountPerPage() * ($media->getCurrentPageNumber()-1) + 1;
        }
    }

    public function getShowingTo($media){
        if ($media->getCurrentPageNumber() == 1) {
            if($media->getTotalItemCount() > $media->getItemCountPerPage()){
                return $media->getItemCountPerPage();
            }
            return $media->getTotalItemCount();
        } else if($media->getCurrentPageNumber() == $media->count()){
            return $media->getTotalItemCount();
        }else{
            return $media->getItemCountPerPage() * ($media->getCurrentPageNumber());
        }
    }
}
