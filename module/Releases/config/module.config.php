<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Releases\Controller\Releases' => 'Releases\Controller\ReleasesController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'releases' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'       => '[/:locale]/releases',
                    'constraints' => array(
                        'locale' => '[a-z]{2}-[a-z]{2}|[a-z]{2}',
                    ),
                    'defaults' => array(
                        'controller' => 'Releases\Controller\Releases',
                        'action'     => 'index',
                        'locale'     => 'en-gb',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'detail' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/:id',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'action' => 'detail',
                            ),
                        ),
                    ),
                    'download' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/download[/:id]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'action' => 'download',
                            ),
                        ),
                    ),
                    'category' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    => '/category[/:id]',
                            'constraints' => array(
                                'id'    => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Releases\Controller\Releases',
                                'action'     => 'category',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    )
);
