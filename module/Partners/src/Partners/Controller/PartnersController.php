<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Partners\Controller;

use Newspress\Mvc\Controller\BaseActionController;

class PartnersController extends BaseActionController
{
    public function indexAction()
    {
        $partners = $this->getPartners();
        $browser = $this->getBrowser();

        return $this->viewModel->setVariables(array(
            'partners' => $partners,
            'browser' => $browser
        ));
    }

    public function partnerModalAction()
    {
        $stringSanitizer = $this->getServiceLocator()->get('StringSanitizer');

        $partner = $stringSanitizer->filter($this->params()->fromRoute('partner'));
        $browser = $this->getBrowser();

        $this->setFrameTemplate('layout/empty');
        $this->setLayoutTemplate('layout/empty');
        $partner = $this->getPartners($partner);
        

        return $this->viewModel->setVariables(array(
            'partner' => $partner,
            'browser' => $browser
        ));
    }

    public function getPartners($partner = false)
    {
    	$partners = (object) array(
			/*'akebono' => (object) array(
				'title' => 'Akebono',
				'content' => 'Akebono, founded in 1929, is a world leader in advanced brake and friction material development and production and operates R&D centres in Japan, the United States and France, and has wholly owned or affiliated manufacturing facilities worldwide. Akebono has been responsible for providing brake technologies for the McLaren P1™ road car while working with McLaren Mercedes. Akebono has developed special lightweight aluminium callipers for Formula 1, which can resist the high torsional loads and 800°C temperatures generated under braking, without distorting.',
				'image' => '/img/partners/akebono.svg',
			),*/
			'akzonobel' => (object) array(
				'title' => 'AkzoNobel',
				'content' => 'It began with the Sikkens brand producing innovative paint technology for the McLaren Formula 1™ car. Now AkzoNobel’s relationship with McLaren has grown to the level of Corporate and Automotive Partner. With a portfolio that includes well-known brands such as Dulux, International and Interpon, AkzoNobel supplies industries and consumers worldwide with market-leading paint and coatings solutions.',
				'image' => '/img/partners/akzonobel.',
			),
			'exxonmobil' => (object) array(
				'title' => 'ExxonMobil',
				'content' => 'ExxonMobil Lubricants and Petroleum Specialties Company is a division of Exxon Mobil Corporation and is a leading supplier of finished lubricants. Beginning its relationship with McLaren in 1995, ExxonMobil technology has contributed to over 70 Formula 1™ race wins for the McLaren Formula 1™ team and the partnership has celebrated more than 300 Grands Prix starts. Mobil 1 is the world’s leading synthetic motor oil brand and features anti-wear technology that provides performance beyond conventional motor oils. This technology allows Mobil 1 to meet or exceed the toughest standards and to provide exceptional protection against engine wear, under normal or even some of the most extreme conditions, flowing quickly in extreme temperatures to protect critical engine parts and is designed to maximise engine performance and help extend engine life. McLaren’s range of high-performance road cars and the GT models all benefit greatly from the Mobil 1 lubricants developed by ExxonMobil.',
				'image' => '/img/partners/mobil1.',
			),
			'pirelli' => (object) array(
				'title' => 'Pirelli',
				'content' => 'Although Pirelli has involvement with McLaren through its current Formula 1™ tyre supply contract, their involvement as exclusive tyre technology partner to McLaren Automotive is separate from these activities. The partnership between McLaren and Pirelli has already given, and will continue to allow, us the ability to develop bespoke high performance tyres, which operate to a very high technical level. It allows the integration of tyres into the development process which, in turn, means that tyres become an integral part of the dynamic make-up of future McLaren models. All McLaren models feature bespoke tyres developed with Pirelli.',
				'image' => '/img/partners/pirelli.',
			),
			'sap' => (object) array(
				'title' => 'SAP',
				'content' => 'As market leader in enterprise application software, SAP helps companies of all sizes and industries run better. From back office to boardroom, warehouse to storefront, desktop to mobile device – SAP empowers people and organisations to work together more efficiently and use business insight more effectively to stay ahead of the competition. SAP has a longstanding relationship with various parts of the McLaren Group, all of which are data-intensive that demand technical agility and continuous, rapid invention. With SAP HANA, mobile and the complete SAP portfolio underpinning its business, McLaren have increased intelligence across the full spectrum of its various operations, helping to better anticipate, accelerate and differentiate its business — keeping them very much in the driver’s seat. In short, working together, SAP help McLaren run like never before.',
				'image' => '/img/partners/sap.',
			),
			/*'tag-heuer' => (object) array(
				'title' => 'Tag Heuer',
				'content' => 'Founded in 1860, TAG Heuer is the world leader in high precision luxury chronographs with an unequalled mechanical accuracy of 1/10th, 1/100th, 1/1000th and even 5/10,000th of a second. TAG Heuer, in a constant quest for innovation, excellence, performance and prestige, continues to aim ever higher. This is reflected in its 29 year partnership with McLaren Racing and more recently McLaren Automotive. The innovative, high-performance and precision manufacturing principles of both TAG Heuer and McLaren have combined to produce so far the unique and limited edition TAG Heuer Carrera 12C Chronograph timepiece.',
				'image' => '/img/partners/tag-heuer.',
			),*/
		);

		if (!$partner) {
			return $partners;
		} else {
			return $partners->$partner;
		}

    }
}
