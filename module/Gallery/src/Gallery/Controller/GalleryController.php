<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Gallery\Controller;

use EventsAdmin\Model\Event;
use Newspress\Mvc\Controller\BaseActionController;
use Search\Model\Search;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use MediaAdmin\Model\Media;
use Zend\View\Model\ViewModel;

class GalleryController extends BaseActionController
{
    protected $mediaTable;
    protected $downloads;
    protected $categories;

    public function indexAction()
    {
        $pageCount  = 0;
        $totalCount = 0;
        $showing    = 0;
        $showingTo  = 0;

        $stringSanitizer = $this->getServiceLocator()->get('StringSanitizer');

        $mediaObj    = new Media();
        $queryFilter = $mediaObj->getMediaFilter($this->params());
        $select      = $mediaObj->prepareMediaQuery($queryFilter, 'media');
        $media       = $this->getMediaTable()->fetchPaginated($select);

        $itemsPerPage = 0;
        if ($media && count($media) > 0) {
            $page = $stringSanitizer->filter($this->params()->fromQuery('page', 1));
            $media->setCurrentPageNumber((int)$page);
            $media->setItemCountPerPage(24);
            $media->setPagerange(5);

            $itemsPerPage = $pageCount = $media->getItemCountPerPage();
            $totalCount = $media->getTotalItemCount();

            $showing = $this->getShowing($media);
            $showingTo = $this->getShowingTo($media);
        }

        if ($pageCount > $totalCount) {
            $pageCount = $totalCount;
        }

        $search = new Search();
        $queryFilter['displayShowAll'] = ($totalCount > $pageCount);
        $searchFilters = $search->getSearchFilters($queryFilter, array('language'));


        return $this->viewModel->setVariables(array(
            'medias'       => $media,
            'downloads'    => $this->getDownloadsModel(),
            'queryParams'  => $queryFilter['queryParams'],
            'types'        => $queryFilter['types'],
            'sort'         => $queryFilter['sort'],
            'showAll'      => $queryFilter['showAll'],
            'itemsPerPage' => $itemsPerPage,
            'pageCount'    => $pageCount,
            'totalCount'   => $totalCount,
            'showing'      => $showing,
            'showingTo'    => $showingTo,
            'browser'      => $this->getBrowser(),
            'filterHtml'   => $searchFilters['filterHtml'],
            'filterViewHtml'     => $searchFilters['filterViewHtml'],
            'filterSortByHtml'   => $searchFilters['filterSortByHtml'],
            'filterCheckboxHtml' => $searchFilters['filterCheckboxHtml'],
        ));
    }

    public function scrollLoadMediaAction()
    {
        $stringSanitizer = $this->getServiceLocator()->get('StringSanitizer');

        $source = $stringSanitizer->filter($this->params()->fromQuery('source', null));

        $mediaObj = new Media();
        if ($source == 'category') {
            $queryFilter = $mediaObj->getCategoryMediaFilter($this->params(), $this->getRequest());
            $catMedia    = $mediaObj->prepareCategoryMedia($queryFilter);
            $select      = $catMedia['select'];
        } else {
            $queryFilter = $mediaObj->getMediaFilter($this->params());
            $select = $mediaObj->prepareMediaQuery($queryFilter);
        }
        $media = $this->getMediaTable()->fetchPaginated($select);

        if ($media && count($media) > 0) {
            $page = $stringSanitizer->filter($this->params()->fromQuery('page', 1));
            $media->setCurrentPageNumber((int)$page);
            $media->setItemCountPerPage(24);
            $media->setPagerange(5);
        }

        $this->layout('');
        $view = new ViewModel();
        $view->setTerminal(true);

        return $view->setVariables(array(
            'medias'      => $media,
            'downloads'   => $this->getDownloadsModel(),
            'browser'     => $this->getBrowser()
        ));
    }

    public function landingAction()
    {
        $gallery = $this->getGallery();

        return $this->viewModel->setVariables(array(
                'gallery' => $gallery
            ));
    }

    public function embedAction()
    {
        $stringSanitizer = $this->getServiceLocator()->get('StringSanitizer');

        $this->setFrameTemplate('layout/empty');
        $this->setLayoutTemplate('layout/empty');

        $mediaId = (int) $stringSanitizer->filter($this->params()->fromRoute('id'));
        $video = new \Newspress\Asset\Video($mediaId);

        // @todo: Check to see video actually exists (and is a video)

        return $this->viewModel->setVariables(array(
                'video' => $video
            ));
    }

    public function getCategoryChildren($category)
    {
        $sql = \Newspress::db()->sql();

        $select = $sql->select();
        $select->from('categories')
               ->where('`parent` = ' . $category->id);

        $results = \Newspress::db()->execute($select, 'object');
        if (count($results) > 0) {
            foreach ($results as $result) {
                $this->categories[] = $result->id;
                $this->getCategoryChildren($result);
            }
        }
    }

    public function categoryAction()
    {
        $stringSanitizer = $this->getServiceLocator()->get('StringSanitizer');

        $mediaObj    = new Media();
        $queryFilter = $mediaObj->getCategoryMediaFilter($this->params(), $this->getRequest());
        $catMedia    = $mediaObj->prepareCategoryMedia($queryFilter);
        $select      = $catMedia['select'];
        $category    = $catMedia['category'];
        $media       = $this->getMediaTable()->fetchPaginated($select);

        if (!$select) {
            $this->redirect()->toRoute('error');
        }

        $pageCount    = 0;
        $totalCount   = 0;
        $itemsPerPage = 0;
        $showing      = 0;
        $showingTo    = 0;

        if ($media && count($media) > 0) {

            $page = $stringSanitizer->filter($this->params()->fromQuery('page', 1));
            $media->setCurrentPageNumber((int)$page);
            $media->setItemCountPerPage(24);
            $media->setPagerange(5);

            $itemsPerPage = $pageCount = $media->getItemCountPerPage();
            $totalCount = $media->getTotalItemCount();

            $showing = $this->getShowing($media);
            $showingTo = $this->getShowingTo($media);
        }

        if ($pageCount > $totalCount) {
            $pageCount = $totalCount;
        }

        return $this->viewModel->setVariables(array(
            'medias'       => $media,
            'downloads'    => $this->getDownloadsModel(),
            'category'     => $category,
            'sort'         => $queryFilter['sort'],
            'order'        => $queryFilter['order'],
            'type'         => $queryFilter['type'],
            'orderBy'      => $queryFilter['orderBy'],
            'showAll'      => $queryFilter['showAll'],
            'itemsPerPage' => $itemsPerPage,
            'showing'      => $showing,
            'showingTo'    => $showingTo,
            'pageCount'    => $pageCount,
            'totalCount'   => $totalCount,
            'browser'      => $this->getBrowser()
        ));
    }

    public function pressKitAction()
    {
        $stringSanitizer = $this->getServiceLocator()->get('StringSanitizer');

        $pressKitId = (int) $stringSanitizer->filter($this->params()->fromRoute('id'));

        $sql = \Newspress::db()->sql();

        $select = $sql->select();
        $select->from('press_kits')
               ->where
               ->equalTo('id', $pressKitId);

        $pressKit = \Newspress::db()->execute($select, 'array');

        if (count($pressKit) == 0) {
            return $this->redirect()->toRoute('gallery');
        }

        $pressKit = (object) $pressKit[0];
        $categoryId = (int) $pressKit->category_id;

        // @todo: Redirect with query params
        return $this->redirect()->toRoute('gallery/category', array('id' => $categoryId));
    }

    public function eventAction()
    {
        $stringSanitizer = $this->getServiceLocator()->get('StringSanitizer');

        $eventId = (int) $stringSanitizer->filter($this->params()->fromRoute('id'));

        $order   = $stringSanitizer->filter($this->params()->fromQuery('order', false));
        $orderBy = $stringSanitizer->filter($this->params()->fromQuery('order-by', 'asc'));

        if ($order && (!in_array($order, array('published')))) {
            $order = false;
        }
        if (!in_array($orderBy, array('asc', 'desc'))) {
            $orderBy = 'asc';
        }

        $now = new \DateTime();
        $sql = \Newspress::db()->sql();

        $select = $sql->select();
        $select->from('events')
               ->where
               ->equalTo('id', $eventId);

        $event = \Newspress::db()->execute($select, 'object');
        $event = current($event);

        $select = $sql->select();
        $select->from('object_links')
               ->where
               ->equalTo('from_object', 4)
               ->equalTo('from_object_id', $eventId)
               ->equalTo('to_object', 2);
        $select->order('sort ASC');

        $objectLinks = \Newspress::db()->execute($select);

        foreach ($objectLinks as $objectLink) {
            $objectLinksArray[] = $objectLink['to_object_id'];
        }

        if (count($objectLinksArray) == 0) {
            return $this->redirect()->toRoute('gallery');
        }

        $select = new Select('media');
        $select->where
               ->in('id', $objectLinksArray)
               ->lessThan('published', $now->format('Y-m-d H:i:s'))
               ->nest()
               ->equalTo('type', 'image')
               ->or
               ->equalTo('type', 'video')
               ->unnest()
               ->equalTo('status', 'active');

        if ($order && $order != 'relevance') {
            $select->order($order . ' ' . $orderBy);
        } else {
            $select->order(array(new Expression('FIELD (id, ' . implode(',', $objectLinksArray) . ')')));
        }

        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Media());

        $paginatorAdapter = new DbSelect(
            $select,
            $this->getMediaTable()->getTableGateway()->getAdapter(),
            $resultSetPrototype
        );

        $pagination = new Paginator($paginatorAdapter);
        $page = $stringSanitizer->filter($this->params()->fromQuery('page', 1));
        $pagination->setCurrentPageNumber((int)$page);
        $pagination->setItemCountPerPage(24);

        $pageCount = $pagination->getItemCountPerPage();
        $totalCount = $pagination->getTotalItemCount();




        if ($pageCount > $totalCount) {
            $pageCount = $totalCount;
        }

        return $this->viewModel->setVariables(array(
            'medias' => $pagination,
            'downloads' => $this->getDownloadsModel(),
            'event' => $event,
            'order' => $order,
            'orderBy' => $orderBy,
            'pageCount' => $pageCount,
            'totalCount' => $totalCount
        ));
    }

    public function videoModalAction()
    {
        $stringSanitizer = $this->getServiceLocator()->get('StringSanitizer');

        $this->setFrameTemplate('layout/empty');
        $this->setLayoutTemplate('layout/empty');

        $videoId = $stringSanitizer->filter($this->params()->fromRoute('id'));

        return $this->viewModel->setVariables(array(
            'videoId' => $videoId
        ));
    }

    public static function getGallery()
    {
        $releases = array(
                0 => (object) array(
                        'type' => 'video',
                        'title' => 'McLaren 650S',
                        'date' => new \DateTime('now'),
                        'keywords' => array('650S Coupe', 'static', 'orange', 'blue'),
                        'sizes' => array(
                                'small' => array(
                                        'dimentions' => '800 x 580',
                                        'bytes' => '650000'
                                    ),
                                'medium' => array(
                                        'dimentions' => '2421 x 1360',
                                        'bytes' => '2000000'
                                    ),
                                'large' => array(
                                        'dimentions' => '7978 x 5700',
                                        'bytes' => '10000000'
                                    ),
                            )
                    ),
                1 => (object) array(
                        'type' => 'video',
                    ),
                2 => (object) array(
                        'type' => 'youtube',
                    ),
                3 => (object) array(
                        'type' => 'video',
                    ),
                4 => (object) array(
                        'type' => 'video',
                    ),
                5 => (object) array(
                        'type' => 'video',
                    ),
                6 => (object) array(
                        'type' => 'video',
                    ),
                7 => (object) array(
                        'type' => 'video',
                    ),
                8 => (object) array(
                        'type' => 'video',
                    ),
                9 => (object) array(
                        'type' => 'video',
                    ),
                10 => (object) array(
                        'type' => 'video',
                    ),
                11 => (object) array(
                        'type' => 'video',
                    )
            );
        return $releases;
    }

    public static function getHomepageGallery($type = 'images')
    {
        if ($type == 'images') {

            $sql = \Newspress::db()->sql();
            $now = new \DateTime();

            $select = $sql->select();
            $select->from('media')
                   ->where
                   ->lessThan('published', $now->format('Y-m-d H:i:s'))
                   ->nest()
                   ->equalTo('type', 'image')
                   ->or
                   ->equalTo('type', 'video')
                   ->unnest()
                   ->equalTo('status', 'active');
            $select->order('id', 'DESC')
                   ->limit(3);

            $images = \Newspress::db()->execute($select);

        }

        $videos = array(
                3 => (object) array(
                        'type' => 'video',
                        'third_party' => 'youtube',
                        'title' => '\'The Chase\' - McLaren 650S GT3'
                    ),
                4 => (object) array(
                        'type' => 'video',
                        'title' => 'McLaren M1B at Goodwood Revival'
                    ),
                5 => (object) array(
                        'type' => 'video',
                        'third_party' => 'youtube',
                        'title' => 'Fittipaldi and the McLaren M23 reunited'
                    )
            );

        if (!isset(${$type})) {
            return false;
        }

        return ${$type};
    }

    public function getMediaTable()
    {
        if (!$this->mediaTable) {
            $sm = $this->getServiceLocator();
            $this->mediaTable = $sm->get('MediaAdmin\Model\MediaTable');
        }
        return $this->mediaTable;
    }

    public function getDownloadsModel()
    {
        if (!$this->downloads) {
            $sm = $this->getServiceLocator();
            $this->downloads = $sm->get('DownloadsModel');
        }
        return $this->downloads;
    }

    public function getShowing($media){
        if($media->getCurrentPageNumber() == 1){
            return 1;
        }else if($media->getCurrentPageNumber() == $media->count()){
            return $media->getTotalItemCount() - $media->getCurrentItemCount() + 1;
        }else{
            return $media->getItemCountPerPage() * ($media->getCurrentPageNumber()-1) + 1;
        }
    }

    public function getShowingTo($media){
        if ($media->getCurrentPageNumber() == 1) {
            if($media->getTotalItemCount() > $media->getItemCountPerPage()){
                return $media->getItemCountPerPage();
            }
            return $media->getTotalItemCount();
        } else if($media->getCurrentPageNumber() == $media->count()){
            return $media->getTotalItemCount();
        }else{
            return $media->getItemCountPerPage() * ($media->getCurrentPageNumber());
        }
    }

}
