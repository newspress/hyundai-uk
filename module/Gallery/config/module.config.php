<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Gallery\Controller\Gallery' => 'Gallery\Controller\GalleryController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'gallery' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/gallery',
                    'defaults' => array(
                        'controller' => 'Gallery\Controller\Gallery',
                        'action' => 'index'
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'landing' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/landing',
                            'defaults' => array(
                                'controller' => 'Gallery\Controller\Gallery',
                                'action' => 'landing'
                            ),
                        ),
                    ),
                    'category' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    => '/category[/:id]',
                            'constraints' => array(
                                'id'    => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Gallery\Controller\Gallery',
                                'action'     => 'category',
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'press_kit' => array(
                                'type' => 'segment',
                                'options' => array(
                                    'route'    => '/press-kits[/:id]',
                                    'constraints' => array(
                                        'id'    => '[0-9]+',
                                    ),
                                    'defaults' => array(
                                        'controller' => 'Gallery\Controller\Gallery',
                                        'action'     => 'press-kit',
                                    ),
                                ),
                            ),
                            'event' => array(
                                'type' => 'segment',
                                'options' => array(
                                    'route'    => '/events[/:id]',
                                    'constraints' => array(
                                        'id'    => '[0-9]+',
                                    ),
                                    'defaults' => array(
                                        'controller' => 'Gallery\Controller\Gallery',
                                        'action'     => 'event',
                                    ),
                                ),
                            ),
                        ),
                    ),
                    'video-modal' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    => '/video-modal[/:id]',
                            'constraints' => array(
                                'id'    => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Gallery\Controller\Gallery',
                                'action'     => 'video-modal',
                            ),
                        ),
                    ),
                    'embed' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    => '/embed[/:id]',
                            'constraints' => array(
                                'id'    => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Gallery\Controller\Gallery',
                                'action'     => 'embed',
                            ),
                        ),
                    ),
                    'scroll-load-media' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    => '/scroll-load-media[/:id]',
                            'constraints' => array(
                                'id'    => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Gallery\Controller\Gallery',
                                'action'     => 'scroll-load-media',
                            ),
                        ),
                    ),
                ),
            )
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    )
);