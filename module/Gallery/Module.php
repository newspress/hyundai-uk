<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Gallery;

use MediaAdmin\Model\Media;
use MediaAdmin\Model\MediaTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Downloads\Model\Downloads;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'MediaAdmin\Model\MediaTable' => function($sm) {
                    $tableGateway = $sm->get('MediaTableGateway');
                    $table = new MediaTable($tableGateway);
                    return $table;
                },
                'MediaTableGateway' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Media());
                    return new TableGateway('media', $dbAdapter, null, $resultSetPrototype);
                },
                'DownloadsModel' => function($sm) {
                    return new Downloads();
                },
            ),
        );
    }
}
