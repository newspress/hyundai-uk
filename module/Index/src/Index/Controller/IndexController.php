<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Index\Controller;

use Index\Model\Index;
use Newspress\Mvc\Controller\BaseActionController;
use Newspress\Mvc\Controller\CoreReleasesController;
use Newspress\Service\Elasticsearch;
use Zend\Session\Container;
use Newspress\Social\Facebook;
use Newspress\Social\Instagram;
use Newspress\Social\Twitter;

class IndexController extends BaseActionController
{
    protected $downloads;

    /**
     * Index Action
     *
     * @return object ViewModel
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    public function indexAction()
    {


        // get all media ids from ElasticSearch
        // they will be necessary for searching for the correct
        // media in the database
        $esResults = array();
        $searchParams = array();
        $esClient = new Elasticsearch();

        $searchParams['type'] = 'media';
        $searchParams['body']['size'] = 1000000;

        $response = $esClient->search($searchParams);

        foreach ($response['hits']['hits'] as $hit) {
            $esResults[$hit['_type']][] = $hit['_id'];
        }

        $sql = \Newspress::db()->sql();
        $now = new \DateTime();

        $select = $sql->select();
        $select->from('releases')
               ->where
               ->lessThan('published', $now->format('Y-m-d H:i:s'))
               ->equalTo('status', 'active');
        $select->order('published DESC')
               ->limit(3);

        $releases = \Newspress::db()->execute($select);
        $releases = CoreReleasesController::addAdditionalDataToArray($releases);

        // Get random images
        $maxImages = 3;
        $select = $sql->select();
        $select->from('media')
               ->where
               ->lessThan('published', $now->format('Y-m-d H:i:s'))
               ->equalTo('type', 'image')
               ->equalTo('status', 'active');
        if (isset($esResults['media']) && count($esResults['media']) > 0) {
            $select->where
                ->in('id', $esResults['media']);
        }
        $select->order('published DESC')
               ->limit($maxImages);

        $imageResults = \Newspress::db()->execute($select);

        $images = array();
        foreach ($imageResults as $imageResult) {
            $images[] = $imageResult;
        }

        // Get random videos
        $maxVideos = 3;
        $select = $sql->select();
        $select->from('media')
               ->where
               ->lessThan('published', $now->format('Y-m-d H:i:s'))
               ->equalTo('type', 'video')
               ->equalTo('status', 'active');
        if (isset($esResults['media']) && count($esResults['media']) > 0) {
            $select->where
                ->in('id', $esResults['media']);
        }
        $select->order('published DESC')
               ->limit($maxVideos);

        $videoResults = \Newspress::db()->execute($select);

        $videos = array();
        foreach ($videoResults as $videoResult) {
            $videos[] = $videoResult;
        }

        $mclarenAuto    = new Twitter();
        $mclarenReviews = new Twitter();
        $daveEden       = new Twitter();

        $mclarenReviews->setUsername('McLarenReviews');
        $daveEden->setUsername('daveeden');

        $data = array();
        $data = array_merge($data, $mclarenAuto->getCachedTweets(3));
        $data = array_merge($data, $mclarenReviews->getCachedTweets(3));
        $data = array_merge($data, $daveEden->getCachedTweets(3));

        $ordered = array();
        foreach ($data as $array) {
            $date = new \Newspress\Date($array['created_at']);
            $ordered[strtotime($date->format('Y-m-d H:i:s'))] = $array;
        }
        ksort($ordered);
        $ordered = array_reverse($ordered);

        $twitter = new Twitter();
        $twitter->setData($ordered);

        $partners = (object) array(
            (object) array(
                'title' => 'akzonobel',
                'img'   => '/img/partners/akzonobel',
            ),
            (object) array(
                'title' => 'exxonmobil',
                'img'   => '/img/partners/mobil1',
            ),
            (object) array(
                'title' => 'pirelli',
                'img'   => '/img/partners/pirelli',
            ),
            (object) array(
                'title' => 'sap',
                'img'   => '/img/partners/sap',
            ),
        );

        $carouselSliders = Index::getCarouselSlides();

        return $this->viewModel->setVariables(array(
            'releases'  => $releases,
            'downloads' => $this->getDownloadsModel(),
            'images'    => $images,
            'videos'    => $videos,
            'facebook'  => new Facebook(),
            'twitter'   => $twitter,
            'instagram' => new Instagram(),
            'browser'   => $this->getBrowser(),
            'partners'  => $partners,
            'carouselSliders' => $carouselSliders
        ));
    }

    public function getRandomNumbers($max)
    {
        $randomNumbers = array();

        while (true) {
            $randomNumber = rand(0, ($max - 1));
            if (!in_array($randomNumber, $randomNumbers)) {
                $randomNumbers[] = $randomNumber;
                if (count($randomNumbers) == 3) {
                    break;
                }
            }
        }

        asort($randomNumbers);

        return $randomNumbers;
    }

    /**
     * Displays the RSS feed for the website
     * 
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    public function rssAction()
    {
        $this->setFrameTemplate('layout/empty');
        $this->setLayoutTemplate('layout/empty');

        // $cache = \Newspress::cache();
        // Reader::setCache($cache);

        // $feed = Reader::importFile(__DIR__ . '/../../../../../data/feed.xml');
        // $data = $feed->saveXml();

        $data = utf8_encode(file_get_contents(__DIR__ . '/../../../../../data/feed.xml'));
        
        return $this->viewModel->setVariables(array(
                'rss' => $data
            ));
    }

    public function siteMapAction()
    {
        // $sql = \Newspress::db()->sql();

        // $select = $sql->select();
        // $select->from('categories')
        //        ->where
        //        ->equalTo('status', 'active');
        // $select->order('sort ASC')
        //        ->limit(1);

        // $categories = \Newspress::db()->execute($select);

        // foreach ($categories as $category) {
        //     var_dump($category);
        // }

        // @todo: Cache the array

        // $cache = \Newspress::cache();

        $sitemap = array(
            'Home' => array(
                'url' => '/',
                'section' => 1
            ),
            'Models' => Index::getSiteMapModels(),
            'Releases' => array(
                'url' => '/releases',
                'section' => 1
            ),
            'Press Kits' => Index::getSiteMapPressKits(),
            'Gallery' => array(
                'url' => '/gallery',
                'section' => 2
            ),
            'MSO' => array(
                'url' => '/mso',
                'section' => 2
            ),
            'McLaren GT' => array(
                'url' => '/models/mclaren-gt',
                'section' => 2
            ),
            'Events' => Index::getSiteMapEvents(),
            'Company' => array(
                array(
                    'name'   => 'Technical Partners',
                    'url'   => 'company/partners',
                    'parent' => 'Partners',
                ),
                array(
                    'name'   => 'Executive and Director Biographies',
                    'url'   => 'company/executive-director-biographies',
                    'parent' => 'People',
                ),
                array(
                    'name'   => 'History and Heritage',
                    'url'   => 'company/history-heritage',
                    'parent' => 'Company',
                ),
                array(
                    'name'   => 'A Technology Company',
                    'url'   => 'company/technology-company',
                    'parent' => 'Company',
                ),
                array(
                    'name'   => 'History of Road Cars',
                    'url'   => 'company/history-road-cars',
                    'parent' => 'Company',
                ),
                'section' => 3
            ),
        );
        $sitemap['Models']['section']     = 1;
        $sitemap['Press Kits']['section'] = 2;
        $sitemap['Events']['section']     = 2;

        return $this->viewModel->setVariables(array(
            'sitemap' => $sitemap,
        ));
    }

    /**
     * Error Action
     *
     * @return object ViewModel
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    public function errorAction()
    {
        $sql = \Newspress::db()->sql();
        $now = new \DateTime();

        $select = $sql->select();
        $select->from('releases')
               ->where
               ->lessThan('published', $now->format('Y-m-d H:i:s'))
               ->equalTo('status', 'active');
        $select->order('id DESC')
               ->limit(3);

        $releases = \Newspress::db()->execute($select);
        $releases = CoreReleasesController::addAdditionalDataToArray($releases);
        
        return $this->viewModel->setVariables(array(
            'releases' => $releases,
        ));
    }

    /**
     * Redirects action
     *
     * Get the slug from the route (this will be passed via the
     * $this->forward() plugin). Search for the route within the database
     * bearing in mind these routes will be relative.
     *
     * If the route is found or the maximum route loops have been met (which)
     * are set in the core config.ini file (which can be changed locally and
     * per environment).
     *
     * @todo   Add this to the core
     * @todo   Put all redirects into cache
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    public function redirectsAction()
    {
        $stringSanitizer = $this->getServiceLocator()->get('StringSanitizer');

        $redirectSession = new Container('redirects');

        if (!isset($redirectSession->count)) {
            $redirectSession->count = 0;
        }

        $slug = '/' . $stringSanitizer->filter($this->params()->fromRoute('slug'));

        $sql = \Newspress::db()->sql();

        $select = $sql->select();
        $select->from('redirects')
               ->where
               ->equalTo('route_from', $slug);
        $select->order('id DESC')
               ->limit(1);

        $results = \Newspress::db()->execute($select);

        if ($results->count() == 0 || $redirectSession->count > (int) \Newspress::config()->redirects->max) {
            // Reset the redirects count
            $redirectSession->getManager()->getStorage()->clear('redirects');

            // Should we be changing the URL in the page?
            return $this->redirect()->toRoute('error');
        }

        $results = $results->current();
        $route_to = $results['route_to'];

        if ($slug == $route_to) {
            // Redirect matches current route (kill this redirect)
            \Newspress\Debug::logFramework('Redirect conflict', 'The redirect: ' . $slug . ' returned a conflict in the database');

            // Redirect to error page
            return $this->redirect()->toRoute('error');
        }

        // Log the redirect
        $redirectSession->count++;
        \Newspress\Debug::logFramework('Redirect', 'Redirecting from route: ' . $slug . ' to route: ' . $route_to);

        // Redirect to the URL specified in the database
        return $this->redirect()->toUrl($route_to);
    }

    public function getDownloadsModel()
    {
        if (!$this->downloads) {
            $sm = $this->getServiceLocator();
            $this->downloads = $sm->get('DownloadsModel');
        }
        return $this->downloads;
    }
}
