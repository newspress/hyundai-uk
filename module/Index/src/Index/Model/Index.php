<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

namespace Index\Model;

use CategoriesAdmin\Model\Helper;
use Zend\Db\Sql\Expression;

class Index
{
    const JOIN_INNER = 'inner';

	public static function strtoupper($string)
	{
		return \Newspress\String::uppercaseApartFromDefined($string, 'McLAREN');
	}

    public static function getNavigationModels()
    {
        $categoryHelper = new Helper(1);
        $categoryHelper->setShowInNav();

        return $categoryHelper->getCategoriesArray();
    }

    public static function getSiteMapModels() {
        $sql = \Newspress::db()->sql();

        $select = $sql->select()
            ->from(array('model' => 'categories'))
            ->join(array('series' => 'categories'),
                'model.parent = series.id',
                array(
                    'parent' => 'name',
                    'parent_url' => new Expression("CONCAT('/models/',series.slug)")
                ),
                self::JOIN_INNER)
            ->join(array('models' => 'categories'),
                'series.name = models.name',
                array(),
                self::JOIN_INNER);

        $select->columns(array(
            'name',
            'url' => new Expression("CONCAT('/models/',model.slug)")
        ));

        $select->where
            ->equalTo('models.parent', 1)
            ->equalTo('model.status', 'active')
            ->equalTo('series.status', 'active')
            ->equalTo('models.status', 'active')
            ->notEqualTo('series.id', 95);

        $select->order('series.sort, series.name, model.name');

        return \Newspress::db()->execute($select, 'array');
    }

    public static function getSiteMapPressKits() {
        $sql = \Newspress::db()->sql();

        $select = $sql->select(array('pk' => 'press_kits'))

            ->join(array('model' => 'categories'),
                'pk.category_id = model.id',
                array(),
                self::JOIN_INNER)

            ->join(array('series' => 'categories'),
                'model.parent = series.id',
                array('parent' => 'name'),
                self::JOIN_INNER);

        $select->columns(array(
            'name' => 'title',
            'url' => new Expression("CONCAT('/press-kits/',pk.slug)")
        ));

        $select->where
            ->equalTo('pk.status', 'active')
            ->equalTo('series.parent', 1)
            ->notEqualTo('pk.slug', '');

        $select->order('series.sort, series.name, model.name');

        return \Newspress::db()->execute($select, 'array');
    }

    public static function getSiteMapEvents() {
        $sql = \Newspress::db()->sql();

        $select = $sql->select('events');

        $select->columns(array(
            'name' => 'title',
            'url' => new Expression("CONCAT('/events/',slug)"),
            'parent' => new Expression("SUBSTRING(slug, -4)")
        ));

        $select->where
            ->equalTo('status', 'active');

        $select->order('parent DESC, title');

        return \Newspress::db()->execute($select, 'array');
    }

    public static function getCarouselSlides(){


        $now = new \DateTime();
        $now = $now->format('Y-m-d H:i:s');

        $sql = \Newspress::db()->sql();

        $select = $sql->select('carousel');

        $select->columns(array(
            'title',
            'link',
            'media_id'
        ));

        $select->where
            ->equalTo('status', 1)
            ->lessThan('publish', $now)
            ->greaterThan('unpublish', $now);

        $select->order(array('sort', 'publish DESC'));
        $select->limit(3);

        $results = \Newspress::db()->execute($select, 'array');


        if(count($results) < 3){

            $now = new \DateTime();
            $now = $now->format('Y-m-d H:i:s');
            $sql = \Newspress::db()->sql();
            $select = $sql->select('carousel');
            $select->columns(array(
                'title',
                'link',
                'media_id'
            ));
            $select->where
                ->isNull('status')
                ->lessThan('publish', $now);
            $select->order('publish DESC');
            $inactiveSlides = \Newspress::db()->execute($select, 'array');

            foreach($inactiveSlides as $slider){

                    if(count($results) < 3){
                        array_push($results,$slider);
                    }else{
                        return $results;
                    }
                }
        }

        return $results;
    }


    public static function getModelsMenu(){
        $sql = \Newspress::db()->sql();

        $select = $sql->select(array( 'OL' => 'object_links'));

        $select->columns(array(
            'parent_id' => new Expression("OL.from_object_id"),
            '_parent' => new Expression("(select name from categories where OL.from_object_id = categories.id )"),
            'slug' => new Expression("CAT.slug"),
            'title' => new Expression("CAT.name"),
            'dropdown_image_id' => new Expression("CAT.dropdown"),
            'col_order' => new Expression("(select column_order from categories where OL.from_object_id = categories.id )"),
            'parent_sort' => new Expression("(select sort from categories where OL.from_object_id = categories.id )"),
        ));

        $select->join(array('CAT' => 'categories'),
            'CAT.id = OL.to_object_id');

        $select->where->equalTo('from_object', 10);
        $select->where->equalTo('to_object', 6);
        $select->where->notEqualTo('from_object_id', 1);
        $select->where->equalTo('CAT.status', 'active');
        $select->where->notEqualTo('CAT.parent', 120);
        $select->where->lessThan('CAT.published', (new \DateTime())->format('Y-m-d H:i:s'));

        $select->order(array('parent_sort', 'CAT.sort'));



        $cache = \Newspress::cache();
        $key = 'query_' . md5(\Newspress::db()->getSqlString($select));
        $cache->setItem($key, null);

        $results= \Newspress::db()->execute($select, 'array');

        $data = array();

        foreach($results as $key => $result) {
            $data[$result['_parent']]['results'][] = $result;

            if( isset($result['col_order']) ){
                $data[$result['_parent']]['column_order'] = $result['col_order'];
            }else{
                $data[$result['_parent']]['column_order'] = 0;
            }
        }

        return $data;
    }

    public static function getPressKitMenu(){
        $sql = \Newspress::db()->sql();

        $select = $sql->select(array( 'OL' => 'object_links'));

        $select->columns(array(
            'parent_id' => new Expression("OL.from_object_id"),
            'parent' => new Expression("(select name from categories where OL.from_object_id = categories.id )"),
            'slug' => new Expression("PK.slug"),
            'title' => new Expression("PK.title"),
            'dropdown_image_id' => new Expression("PK.dropdown"),
            'col_order' => new Expression("(select column_order from categories where OL.from_object_id = categories.id )"),
            'parent_sort' => new Expression("(select sort from categories where OL.from_object_id = categories.id )")
        ));

        $select->join(array('PK' => 'press_kits'),
            'PK.id = OL.to_object_id');

        $select->where->equalTo('from_object', 10);
        $select->where->equalTo('to_object', 3);
        $select->where->equalTo('status', 'active');
        $select->where->lessThan('PK.published', (new \DateTime())->format('Y-m-d H:i:s'));

        $select->order(array('parent_sort ASC', 'PK.sort '));

        $cache = \Newspress::cache();
        $key = 'query_' . md5(\Newspress::db()->getSqlString($select));
        $cache->setItem($key, null);

        $results= \Newspress::db()->execute($select, 'array');

        $data = array();

        foreach($results as $key => $result) {
            $data[$result['parent']]['results'][] = $result;

            if( isset($result['col_order']) ){
                $data[$result['parent']]['column_order'] = $result['col_order'];
            }else{
                $data[$result['parent']]['column_order'] = 0;
            }
        }

//        echo '<pre>';
//        var_dump($data);
//        exit;

        return $data;
    }

    public static function getEventMenu(){


        $sql = \Newspress::db()->sql();

        $select = $sql->select(array( 'OL' => 'object_links'));

        $select->columns(array(
            'parent' => new Expression("(select name from categories where OL.from_object_id = categories.id )"),
            'slug' => new Expression("EV.slug"),
            'title' => new Expression("EV.title"),
            'parent_sort' => new Expression("(select sort from categories where OL.from_object_id = categories.id )"),
            'col_order' => new Expression("(select column_order from categories where OL.from_object_id = categories.id )")
        ));

        $select->join(array('EV' => 'events'),
            'EV.id = OL.to_object_id');

        $select->where->equalTo('from_object', 10);
        $select->where->equalTo('to_object', 4);
        $select->where->equalTo('status', 'active');
        $select->where->notEqualTo('from_object_id',0);
        $select->where->lessThan('EV.published', (new \DateTime())->format('Y-m-d H:i:s'));

        $select->order(array('parent_sort ASC', 'EV.sort'));

        $cache = \Newspress::cache();
        $key = 'query_' . md5(\Newspress::db()->getSqlString($select));
        $cache->setItem($key, null);

        $results= \Newspress::db()->execute($select, 'array');

        $data = array();

        foreach($results as $key => $result) {
            $data[$result['parent']]['results'][] = $result;

            if( isset($result['col_order']) ){
                $data[$result['parent']]['column_order'] = $result['col_order'];
            }else{
                $data[$result['parent']]['column_order'] = 0;
            }
        }

        return $data;

    }

    public static function getCompanyMenu(){
        $sql = \Newspress::db()->sql();

        $select = $sql->select(array( 'OL' => 'object_links'));

        $select->columns(array(
            'parent' => new Expression("(select name from categories where OL.from_object_id = categories.id )"),
            'slug' => new Expression("CON.slug"),
            'title' => new Expression("CON.title"),
            'col_order' => new Expression("(select column_order from categories where OL.from_object_id = categories.id )"),
            'parent_sort' => new Expression("(select sort from categories where OL.from_object_id = categories.id )"),
            'con_sort' => new Expression("CON.sort"),
        ));

        $select->join(array('CON' => 'content'),
            'CON.id = OL.to_object_id');

        $select->where->equalTo('from_object', 10);
        $select->where->equalTo('to_object', 11);
        $select->where->equalTo('status', 'active');
        $select->where->notEqualTo('from_object_id',0);
        $select->where->lessThan('CON.published', (new \DateTime())->format('Y-m-d H:i:s'));

        $select->order(array('parent_sort ASC', 'con_sort'));

        $cache = \Newspress::cache();
        $key = 'query_' . md5(\Newspress::db()->getSqlString($select));
        $cache->setItem($key, null);

        $results= \Newspress::db()->execute($select, 'array');

        $data = array();

        foreach($results as $key => $result) {
            $data[$result['parent']]['results'][] = $result;

            if( isset($result['col_order']) ){
                $data[$result['parent']]['column_order'] = $result['col_order'];
            }else{
                $data[$result['parent']]['column_order'] = 0;
            }
        }

        return $data;
    }
}
