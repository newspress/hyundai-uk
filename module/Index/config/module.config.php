<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Index\Controller\Index' => 'Index\Controller\IndexController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'index' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/index',
                    'defaults' => array(
                        'controller' => 'Index\Controller\Index',
                        'action' => 'index'
                    )
                )
            ),
            'error' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/page-not-found',
                    'defaults' => array(
                        'controller' => 'Index\Controller\Index',
                        'action' => 'error'
                    )
                )
            ),
            'rss' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/rss',
                    'defaults' => array(
                        'controller' => 'Index\Controller\Index',
                        'action' => 'rss'
                    )
                )
            ),
            'site_map' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/site-map',
                    'defaults' => array(
                        'controller' => 'Index\Controller\Index',
                        'action' => 'site-map'
                    )
                )
            )
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    )
);
