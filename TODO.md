Todo items to improve the system
--------------------------------

    - Change .ini files to PHP arrays (as part of the new ZF2 configuration)
    - Relocate core controllers and core admin controllers to its own namespace
      or relocate in their own file \Newspress\Controllers\CoreReleasesController
    - Think about using bower for downloading the latest versions of 3rd party
      libraries for the front end.
    - Change the /scripts files to become part of the controllers and use ZF2s
      console routing.
    - Add core and skeleton application to a private composer repository:
      https://getcomposer.org/doc/05-repositories.md#hosting-your-own


Outstanding coding todo items
-----------------------------

To get the latest @todo items, use `ack`:

    ack '\@todo' --ignore-dir={vendor,core/Newspress/library}

Results from the above command as of 24 April 2015:

    core/Newspress/module/Application/src/Application/Controller/ApplicationController.php
    43:         // @todo: Change this to \Newspress::setLocale(); so we don't override the locale container

    core/Newspress/module/Application/src/Application/Model/Object.php
    56:     * @todo   Move the `locale` column betweeen toObjectId and sort on the production
    87:        // @todo: Do we really need to run another two queries here?
    253:     * @todo   Do we want to cache these results?

    core/Newspress/module/CategoriesAdmin/src/CategoriesAdmin/Model/CategoryTable.php
    21:  * @todo   Get the data array from the getArrayCopy() method

    core/Newspress/module/CategoriesAdmin/src/CategoriesAdmin/Model/Helper.php
    33:     * @todo  Add functionality for $categoryId
    34:     * @todo  Add caching functionality

    core/Newspress/module/ContentAdmin/src/ContentAdmin/Form/ContentForm.php
    18:     // @todo: Change this to use the Newspress form and pass the class name

    core/Newspress/module/ContentAdmin/src/ContentAdmin/Model/ContentTable.php
    21:  * @todo   Get the data array from the getArrayCopy() method
    36:         // 'author'    => $content->author, // @todo: Set the author here

    core/Newspress/module/MediaAdmin/src/MediaAdmin/Controller/MediaAdminController.php
    139:                // @todo: Relocate this to the media object (create additionalData() method)
    140:                // @todo: Replace this query by using the Object class
    355:                    // @todo: Test to see if we can apply multiple crops

    core/Newspress/module/MediaAdmin/src/MediaAdmin/Model/MediaTable.php
    21:  * @todo   Get the data array from the getArrayCopy() method
    40:         'author'      => $media->author, // @todo: Set the author here

    core/Newspress/module/MediaAdmin/view/media-admin/media-admin/upload.phtml
    53:    // @todo: Change this so it doesn't include /admin (use a global variable)

    core/Newspress/module/ModelsAdmin/src/ModelsAdmin/Form/ModelForm.php
    18:     // @todo: Change this to use the Newspress form and pass the class name

    core/Newspress/module/ModelsAdmin/src/ModelsAdmin/Model/ModelTable.php
    21:  * @todo   Get the data array from the getArrayCopy() method
    38:         // 'author'      => $model->author, // @todo: Set the author here

    core/Newspress/module/PacksAdmin/src/PacksAdmin/Model/PackTable.php
    21:  * @todo   Get the data array from the getArrayCopy() method

    core/Newspress/module/PressKitsAdmin/src/PressKitsAdmin/Form/PressKitForm.php
    18:     // @todo: Change this to use the Newspress form and pass the class name

    core/Newspress/module/PressKitsAdmin/src/PressKitsAdmin/Model/PressKitTable.php
    21:  * @todo   Get the data array from the getArrayCopy() method
    39:         // 'author'      => $pressKit->author, // @todo: Set the author here

    core/Newspress/module/ReleasesAdmin/src/ReleasesAdmin/Model/ReleaseTable.php
    21:  * @todo   Get the data array from the getArrayCopy() method
    37:         'author'    => $release->author, // @todo: Set the author here

    core/Newspress/module/TranslationsAdmin/src/TranslationsAdmin/Model/TranslationTable.php
    21:  * @todo   Get the data array from the getArrayCopy() method

    core/Newspress/module/Users/src/Users/Controller/CoreUserController.php
    37:     * @todo Add translations for error messages
    468:     * @todo Put this into abstract class

    core/Newspress/module/Users/src/Users/Model/UsersTable.php
    33:     * @todo Put this somewhere nicer so it can be extended on all models

    core/Newspress/module/UsersAdmin/src/UsersAdmin/Model/UserTable.php
    21:  * @todo   Get the data array from the getArrayCopy() method

    core/Newspress/resource/javascript/global.js
    92:     * @todo   Change selector to class name
    93:     * @todo   Fix bug when tinymce isn't availabe as an object
    94:     * @todo   Finish styling
    95:     * @todo   Add content_css parameter for core and local
    96:     * @todo   Use best option for paste_as_text/oninit:'setPlainText'
    116:     * @todo   When adding more than one
    168:     * @todo   When adding more than one
    544:     * @todo   This doesn't stop any existing events
    615:                // @todo: Show the user this has saved

    core/Newspress/resource/sass/includes/_global.scss
    117:// @todo: Clear this up

    core/Newspress/resource/sass/includes/_grids.scss
    6: * @todo: Move these variables into _variables.scss to they can be inherited

    core/Newspress.php
    40:  * @todo   Test this at the command line as this doesn't seem to be caching
    62:        //     // @todo: Create/configure the wrapper for this extension
    ack: data/cache/zfcache-bd: Permission denied

    module/Content/view/content/content/index.phtml
    14:     <div class="grid__item <?= ($images || $videos || $documents) ? 'two-thirds l-two-thirds' : 'full l-full'; // @todo: Add languages ?> m-full s-full">
    21:     <? if ($images || $videos || $documents): // @todo: Add languages ?>

    module/Downloads/src/Downloads/Model/Downloads.php
    25:     * @todo   At the moment these get saved to session. Enable core
    29:     * @todo   Move this model to a core model
    319:        // @todo: Check file doesn't already exist

    module/Events/src/Events/Controller/EventsController.php
    64:            // @todo: Work out where this should redirect if the client does not want a models page

    module/Events/view/events/events/detail.phtml
    51:         <div class="grid__item <?= ($images || $videos || $documents) ? 'two-thirds l-two-thirds' : 'full l-full'; // @todo: Add languages ?> m-full s-full">
    89:         <? if ($images || $videos || $documents): // @todo: Add languages ?>

    module/Gallery/src/Gallery/Controller/GalleryController.php
    140:            // @todo: Add pagination to Elasticsearch to speed up queries
    215:        // @todo: Check to see video actually exists (and is a video)
    374:        // @todo: Redirect with query params

    module/Index/src/Index/Controller/IndexController.php
    142:        // @todo: Cache the array
    261:     * @todo   Add this to the core
    262:     * @todo   Put all redirects into cache

    module/Models/src/Models/Controller/ModelsController.php
    98:            // @todo: Work out where this should redirect if the client does not want a models page
    293:            // @todo: Remove this once the images for the Pitwall have been fixed

    module/Models/view/models/models/detail.phtml
    89:         <? if ($images || $videos || $documents): // @todo: Add languages ?>

    module/PressKits/src/PressKits/Controller/PressKitsController.php
    75:            // @todo: Make sure this doesn't redirect more than once
    93:            // @todo: Work out where this should redirect if the client does not want a models page

    module/Releases/src/Releases/Controller/ReleasesController.php
    190:        // @todo: Add flash message saying release does not exist
    215:            // @todo: Make sure this doesn't redirect more than once
    387:        // @todo: Add flash message saying release does not exist

    module/Search/view/search/search/index.phtml
    70: <? // @todo: Add pagination ?>

    resource/gulpfile.js
    61: * @todo: Add our sprite configuration as a variable so we don't repeat code
    132: * @todo   Add streaming
    163: * @todo   Add streaming
    365: * @todo: Validate HTML
    366: * @todo: Validate XHTML for email templates
    400: * @todo   Make update for html validation to ignore PHP
    416: * @todo   Make update for html validation to ignore PHP

    resource/javascript/global.js
    28:  * @todo Think of a name for me
    121:     * @todo   Add styling for download partial when at one-quarter
    200:     * @todo   Add last navigationIsFixed to check before running the function
    253:    // @todo: If the window is resized after the page is loaded (from mobile) the
    396:     * @todo   Remove the event.preventDefault() function (add core JS)
    397:     * @todo   Fix issue with height being 100% when scrollTop is past 0
    412:     * @todo   Consider removig this from global JS file
    423:     * @todo   Change image function to take from data attribute
    424:     * @todo   Change the selector from an a tag to an li
    711:     * @todo   add actual download action!
    862:     * @todo   Update user feedback
    943:     * @todo   Update user feedback

    resource/sass/style.scss
    1545:        margin-bottom: 430px; // @todo: Sort the height of the media download container
    1581:        // @todo: Issue with page being wider than needed. Fix with: html, body { overflow-x: hidden; }

    scripts/attachPdfsToPacks.php
    16: * @todo Add functionality to see documents for a specific locale

    scripts/attachPdfsToReleases.php
    16: * @todo Add functionality to see documents for a specific locale

    scripts/backupDatabase.php
    16: * @todo Remove older backups

    scripts/import.php
    22:// @todo: Add warning to this script as it should only be used once (before any data has been added)
    26:// @todo: Add start time and timer, roughly work out an estimated time for each section?
    27:// @todo: Upon counting, remove one so the last image that failed can be re-uploaded
    167:        // \Newspress::db()->truncate('media'); // @todo: Think about... Hmmmm...
    174:// @todo: Create image indexed database with links to releases (once you've done that, truncate it here)
    180: * @todo Change this to only include images
    498:     * @todo: Update this to use object_links
    596:     * @todo: Import video category_links
    691:     * @todo Update this to use object_lnks
    804:            // @todo: Get the image from Rackspace using the ID and/or other data
    852:            // @todo: Add additional crops if needed (which we will need eventually)
    1168: * @todo Fix issue with encoding
    1225:                    'slug'      => null, // @todo: Create slug (and check slug doesn't exist)
    1256:            // @todo: Get any videos and documents relating to the release
    1265:            $releaseObjectId = 1; // @todo: Get this from the database (but that's another query...)
    1308:                // @todo: Do not include english translations (update the main releases table)
    1339:                            // @todo: Maybe change the key $localeId to the text version?
    1387:     * @todo: Create links between documents and releases
    1394:           ->equalTo('lid', 1) // @todo: Add documents for different locales
    1437:     * @todo: Create links between documents and releases

    scripts/index.php
    219:                        // @todo: Maybe change the key $localeId to the text version?

    scripts/updateMediaLinks.php
    15: * @todo Add elasticsearch indexing update to this script