<?php
/**
 * Newspress Ltd (http://www.newspress.co.uk)
 *
 * @link      http://www.newspress.co.uk
 * @copyright Copyright (c) 2015 Newspress Ltd (http://www.newspress.co.uk)
 * @license   http://www.newspress.co.uk/license License
 */

/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
    'db' => array(
        'driver'         => 'Pdo',
        'dsn'            => 'mysql:dbname=' . \Newspress::config()->database->name . ';host=' . \Newspress::config()->database->host,
        'username'       => \Newspress::config()->database->username,
        'password'       => \Newspress::config()->database->password,
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        )
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => function($serviceManager) {
                if (\Newspress::isDevelopment()) {

                    $adapter = new BjyProfiler\Db\Adapter\ProfilingAdapter(array(
                        'driver'    => 'Pdo',
                        'dsn'       => 'mysql:dbname=' . \Newspress::config()->database->name . ';host=' . \Newspress::config()->database->host,
                        'database'  => \Newspress::config()->database->name,
                        'username'  => \Newspress::config()->database->username,
                        'password'  => \Newspress::config()->database->password,
                        'hostname'  => \Newspress::config()->database->host
                    ));

                    if (\Newspress\Cli::isCli()) {
                        $logger = new Zend\Log\Logger();
                        $writer = new Zend\Log\Writer\Stream('php://output');
                        $logger->addWriter($writer, Zend\Log\Logger::DEBUG);
                        $adapter->setProfiler(new BjyProfiler\Db\Profiler\LoggingProfiler($logger));
                    } else {
                        $adapter->setProfiler(new BjyProfiler\Db\Profiler\Profiler());
                    }

                    if (isset($databaseParameters['options']) && is_array($databaseParameters['options'])) {
                        $options = $databaseParameters['options'];
                    } else {
                        $options = array();
                    }

                    $adapter->injectProfilingStatementPrototype($options);

                } else {
                    $adapterFactory = new \Zend\Db\Adapter\AdapterServiceFactory();
                    $adapter = $adapterFactory->createService($serviceManager);
                }

                \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::setStaticAdapter($adapter);
                return $adapter;
            },
        ),
    ),
);
