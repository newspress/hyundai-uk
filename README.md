Newspress: Core Framework
=========================

Introduction
------------
This is the core application for Skeleton that binds with the core repository
that will be used across all media websites. It is a custom framework with a
core repository built on top of Zend Framework 2.

Installation
------------

Install with Git and Composer (recommended)
-------------------------------------------
Clone the Git repository using:

    cd ~/Sites/skeleton
    git clone git@bitbucket.org:newspress/skeleton.git --recursive .

Then, install libraries and generate the autoloader via composer:

    curl -sS https://getcomposer.org/installer | php
    php composer.phar install


Elastic Installation (indexing)
-------------------------------

### Install Elasticsearch locally

To install elastic (formally named elasticsearch), simply use homebrew:

    brew install elasticsearch

After installation, set up elasticsearch head on your localhost:

    https://github.com/mobz/elasticsearch-head

Finally, enable CORS headers for your localhost to connect to elasticsearch.
Here's a one-liner:

    echo "http.cors.enabled: true" >> /usr/local/etc/elasticsearch/elasticsearch.yml

CREATE THE INDEX curl -XPUT 'http://localhost:9200/skeleton/'

Once set up, you will need to index what is in your database (if anything). On
a new website, you would run the import script using:

    php scripts/import.php development

But on an existing site, you will just need to reindex:

    php scripts/index.php development


### Install Elasticsearch on EC2

    http://www.elastic.co/guide/en/elasticsearch/reference/current/setup-repositories.html#_yum


Gulp Setup and Installation
---------------------------

### Install node.js

To install node.js, simply download the package from their website:
http://nodejs.org/download/

### Homebrew gulp installation

The easiest way to get started with npm is to use our package.json file that
has been automatically generated to install all the dependencies we need:

    brew install node
    sudo gem install sass
    cd resource
    sudo npm install -g gulp
    sudo npm install

Once Node.js and npm has been installed, change the directory to resources and
run `gulp`. This will automatically generate sprites, compress images, validate
CSS and JavaScript and compiles Sass stylesheets which saved as .scss files.
See this answer from StackOverflow to see the difference between .sass and
.scss: http://stackoverflow.com/a/5654471/1421836

To run gulp and complete the above, the commands are as follows:

    cd resource
    gulp

After Gulp has finished running the default task, it will carry on a watch task
which looks out for any changes in files. Compiled files will end up in the
public directory.


### Run composer to set the autoload configuration


Zend Framework 2: Server Setup
==============================

Web Server Setup
----------------

### PHP CLI Server

The simplest way to get started if you are using PHP 5.4 or above is to start
the internal PHP cli-server in the root directory:

    php -S 0.0.0.0:8080 -t public/ public/index.php

This will start the cli-server on port 8080, and bind it to all network
interfaces.

**Note: ** The built-in CLI server is *for development only*.

### Apache Setup

To setup apache, setup a virtual host to point to the public/ directory of the
project and ensure the alias is set up for the core front end files. It should
look something like below:

    <VirtualHost *:80>
        ServerAdmin webmaster@localhost
        DocumentRoot /path/to/handle/environment/public
        AliasMatch ^/core/(.*)$ /path/to/handle/environment/core/Newspress/public/$1
        ServerName handle.com
        ServerAlias handle.environment.newspress.co.uk
        ErrorLog logs/handle.environment.newspress.co.uk-error_log
        CustomLog logs/handle.environment.newspress.co.uk-access_log common
        <Directory />
            Options FollowSymLinks
            AllowOverride None
        </Directory>
        <Directory /var/www/handle/environment/public>
            Options Indexes FollowSymLinks MultiViews
            AllowOverride All
            Order allow,deny
            allow from all
            Require all granted
        </Directory>
    </VirtualHost>

### Nginx Setup

To setup nginx, setup a virtual host to point to the public/ directory of the
project and you should be ready to go! It should look something like below:

    server {
        listen 80;
        server_name skeleton.local;
        client_max_body_size 500M;
        root /path/to/skeleton/public;
        index index.php;
        location / {
            try_files $uri $uri/ /index.php?$query_string;
        }
        location ~ ^/core/(.*)$ {
            alias /path/to/skeleton/core/Newspress/public/$1;
        }
        location ~ \.php$ {
        include fastcgi.conf;
            fastcgi_pass 127.0.0.1:9000;
        }
        location ~ \.htaccess {
            deny all;
        }
    }


New Website Installation
------------------------

### Amazon EC2 Setup

To setup a new website using the current core system, we can use a new server
or an existing server that holds another website using the core system. This
system is hosted independantly and does not rely on other websites being
available as the only connection is the core repository, which can also be
updated seperately.

Using the current EC2 instance as an example, we can set up a new website very
easily. Using an existing website (or ideally a skeleton repository build for
specifically this purpose, which will be a bare boned website with all the
necessaries included), we can use this for cloning and then change the remote
repository to our new one we will setup in version control.

Please note, the name 'handle' will be our new website handle name and the EC2
username and IP address used in this example are only placeholders.

    ssh ec2-user@52.16.91.157
    cd /var/www/
    mkdir -p handle/production
    cd handle/production

Once these folders have been set up, we can install our website (again, ideally
using our skeleton repository, but as this has not been created yet, we will
use the Skeleton website as an example).

    cd /var/www/handle/production
    git clone git@bitbucket.org:newspress/skeleton.git --recursive .

Using `git remote -v` we can see our current remote repository, which we need
to change. Firstly, we need to setup a new repository in version control.
Ensure you enable HipChat notifications (this is only applicable to BitBucket,
otherwise find a tutorial online for GitHub or another version control host)
to see all commits by enabling them when creating the repository, then going
into Settings, and under Intergrations you will find a link to 'HipChat
Intergration'. Click on that, select the 'Dev' room and click 'Add'.

Once setup, we will have a repository SSH URL. We will use this to replace the
existing remote repository.

    git remote remove origin
    git remote add origin git@bitbucket.org:newspress/citroen.git
    git push -u origin --all
    git push -u origin --tags

Then, install libraries and generate the autoloader via composer:

    curl -sS https://getcomposer.org/installer | php
    php composer.phar install

At this point, we now have a new website available in version control and the
code base on the server is now set up. It would be a good time to set up our
database and change any connection details in the `config/config.ini` file
locally, ensure they are working and commit them. If we are setting up a local
database rather than using RDS, simply change the hostname in the config to
localhost.

Finally, we will need to set up the virtual host records. Follow the 'Web
Server Setup' in this readme document to find virtual host records for both
Apache and Nginx.

    sudo vi /etc/httpd/conf.d/vhosts.conf

And appending the following virtual host record (changing the relevant bits):

    <VirtualHost *:80>
        ServerAdmin webmaster@localhost
        DocumentRoot /path/to/handle/environment/public
        AliasMatch ^/core/(.*)$ /path/to/handle/environment/core/Newspress/public/$1
        ServerName handle.com
        ServerAlias handle.environment.newspress.co.uk
        ErrorLog logs/handle.environment.newspress.co.uk-error_log
        CustomLog logs/handle.environment.newspress.co.uk-access_log common
        <Directory />
            Options FollowSymLinks
            AllowOverride None
        </Directory>
        <Directory /var/www/handle/environment/public>
            Options Indexes FollowSymLinks MultiViews
            AllowOverride All
            Order allow,deny
            allow from all
            Require all granted
        </Directory>
    </VirtualHost>

Once saved, we need to restart Apache (or Nginx) to ensure this change has been
applied. To ensure no errors were made, test the configuration:

    sudo apachectl -t

The same can be done in Nginx:

    sudo nginx -t

Apache should spit something out similar to:

    Syntax OK

And Nginx should spit out something similar to:

    nginx: the configuration file /usr/local/etc/nginx/nginx.conf syntax is ok
    nginx: configuration file /usr/local/etc/nginx/nginx.conf test is successful

If all if okay, we can go ahead and restart Apache:

    sudo apachectl restart

Or Nginx:

    sudo nginx -s reload

After this has been completed, go ahead and set up your A records for
production and, if needed, staging. Where the domain name structure will be:

    {handle}.{environment}.newspress.co.uk => {IP address of the server}

For example:

    handle.production.newspress.co.uk => 52.16.91.157

In Rackspace Cloud DNS (or whichever DNS system you are using, such as Amazon
Route 53), you can set the Time To Live (TTL) to a lower value such as 500.
This will enable your sub domain to become available quickly. You can check to
see if your new sub domain is availabe via the command line by typing:

    host handle.environment.newspress.co.uk

If the host is available, you should see something like this:

    handle.environment.newspress.co.uk has address 52.16.91.157

If the hosting is not available yet, you will see something like:

    Host handle.environment.newspress.co.uk not found: 3(NXDOMAIN)

In which case, just wait a few more minutes until it becomes available.

Once the host is available, you will be able to access your website via the sub
domain you have set up. Simply go to handle.environment.newspress.co.uk to see
your new working website.

Finally, you need to change one last thing to ensure the website can use assets
quickly and easily. You will need to set up a new bucket via Amazon S3 and new
credentials. Change these in the `config/config.ini` file.

A few notes to ensure we have covered everything:

* Virtual host setup
* Website installation (codebase and composer files)
* Version control setup (changing the remote origin)
* Database setup (and changing credentials in the config)
* DNS setup
* Amazon S3 bucket setup (and changing S3 credentials in the config)

Possible Errors and Fixes
-------------------------

ERROR - Unexpected end of file
FIX   - Set short_open_tag = On in php.ini (Possible location /etc/php-5.5.ini)

ERROR - Can't find Locale Class
FIX   - Instal intl extension 


During Development Notes
------------------------

### DocBlocks

Where necessary, use DocBlocks for functions within classes. Please ensure to
use the `@return` and `@author` tags like so:

    /**
     * Description of method
     *
     * @return void
     * @author Oliver Tappin <oliver.tappin@newspress.co.uk>
     */
    public function someMethod()
    {
        // Beautiful code goes here...
    }


### Browser Support

When adding something that is fairly new, add a comment with a browserSupport
tag (whether it be in PHP, JavaScript or Sass defining the technology used that
can be queried at caniuse.com - example below as a PHP comment:

    // @browserSupport: flexbox


### Committing Core Changes

    cd core
    git add <file>
    git commit -m "Commit message"
    git push origin master

    # You have now committed and pushed to the core repository which will be
    # available across all media websites. Media websites will require their
    # repsitories to be updated before they see the new changes. You can update
    # the local core file to show it is up to date although this is not
    # required or necessary:

    cd ../
    git add core
    git commit -m "Updated core"
    git push origin master